#!/bin/sh
#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier: GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

# need selenium http://www.seleniumhq.org/download/
# need chromedriver : apt-get install chromedriver
BIN_PATH="`dirname \"$0\"`"
BIN_PATH="`( cd \"$BIN_PATH\" && pwd)`"
if [ -z "$BIN_PATH" ] ; then
  exit 1
fi

java -jar -Dwebdriver.chrome.driver='/usr/lib/chromium/chromedriver' -Dwebdriver.opera.driver='$BIN_PATH/operadriver' $BIN_PATH/selenium-server-standalone-3.0.1.jar
