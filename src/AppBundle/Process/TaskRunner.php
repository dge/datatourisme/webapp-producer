<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Process;
use AppBundle\Entity\ProcessStrategy;
use Datatourisme\Bundle\WebAppBundle\TaskManager\BeanStalkMessenger;
use Datatourisme\Bundle\WebAppBundle\TaskManager\Rundeck\JobManager\JobManagerInterface;
use Datatourisme\Bundle\WebAppBundle\TaskManager\TaskManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Workflow\StateMachine;
use Symfony\Component\Routing\Router;

/**
 * Class TaskRunner.
 */
class TaskRunner
{
    /**
     * @var TaskManager
     */
    private $taskManager;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var StateMachine
     */
    protected $processOperation;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var string
     */
    const TASK_NAME = 'Alignement';

    /**
     * ScheduleTask constructor.
     *
     * @param TaskManager   $taskManager
     * @param EntityManager $em
     * @param StateMachine  $processOperation
     * @param Router        $router
     *
     * @internal param BeanStalkMessenger $beanStalkMessenger
     * @internal param JobManagerInterface $rundeckManager
     */
    public function __construct(TaskManager $taskManager, EntityManager $em, StateMachine $processOperation, Router $router)
    {
        $this->taskManager = $taskManager;
        $this->em = $em;
        $this->processOperation = $processOperation;
        $this->router = $router;
    }

    /**
     * Start a whole process from beginning or from $task if provided.
     *
     * @param Flux $flux
     * @param null $source
     * @param null $task
     *
     * @throws \Exception
     */
    public function start(Flux $flux, $source = null, $task = null)
    {
        // check status
        if ($flux->getStatus() !== Flux::STATUS_PRODUCTION) {
            throw new \Exception('The flux is not in production');
        }

        // create process & payload
        $process = $this->createProcess($flux);
        $payload = $this->createDefaultPayload($flux, $process, $source);
        if ($task) {
            $payload['task'] = $task;
        }

        // run
        $jobId = $this->taskManager->run('producer', $payload);
        $process->setLastJob($jobId);
        $this->em->flush();
    }

    /**
     * @param Flux $flux
     */
    public function delete(Flux $flux)
    {
        // create process
        $process = $this->createProcess($flux);
        $payload = array(
            'uuid' => $process->getId(),
            'task' => 'delete',
            'flux' => $flux->getId(),
        );
        $jobId = $this->taskManager->run('producer', $payload);
        $process->setLastJob($jobId);
        $this->em->flush();
    }

    /**
     * Schedule a task for the worker.
     *
     * @param Flux $flux
     * @param int  $hour
     *
     * @return int
     *
     * @throws \Exception
     */
    public function schedule(Flux $flux, $hour = -1)
    {
        $this->taskManager->update(
            $flux->getId(),
            self::TASK_NAME,
            $this->router->generate('internal.api.run', array('id' => $flux->getId()), UrlGeneratorInterface::ABSOLUTE_URL),
            $hour,
            null,
            true
        );
    }

    /**
     * Unschedule a task for the worker.
     *
     * @param Flux $flux
     *
     * @return int
     *
     * @throws \Exception
     */
    public function unschedule(Flux $flux)
    {
        return $this->taskManager->delete($flux->getId());
    }

    /**
     * @param Flux $flux
     * @param null $process
     * @param null $source
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function createDefaultPayload($flux, $process = null, $source = null)
    {
        // routes
        $alignmentRoute = $this->router->generate('api.rules', array('flux_id' => $flux->getId()), UrlGeneratorInterface::ABSOLUTE_URL);
        $thesaurusRoute = $this->router->generate('internal.api.thesaurus', array('organization_id' => $flux->getOrganization()->getId()), UrlGeneratorInterface::ABSOLUTE_URL);

        $payload = array(
            'source' => $source,
            'alignment' => $alignmentRoute,
            'thesaurus' => $thesaurusRoute,
            'flux' => $flux->getId(),
            'checksum' => $flux->getchecksum(),
            'reset' => ($flux->getProcessStrategy()->getId() == ProcessStrategy::COMPLETE),
            'organization' => $flux->getOrganization()->getId(),
        );

        // flux crawler
        $crawler = $flux->getFluxCrawler();
        if ($crawler) {
            // source
            if (empty($payload['source']) && $crawler->getUrl()) {
                $payload['source'] = $crawler->getUrl();
            }
            // i18n sources
            if(!empty($crawler->getI18nUrls())) {
                $payload['i18nSources'] = $crawler->getI18nUrls();
            }
            // ssh key
            if($crawler->getSshKey()) {
                $payload['sshKey'] = $crawler->getSshKey();
            }
        }


        if ($process) {
            $payload['uuid'] = $process->getId();
        }

        return $payload;
    }

    /**
     * @param Flux $flux
     *
     * @return Process
     */
    protected function createProcess(Flux $flux)
    {
        $process = new Process($flux);
        $process->setMode($flux->getMode());
        $this->em->persist($process);
        $this->processOperation->getMarking($process);
        $this->em->flush();

        return $process;
    }
}
