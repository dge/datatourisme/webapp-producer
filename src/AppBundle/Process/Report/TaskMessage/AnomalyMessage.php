<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\TaskMessage;

use AppBundle\Entity\Anomaly;
use AppBundle\Entity\RdfResource;
use AppBundle\Process\Report\JobMessage\ReportEntry;

class AnomalyMessage extends AbstractMessage
{
    public function apply(RdfResource $rdfResource, ReportEntry $report)
    {
        if ($report->getReports()->count()) {
            foreach ($report->getReports() as $item) {
                $anomaly = new Anomaly();
                $anomaly->setRdfResource($rdfResource);
                $anomaly->setProcess($this->process);
                $anomaly->setContent($item->getMessage());
                if ($item->getLevel() === 'ERROR') {
                    $anomaly->setBreaker(true);
                }
                $this->em->persist($anomaly);
            }
        }

        return $this->process;
    }
}
