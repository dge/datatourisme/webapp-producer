<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\TaskMessage;

use AppBundle\Entity\RdfResource;
use AppBundle\Process\Report\JobMessage\ReportEntry;

class ProduceRdfMessage extends AbstractMessage
{
    protected $producedPOI;

    public function apply(ReportEntry $report)
    {
        $this->producedPOI = [];

        if ($report->getReports()->count()) {
            $this->batchProcess($report->getReports(), function (ReportEntry $item) {
                $data = $item->getData();

                $rdfResource = new RdfResource();
                $rdfResource->setFlux($this->process->getFlux());
                $rdfResource->setIdentitySIT($data['id']);
                $rdfResource->setUri($data['uri']);
                $rdfResource->setStatus(RdfResource::STATUS_PUBLISHED);
                if (isset($data['type'])) {
                    $rdfResource->setType($data['type']);
                }
                if (isset($data['type2'])) {
                    $rdfResource->setType2($data['type2']);
                }
                if (isset($data['label'])) {
                    $rdfResource->setLabel($data['label']);
                }
                if (isset($data['postalCode'])) {
                    $rdfResource->setPostalCode($data['postalCode']);
                }
                if (isset($data['city'])) {
                    $rdfResource->setCity($data['city']);
                }
                if (isset($data['creator'])) {
                    $rdfResource->setCreator($data['creator']);
                }
                $this->producedPOI[$data['id']] = $data['id'];

                switch ($item->getLevel()) {
                    case 'ERROR':
                        $rdfResource->setStatus(RdfResource::STATUS_REJECTED);
                    case 'WARN':
                        $anomaly = new AnomalyMessage($this->em, $this->process);
                        $anomaly->apply($rdfResource, $item);
                        break;
                }

                $this->em->persist($rdfResource);
            });
        }

        $datas = $report->getData();
        $this->process->setStatsProduceRDF($datas['stats']);

        return $this->process;
    }

    /**
     * @return mixed
     */
    public function getProducedPOI()
    {
        return $this->producedPOI;
    }
}
