<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\TaskMessage;

use AppBundle\Entity\RdfResource;
use AppBundle\Process\Report\JobMessage\ReportEntry;

class PersistRdfMessage extends AbstractMessage
{
    public function apply(ReportEntry $report, $producedPoi)
    {
        if ($report->getReports()->count()) {
            $persistedPOI = [];

            $this->batchProcess($report->getReports(), function (ReportEntry $item) use ($producedPoi, $persistedPOI) {
                $data = $item->getData();
                if (isset($producedPoi[$data['id']]) || isset($persistedPOI[$data['id']])) {
                    return;
                }
                $persistedPOI[$data['id']] = $data;
                $rdfResource = new RdfResource();
                $rdfResource->setFlux($this->process->getFlux());
                $rdfResource->setIdentitySIT($data['id']);
                $rdfResource->setUri($data['uri']);
                $rdfResource->setStatus(RdfResource::STATUS_PUBLISHED);
                if (isset($data['type'])) {
                    $rdfResource->setType($data['type']);
                }
                if (isset($data['type2'])) {
                    $rdfResource->setType2($data['type2']);
                }
                if (isset($data['label'])) {
                    $rdfResource->setLabel($data['label']);
                }
                if (isset($data['postalCode'])) {
                    $rdfResource->setPostalCode($data['postalCode']);
                }
                if (isset($data['city'])) {
                    $rdfResource->setCity($data['city']);
                }
                if (isset($data['creator'])) {
                    $rdfResource->setCreator($data['creator']);
                }
                $this->em->persist($rdfResource);
            });
        }

        $datas = $report->getData();
        $this->process->setStatsPersistRDF($datas['stats']);
        if (isset($datas['stats']['evolution'])) {
            $this->process->setEvolution(number_format(floatval($datas['stats']['evolution']) * 100, 2, '.', ''));
        }
        $this->process->setNbPublishedRdfResources($datas['stats']['total']);

        $statsProduce = $this->process->getStatsProduceRDF();
        $this->process->setNbExcludedRdfResources((isset($statsProduce['total']) ? $statsProduce['total'] : 0) - $datas['stats']['total']);

        return $this->process;
    }
}
