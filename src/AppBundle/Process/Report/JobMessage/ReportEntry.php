<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\JobMessage;

class ReportEntry
{
    const LEVEL_FATAL = 'FATAL';
    const LEVEL_INFO = 'INFO';

    private $message;
    private $stage;
    private $level;
    private $exception;
    private $data;
    private $duration;
    private $reports;

    public function __construct()
    {
        $this->reports = new \ArrayObject();
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @param mixed $stage
     */
    public function setStage(string $stage)
    {
        $this->stage = $stage;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param mixed $exception
     */
    public function setException($exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        if (is_array($duration)) {
            $this->duration = $duration['total'];
        } else {
            $this->duration = $duration;
        }
    }

    /**
     * @return \ArrayObject|ReportEntry[] ReportEntry
     */
    public function getReports(): \ArrayObject
    {
        return $this->reports;
    }

    /**
     * @param array $reports
     */
    public function setReports(array $reports)
    {
        foreach ($reports as $report) {
            $reportEntry = new self();
            $reportEntry->setEntry($report);
            $this->reports->append($reportEntry);
        }
    }

    /**
     * @param $r
     *
     * @return ReportEntry
     */
    public function setEntry($r)
    {
        isset($r['message']) ? $this->setMessage($r['message']) : '';
        isset($r['data']) ? $this->setData($r['data']) : '';
        isset($r['stage']) ? $this->setStage($r['stage']) : '';
        isset($r['level']) ? $this->setLevel($r['level']) : '';
        isset($r['exception']) ? $this->setException($r['exception']) : '';
        isset($r['reports']) ? $this->setReports($r['reports']) : '';
        isset($r['duration']) ? $this->setDuration($r['duration']) : '';

        return $this;
    }
}
