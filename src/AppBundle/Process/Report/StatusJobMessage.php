<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report;

use AppBundle\Entity\Process;
use AppBundle\Process\Report\JobMessage\JobMessageAbstract;

class StatusJobMessage extends JobMessageAbstract
{
    public function updateProcess(Process $process)
    {
        $process->setLastJob($this->getStats()->offsetGet('id'));
        $process->setProgress($this->getTask());
        $process->setDuration($process->getUpdatedAt()->getTimestamp() - $process->getCreatedAt()->getTimestamp());
    }
}
