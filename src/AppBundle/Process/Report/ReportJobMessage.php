<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report;

use AppBundle\Entity\Process;
use AppBundle\Process\Report\JobMessage\JobMessageAbstract;
use AppBundle\Process\Report\JobMessage\ReportEntry;
use AppBundle\Process\Report\TaskMessage\FatalMessage;
use AppBundle\Process\Report\TaskMessage\PersistRdfMessage;
use AppBundle\Process\Report\TaskMessage\ProcessXMLMessage;
use AppBundle\Process\Report\TaskMessage\ProduceRdfMessage;
use Doctrine\ORM\EntityManager;

class ReportJobMessage extends JobMessageAbstract
{
    /** @var \ArrayObject $report */
    private $reports;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->reports = new \ArrayObject();
        isset($data['reports']) ? $this->setReports($data['reports']) : '';
    }

    /**
     * @return mixed
     */
    public function getReports(): \ArrayObject
    {
        return $this->reports;
    }

    /**
     * @param array $reports
     */
    public function setReports($reports)
    {
        foreach ($reports as $step => $report) {
            $reportEntry = new ReportEntry();
            $reportEntry->setStage($step);
            $reportEntry->setEntry($report);
            $this->reports->append($reportEntry);
        }
    }

    /**
     * @param EntityManager $em
     * @param Process       $process
     *
     * @return bool
     */
    public function handleReports(EntityManager $em, Process $process)
    {
        $reports = $this->getReports();
        // get the last report and stop execution if fatal
        if ($reports && $reports->count()) {
            $job = $reports[$reports->count() - 1];
            if ($job->getLevel() == ReportEntry::LEVEL_FATAL) {
                $fatal = new FatalMessage($em, $process);
                $fatal->apply($job);

                return $process; // Fatal error, stop the loop
            }
        }

        $produceRdf = null;

        foreach ($reports as $job) {
            switch ($job->getStage()) {
                case 'delete':
                    $em->remove($process->getFlux());
                    break;
                case 'process_xml':
                    $processXML = new ProcessXMLMessage($em, $process);
                    $process = $processXML->apply($job);
                    break;
                case 'produce_rdf':
                    $produceRdf = new ProduceRdfMessage($em, $process);
                    $process = $produceRdf->apply($job);
                    break;
                case 'persist_rdf':
                    $persistRdf = new PersistRdfMessage($em, $process);
                    $process = $persistRdf->apply($job, $produceRdf ? $produceRdf->getProducedPOI() : []);
                    break;
            }
        }

        return $process;
    }

    /**
     * @return array
     */
    public function getPersistStats()
    {
        $reports = $this->getReports();
        if ($reports && $reports->count()) {
            foreach ($reports as $report) {
                if ($report->getStage() == 'persist_rdf') {
                    return $report->getData();
                }
            }
        }

        return [];
    }

    /**
     * total duration, in ms.
     *
     * @return int|mixed
     */
    public function getTotalDuration()
    {
        $total = 0;
        $reports = $this->getReports();
        if ($reports && $reports->count()) {
            /** @var ReportEntry $report */
            foreach ($reports as $report) {
                $total += $report->getDuration() ? $report->getDuration() : 0;
            }
        }

        return $total;
    }
}
