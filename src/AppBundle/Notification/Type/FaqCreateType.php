<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractType;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;

class FaqCreateType extends AbstractType
{
    private $_em;

    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
    }

    public function getMessage()
    {
        return "Une nouvelle question a été ajoutée à la FAQ : {{ faq.question }}";
    }

    public function getDescription()
    {
        return "{{ faq.answer | markdownize | raw }}";
    }

    public function getContext()
    {
        return array('faq' => $this->getSubject());
    }

    public function getRoute()
    {
        return  'faq.index';
    }

    public function getLevel()
    {
        return Logger::INFO;
    }

    public function getRecipients()
    {
        $recipients = $this->_em->getRepository('AppBundle:User')->findBy(['role' => ['ROLE_USER']]);
        return $recipients;
    }

    public function getEntity()
    {
        $notification = new Notification();
        $notification->setRole('ROLE_USER');
        return $notification;
    }
}
