<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractType;
use Doctrine\Common\Collections\Collection;
use Monolog\Logger;

class ReuseReportNotifyType extends AbstractType
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'Le rapport de réutilisation du mois de {{ (date|moment).format("f Y") }} est disponible.';
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return ((array) $this->getSubject());
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return 'reuse.archive.download';
    }

    /**
     * @return string
     */
    public function getRouteTitle(): string
    {
        return 'Télécharger le rapport';
    }

    /**
     * @return array
     */
    public function getRouteParameters(): array
    {
        $date = $this->getSubject()->date;
        return ['year' => $date->format('Y'), 'month' => $date->format('m')];
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return Logger::NOTICE;
    }

    /**
     * @return Collection
     */
    public function getRecipients(): Collection
    {
        return $this->getSubject()->organization->getUsers();
    }
}
