<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\Organization;
use AppBundle\Repository\ReuseRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class ReuseFilter.
 */
class ReuseFilter extends AbstractType
{
    /**
     * @var ReuseRepository
     */
    private $reuseRepository;

    /**
     * @param ReuseRepository $reuseRepository
     */
    public function setReuseRepository(ReuseRepository $reuseRepository)
    {
        $this->reuseRepository = $reuseRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('creator', ChoiceType::class, array(
                'placeholder' => '-- Tous les créateurs --',
                'choices' => $this->reuseRepository->getCreatorChoices(),
                'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values) {
                    if (empty($values['value'])) {
                        return null;
                    }
                    $builder = $filterQuery->getQueryBuilder();
                    $builder->andWhere('r.poiCreator = :creator');
                    $builder->setParameter('creator', $values['value']);
                }
            ));
    }
}
