<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Process;
use AppBundle\Repository\FluxRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class ProductionFluxFilter.
 */
class ProductionFluxFilter extends AbstractType
{
    /**
     * @var AuthorizationChecker
     */
    private $checker;

    /**
     * SupportPostType constructor.
     *
     * @param AuthorizationChecker $checker
     */
    public function __construct(AuthorizationChecker $checker)
    {
        $this->checker = $checker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultPlaceholder = '-- Tout --';

        $builder

            // ____ ORGANIZATION
            ->add('organization', EntityFilterType::class, array(
                'class' => Organization::class,
                'placeholder' => $defaultPlaceholder,
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('o')
                        ->orderBy('o.name');
                },
            ))

            // ____ NAME
            ->add('name', TextFilterType::class)

            // ____ STATUS
            ->add('status', ChoiceFilterType::class, array(
                'placeholder' => $defaultPlaceholder,
                'choices' => (function (): array {
                    $choices = array();
                    $fluxProdStatusList = Flux::productionStatusList();
                    foreach ($fluxProdStatusList as $fluxStatus) {
                        if ($fluxStatus === Flux::STATUS_PRODUCTION) {
                            $processStatusList = Process::statusList();
                            foreach ($processStatusList as $processStatus) {
                                $choices[Process::$statusDefinitions[$processStatus][0]] = $processStatus;
                            }
                        } else {
                            $choices[Flux::$statusDefinitions[$fluxStatus][0]] = $fluxStatus;
                        }
                    }
                    ksort($choices);

                    return $choices;
                })(),
                'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values): bool {
                    if (empty($values['value'])) {
                        return false;
                    }

                    $builder = $filterQuery->getQueryBuilder();
                    $builder->andWhere(FluxRepository::$statusCaseSqlStatement.' = :status');
                    $builder->setParameter('status', $values['value']);

                    return true;
                },
            ))

            // ____ RULESET
            ->add('ruleset', ChoiceFilterType::class, array(
                'placeholder' => $defaultPlaceholder,
                'choices' => (function (): array {
                    $pendingStatus = AlignmentRulesetRevision::STATUS_PENDING;
                    $activeStatus = AlignmentRulesetRevision::STATUS_ACTIVE;
                    $definitions = AlignmentRulesetRevision::$statusDefinitions;

                    $choices = [
                        $definitions[$pendingStatus][0] => $pendingStatus,
                        $definitions[$activeStatus][0] => $activeStatus,
                    ];
                    ksort($choices);

                    return $choices;
                })(),
                'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values): bool {
                    $status = &$values['value'];
                    if (empty($status)) {
                        return false;
                    } elseif (!in_array($status, [AlignmentRulesetRevision::STATUS_PENDING,
                                                  AlignmentRulesetRevision::STATUS_ACTIVE, ], true)) {
                        throw new \UnexpectedValueException('Given status "'.$status.'" was unexpected.');
                    }

                    $builder = $filterQuery->getQueryBuilder();
                    $builder->andWhere('draft_revision.status = :status');
                    $builder->setParameter('status', $status);

                    return true;
                },
            ))

            // ____ LAST PROCESS DATE
            ->add('last_process_date', TextFilterType::class, array(
                'apply_filter' => false,
            ))

            // ____ NB RDF RESOURCES
            ->add('nb_rdf_resources', TextFilterType::class, array(
                'apply_filter' => false,
            ))

            // ____ EVENTS
            ->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'))

        ;
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        if (false === $this->checker->isGranted('flux.filter.organization')) {
            $event->getForm()->remove('organization');
        }
    }
}
