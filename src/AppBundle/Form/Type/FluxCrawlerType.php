<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Flux;
use AppBundle\Entity\FluxCrawler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FluxCrawlerType.
 */
class FluxCrawlerType extends AbstractType
{
    public static $authorizedProtocols = array(
        'https', 'sftp',
    );

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('url', UrlType::class, array(
                'label' => 'Adresse URL',
                'help' => 'L\'adresse spécifiée doit être publiquement accessible et fournir un fichier de type XML ou ZIP.
                    Le format ZIP doit contenir exclusivement des fichiers au format XML.
                    <br>Protocoles acceptés : '.implode(', ', array_map(function ($protocol) { return '<strong>'.strtoupper($protocol).'</strong>'; }, self::$authorizedProtocols)),
                'required' => false,
                'attr' => array(
                    'maxLength' => 1024,
                    'placeHolder' => 'Adresse URL de récupération du flux',
                ),
                'group_attr' => array(
                    'data-visibility-target' => '#flux_detail_mode',
                    'data-visibility-value' => Flux::MODE_PULL,
                    'class' => 'required',
                ),
            ))
            ->add('i18nUrls', I18nUrlsType::class, array(
                'label' => 'Adresses linguistiques',
                'group_attr' => array(
                    'data-visibility-target' => '#flux_detail_mode',
                    'data-visibility-value' => Flux::MODE_PULL
                ),
            ))
            ->add('updateHour', ChoiceType::class, array(
                'label' => 'Heure',
                'choices' => array(
                    '00h00' => 0,
                    '01h00' => 1,
                    '02h00' => 2,
                    '03h00' => 3,
                    '04h00' => 4,
                    '05h00' => 5,
                    '06h00' => 6,
                    '07h00' => 7,
                    '08h00' => 8,
                    '09h00' => 9,
                    '10h00' => 10,
                    '11h00' => 11,
                    '12h00' => 12,
                    '13h00' => 13,
                    '14h00' => 14,
                    '15h00' => 15,
                    '16h00' => 16,
                    '17h00' => 17,
                    '18h00' => 18,
                    '19h00' => 19,
                    '20h00' => 20,
                    '21h00' => 21,
                    '22h00' => 22,
                    '23h00' => 23,
                ),
                'help' => 'Vous pouvez spécifier une heure à partir de laquelle la plateforme est sûre de récupérer un flux à jour.',
                'required' => false,
                'placeholder' => 'N\'importe',
                'attr' => array(
                    'class' => 'required',
                ),
                'group_attr' => array(
                    'data-visibility-target' => '#flux_detail_mode',
                    'data-visibility-value' => Flux::MODE_PULL,
                    'class' => 'required',
                ),
            ))
            ->add('sshKey', TextareaType::class, array(
                'label' => 'Clé SSH',
                'help' => 'Si vous avez spécifié une URL avec le protocole <strong>SFTP</strong>, une clé SSH est requise. La clé publique doit être ajoutée aux <strong>authorized_keys</strong>.',
                'required' => false,
                'attr' => array(
                    'rows' => 5,
                    'placeHolder' => 'Clé SSH publique d\'authentification sur votre serveur.',
                    'maxLength' => 4096,
                ),
                'group_attr' => array(
                    'data-visibility-target' => '#flux_detail_mode',
                    'data-visibility-value' => Flux::MODE_PULL,
                ),
            ))
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmit'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => FluxCrawler::class,
            'sshKey' => null,
        ));
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $mode = $form->getParent()->get('mode')->getData();

        if ($mode === Flux::MODE_PULL) {
            $crawlerUrl = $form->get('url')->getData();
            $crawlerShhKey = $form->get('sshKey')->getData();

            if (empty($crawlerUrl)) {
                $form->get('url')->addError(new FormError('Vous devez renseigner une adresse URL'));

                return;
            }

            $scheme = strtolower(parse_url($crawlerUrl, PHP_URL_SCHEME));
            if (!in_array($scheme, self::$authorizedProtocols)) {
                $form->get('url')->addError(new FormError(sprintf('Le protocole %s n\'est pas autorisé', strtoupper($scheme))));

                return;
            }

            if ($scheme == 'sftp' && empty($crawlerShhKey)) {
                $form->get('sshKey')->addError(new FormError('Vous devez renseigner une clé SSH pour récupérer votre flux en SFTP.'));
            }
        }
    }
}
