<?php 
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

// src/Form/ProductType.php
namespace AppBundle\Form\Type;

use AppBundle\Entity\News;
use AppBundle\Utils\MimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'label' => 'Date',
            ))
            ->add('title', TextType::class, array(
                'attr' => array(
                    'placeholder' => "Titre de l'actualité...",
                ),
                'required' => true,
                'label' => 'Titre',
            ))
            ->add('body', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => "Contenu de l'actualité...",
                    'rows' => '10',
                ),
                'required' => true,
                'label' => 'Texte',
            ));
            // ->add('file', FileType::class, array(
            //     'label' => 'Fichier joint',
            //     'required' => false,
            //     'help' => 'Le fichier joint est limité à 5Mo. <br><span class="text-sm">Extensions autorisées : '.implode(', ', $allowedExtensions).'</span>',
            // ))
            // ->add('removeFile', CheckboxType::class, array(
            //     'label' => 'Supprimer',
            //     'required' => false,
            // ))
            // ;
            
            $builder->addEventListener(FormEvents::POST_SET_DATA, array($this, 'onPostSetData'));
        ;
    }

    /**
     * onPostSetData
     */
    public function onPostSetData(FormEvent $event) {
        $form = $event->getForm();
        if ($event->getData()->getFile()) {
            $form->add('removeFile', CheckboxType::class, array(
                'label' => 'Supprimer le fichier joint',
                'mapped' => false,
                'required' => false,
            ));
        } else {
            $allowedExtensions = MimeType::getExtensionsFromTypes(News::getAllowedMimeTypes());
            sort($allowedExtensions);
            $form->add('file', FileType::class, array(
                'label' => 'Fichier joint',
                'required' => false,
                'help' => 'Le fichier joint est limité à 5Mo. <br><span class="text-sm">Extensions autorisées : '.implode(', ', $allowedExtensions).'</span>',
            ));
        }
    }

    /**
     * configureOptions
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}