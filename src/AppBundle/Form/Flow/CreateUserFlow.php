<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Flow;

use AppBundle\Form\Type\UserType;
use AppBundle\Form\Type\UserOrganizationType;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

/**
 * Class CreateUserFlow.
 */
class CreateUserFlow extends FormFlow
{
    /**
     * @var RoleHierarchy
     */
    protected $roleHierarchy;

    public function __construct(RoleHierarchy $roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * @return array
     */
    protected function loadStepsConfig()
    {
        return array(
            array(
                'label' => 'Nouvel utilisateur',
                'form_type' => UserType::class,
                'form_options' => array(
                    'validation_groups' => array('Default'),
                ),
            ),
            array(
                'label' => 'Nouvelle structure',
                'form_type' => UserOrganizationType::class,
                'form_options' => array(
                    'validation_groups' => array('Default'),
                ),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $organization = $flow->getFormData()->getOrganization();
                    $user = $flow->getFormData();
                    $hasRoleAdmin = in_array(new Role('ROLE_ADMIN'), $this->roleHierarchy->getReachableRoles(array(new Role($user->getRole()))));

                    return $hasRoleAdmin || $organization && $organization->getId() !== null;
                },
            ),
        );
    }
}
