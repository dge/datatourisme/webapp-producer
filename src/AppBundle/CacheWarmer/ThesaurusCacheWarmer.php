<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\CacheWarmer;

use AppBundle\Thesaurus\Cache\Export;
use AppBundle\Thesaurus\Cache\Hierarchy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

/**
 * Class ThesaurusCacheWarmer.
 */
class ThesaurusCacheWarmer implements CacheWarmerInterface
{
    /**
     * @var Hierarchy
     */
    protected $hierarchyCache;

    /**
     * @var Export
     */
    protected $exportCache;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ThesaurusWarmer constructor.
     *
     * @param Hierarchy          $hierarchyCache
     * @param Export             $exportCache
     * @param ContainerInterface $container
     */
    public function __construct($hierarchyCache, $exportCache, ContainerInterface $container)
    {
        $this->hierarchyCache = $hierarchyCache;
        $this->exportCache = $exportCache;
        $this->container = $container;
    }

    /**
     * @param string $cacheDir
     */
    public function warmUp($cacheDir)
    {
        $routes = array(
            array(
                'type' => 'classes',
                'classes' => array(
                    $this->container->getParameter('namespaces')['ontology'] . 'PointOfInterest',
                ),
            ),
            array(
                'type' => 'individuals',
                'classes' => array(
                    $this->container->getParameter('namespaces')['ontology'] . 'Audience',
                    $this->container->getParameter('namespaces')['ontology'] . 'Amenity',
                    $this->container->getParameter('namespaces')['ontology'] . 'Theme',
                ),
            ),
//            array(
//                'type' => 'geographic',
//                'classes' => array(
//                    'https://www.datatourisme.gouv.fr/resource/core/1.0#France',
//                    'http://schema.org/Country',
//                ),
//            ),
            array(
                'type' => 'other',
                'classes' => array('http://www.w3.org/2002/07/owl#NamedIndividual'),
            ),
        );

        // generate cache for classes alignment and individuals alignment
        foreach ($routes as $route) {
            foreach ($route['classes'] as $rdfClass) {
                $this->generateCache($route['type'], $rdfClass, $cacheDir);
            }
        }
    }

    /**
     * @return bool
     */
    public function isOptional()
    {
        return true;
    }

    /**
     * @param $type
     * @param $rdfClass
     * @param $cacheDir
     */
    protected function generateCache($type, $rdfClass, $cacheDir)
    {
        if ('geographic' === $type) {
            $rdfClass = $this->container->get('app.thesaurus.hierarchy.classes_builder')->loadOntology()->shortIri($rdfClass);
        } else {
            $rdfClass = $this->container->get('app.thesaurus.hierarchy.individuals_builder')->loadOntology()->shortIri($rdfClass);
        }

        $this->exportCache->get($type, $rdfClass, $cacheDir);
        $params = $this->hierarchyCache->get($type, $rdfClass, $cacheDir);
        $menu = $rdfClass ? $params['currentHierarchy']['hierarchy'] : $params['menu'];

        foreach ($menu as $class => $info) {
            if (is_array($info) && isset($info['nbrChild']) && $info['nbrChild'] > 0) {
                $this->generateCache($type, $class, $cacheDir);
            }
        }
    }
}
