<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\JavaWorker;

use AppBundle\Entity\Flux;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class JavaWorkerClient
{
    private $client;
    private $scheme = 'http';
    private $host = 'localhost';
    private $port = 8042;

    /**
     * JavaWorkerClient constructor.
     *
     * @param $host
     * @param $port
     */
    public function __construct($host, $port)
    {
        $this->host = $host;
        $this->port = $port;
        $this->client = new Client([
            'base_uri' => $this->scheme.'://'.$this->host.':'.$this->port,
            //'timeout'  => 2.0
        ]);
    }

    /**
     * Get the ontology json.
     *
     * @param bool $lite
     * @param bool $assoc
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function ontology($profile = null)
    {
        $url = 'ontology'.($profile ? '/'.$profile : '');
        $response = $this->client->get($url);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * Get the xpath-schema json.
     *
     * @param Flux $flux
     * @param null $type
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function xpathSchema(Flux $flux, $type = null)
    {
        $url = 'producer/'.$flux->getId().'/xpath-schema';
        if ($type != null) {
            $url .= '/'.$type;
        }
        $response = $this->client->get($url);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux $flux
     * @param $params
     *
     * @return mixed
     */
    public function exprRawExtract(Flux $flux, $params)
    {
        $url = 'producer/'.$flux->getId().'/expr-raw-extract';
        $response = $this->client->post($url, ['json' => array_filter($params)]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux $flux
     * @param $source
     * @param $params
     *
     * @return mixed
     */
    public function exprExtract(Flux $flux, $source = 'dao', $params)
    {
        $url = 'producer/'.$flux->getId().'/expr-preview/'.$source;
        $response = $this->client->post($url, ['json' => array_filter($params)]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux $flux
     * @param $query
     *
     * @return mixed
     */
    public function executeSQL(Flux $flux, $query)
    {
        $url = 'producer/'.$flux->getId().'/execute-sql';
        $params = array_filter([
            'query' => $query,
        ]);
        $response = $this->client->post($url, ['json' => $params]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux $flux
     * @param $task
     * @param null $endTask
     * @param $payload
     */
    public function runTask(Flux $flux, $task, $endTask = null, $payload)
    {
        $url = 'producer/'.$flux->getId().'/run-task';
        $params = array_filter([
            'task' => $task,
            'endTask' => $endTask,
            'payload' => $payload,
        ]);
        $this->client->post($url, ['json' => $params]);
    }

    /**
     * @param Flux $flux
     * @param $jobId
     *
     * @return mixed
     */
    public function jobLog(Flux $flux, $jobId)
    {
        $url = 'producer/'.$flux->getId().'/job/'.$jobId.'/log';
        $response = $this->client->get($url);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux  $flux
     * @param array $params
     *
     * @return mixed
     */
    public function previewIndex(Flux $flux, $params = array())
    {
        $url = 'producer/'.$flux->getId().'/preview';
        $response = $this->client->get($url, [
            'query' => $params,
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux $flux
     * @param $id
     * @param $alignment
     *
     * @return mixed
     */
    public function previewResource(Flux $flux, $id, $alignment, $thesaurus)
    {
        $url = 'producer/'.$flux->getId().'/preview/'.urlencode($id);

        // fix to avoid bas cast
        if (empty($alignment['rules'])) {
            $alignment['rules'] = new \stdClass();
        }
        if (empty($alignment['@context'])) {
            $alignment['@context'] = new \stdClass();
        }

        $params = array_filter([
            'alignment' => $alignment,
            'thesaurus' => $thesaurus,
        ]);

        $response = $this->client->post($url, ['json' => $params]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @return mixed
     */
    public function previewAtomicChain(array $values, array $chain)
    {
        $url = 'producer/atomic/preview';
        $params = array_filter([
            'values' => $values,
            'chain' => $chain,
        ]);

        $response = $this->client->post($url, ['json' => $params]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux $flux
     *
     * @return StreamedResponse
     */
    public function downloadSource(Flux $flux)
    {
        $response = new StreamedResponse();

        $response->setCallback(function () use ($flux) {
            $resp = $this->client->request('GET', 'producer/'.$flux->getId().'/source/download', [
                'stream' => true,
                'on_headers' => function (ResponseInterface $resp) use ($flux) {
                    // set content type
                    header('Content-Type: '.$resp->getHeaderLine('Content-Type'));
                    header('Content-Disposition: '.$resp->getHeaderLine('Content-Disposition'));
                },
            ]);

            $body = $resp->getBody();
            // Read bytes off of the stream until the end of the stream is reached
            while (!$body->eof()) {
                echo $body->read(1024);
            }
        });

        $response->send();

        return $response;
    }

    /**
     * Generate a JsonResponse based on RequestException.
     *
     * @param RequestException $e
     *
     * @return JsonResponse
     */
    public static function handleRequestException(RequestException $e)
    {
        if ($response = $e->getResponse()) {
            return JsonResponse::fromJsonString($response->getBody()->getContents(), $e->getCode());
        } else {
            return JsonResponse::create(['error' => ['code' => 504, 'message' => 'Aucune réponse du serveur de traitement.']], 504);
        }
    }
}
