<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Process;
use AppBundle\Entity\RdfResource;
use Doctrine\DBAL\Query\QueryBuilder as QueryQueryBuilder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class RdfResourceRepository.
 */
class RdfResourceRepository extends EntityRepository
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param Flux $flux
     *
     * @return QueryBuilder
     */
    public function createByFluxQueryBuilder(Flux $flux): QueryBuilder
    {
        $qb = $this->createQueryBuilder('rdf_resource');

        $qb
            ->where('rdf_resource.flux = :f')
            ->setParameter('f', $flux);

        $qb
            ->addSelect('rdf_resource.identitySIT AS identitySIT')
            ->addSelect('rdf_resource.label AS label')
            ->addSelect('rdf_resource.type AS type')
            ->addSelect('rdf_resource.city AS city')
            ->addSelect('rdf_resource.status AS status')
            ->addSelect('COUNT ( anomalies ) AS anomalies_count')
            ->leftJoin('rdf_resource.anomalies', 'anomalies')
            ->addGroupBy('rdf_resource.id');

        return $qb;
    }

    /**
     * @param Flux $flux
     *
     * @return array
     */
    public function findCityList(Flux $flux): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $alias = 'city';

        $qb
            ->select('rdf_resource.city AS '.$alias)
            ->from(RdfResource::class, 'rdf_resource')
            ->where('rdf_resource.flux = :f')
            ->setParameter('f', $flux);

        $result = $qb->getQuery()->getScalarResult();

        $cityList = array();
        $count = count($result);
        for ($i = 0; $i < $count; ++$i) {
            $cityList[] = $result[$i][$alias];
        }

        $cityList = array_unique($cityList);

        return $cityList;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->getCountByStatus(null);
    }

    /**
     * @return int
     */
    public function getPublishedCount(): int
    {
        return $this->getCountByStatus(RdfResource::STATUS_PUBLISHED);
    }

    /**
     * @return int
     */
    public function getRejectedCount(): int
    {
        return $this->getCountByStatus(RdfResource::STATUS_REJECTED);
    }

    /**
     * @param Flux        $flux
     * @param string|null $status
     *
     * @return int
     */
    public function getCountByFlux(Flux $flux, string $status = null): int
    {
        $qb = $this->createQueryBuilder('rdf_resource');

        $qb->select('COUNT (rdf_resource)');

        $qb->where('rdf_resource.flux = :flux');
        $qb->setParameter('flux', $flux);

        if ($status) {
            $qb->andWhere('rdf_resource.status = :status');
            $qb->setParameter('status', $status);
        }

        if (false === $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $qb->join('rdf_resource.flux', 'flux', Join::WITH, 'flux.organization = :organization');
            $qb->andWhere('flux.organization = :organization');
            $qb->setParameter('organization', $this->tokenStorage->getToken()->getUser()->getOrganization());
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string|null $status
     * @param bool        $restrictive If true, applies restriction based upon user's role
     *                                 /!\ Setting false to this parameter is not a normal usage of the method;
     *                                 it has to be used carefuly with specific and controlled needs
     *
     * @return int
     */
    public function getCountByStatus(string $status = null, Flux $flux = null, bool $restrictive = true): int
    {
        // COUNT(DISTINCT ) is really slow on PostgreSQL, so we use the $qb as a subquery in a DBAL COUNT(*) query
        // @see https://stackoverflow.com/questions/11250253/postgresql-countdistinct-very-slow

        // use dbal query builder to be able to make a subquery
        $conn = $this->getEntityManager()->getConnection();
        $qb = $conn->createQueryBuilder();

        $qb->select('DISTINCT uri')
            ->from('rdf_resource', 'res');

        if ($status) {
            $qb->andWhere('res.status = :status');
            $qb->setParameter('status', $status);
        }

        if ($flux) {
            $qb->andWhere('flux_id = :flux_id');
            $qb->setParameter('flux_id', $flux->getId());
        }

        if (true === $restrictive) {
            if (false === $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                $qb->innerJoin('res', 'flux', 'flux', 'flux.id = res.flux_id');
                $qb->andWhere('flux.organization_id = :organization_id');
                $qb->setParameter('organization_id', $this->tokenStorage->getToken()->getUser()->getOrganization()->getId());
            }
        }

        $sql = "SELECT COUNT(*) FROM (" . $qb->getSQL() . ") as tmp";
        $stmt = $conn->prepare($sql);
        foreach ($qb->getParameters() as $key => $value) {
            $stmt->bindValue($key, $value, is_int($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR);
        }
        $stmt->execute();
        
        return (int) $stmt->fetchColumn(0);
    }

    /**
     * @param Process $process
     * @param string  $content
     *
     * @return array
     */
    public function findByAnomalyContent(Process $process, string $content): array
    {
        $qb = $this->createQueryBuilder('rdf_resource');

        $qb
            ->leftJoin('rdf_resource.anomalies', 'anomaly')
            ->where('anomaly.process = :process')
            ->andWhere('anomaly.content = :content')
            ->setParameter('process', $process)
            ->setParameter('content', $content)
        ;

        $rdfResources = $qb->getQuery()->getResult();

        return $rdfResources;
    }

    public function getPublishedByPoi()
    {
        $qb = $this->createQueryBuilder('rdf_resource');

        $qb
            ->select('rdf_resource.type, count(rdf_resource) as nb_poi')
            ->where('rdf_resource.status = :status')
            ->groupBy('rdf_resource.type')
            ->setParameter('status', RdfResource::STATUS_PUBLISHED)
        ;

        if (false === $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $qb->join('rdf_resource.flux', 'flux', Join::WITH, 'flux.organization = :organization');
            $qb->andWhere('flux.organization = :organization');
            $qb->setParameter('organization', $this->tokenStorage->getToken()->getUser()->getOrganization());
        }

        $rdfResources = $qb->getQuery()->getResult();

        return $rdfResources;
    }

    /**
     * @param Flux|null $flux
     *
     * @return array
     */
    public function getByPoi($flux = null): array
    {
        $qb = $this->createQueryBuilder('rdf_resource');

        $qb
            ->select('rdf_resource.type, count(rdf_resource) as nb_poi')
            ->groupBy('rdf_resource.type')
        ;

        if ($flux) {
            $qb
                ->where('rdf_resource.flux = :flux')
                ->setParameter('flux', $flux);
        }

        $qb->andWhere('rdf_resource.status = :status')
            ->setParameter('status', RdfResource::STATUS_PUBLISHED);

        $rdfResources = $qb->getQuery()->getResult();

        return $rdfResources;
    }
}
