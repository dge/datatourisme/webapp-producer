<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Process;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class AnomalyRepository.
 */
class AnomalyRepository extends EntityRepository
{
    /**
     * @param Process $process
     *
     * @return Query
     */
    public function createByProcessQuery(Process $process): Query
    {
        $qb = $this->createQueryBuilder('anomaly');

        $qb
                ->select('anomaly.content')
                ->addSelect('anomaly.breaker')
                ->addSelect('COUNT (anomaly.content) as nbr_rdf_resources')
                ->where('anomaly.process = :process')
                ->setParameter('process', $process)
                ->addOrderBy('anomaly.breaker', 'desc')
                ->addGroupBy('anomaly.content')
                ->addGroupBy('anomaly.breaker');

        $query = $qb->getQuery();
        $query->setHint('knp_paginator.count', $this->getGroupedByContentCount($process));

        return $query;
    }

    /**
     * @param Process $process
     *
     * @return int
     */
    public function getCountByProcess(Process $process): int
    {
        $qb = $this->createQueryBuilder('anomaly');

        $qb->select('COUNT (anomaly.id)');

        $qb->where('anomaly.process = :process');
        $qb->setParameter('process', $process);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Flux $flux
     *
     * @return int
     *
     * @throws \Exception
     */
    public function getCountByFlux(Flux $flux): int
    {
        if (!$process = $flux->getLastActiveProcess()) {
            return 0;
        }

        return $this->getCountByProcess($process);
    }

    /**
     * @param Process $process
     *
     * @return int
     */
    public function getTotalAnomalies(Process $process)
    {
        $builder = $this->createQueryBuilder('anomaly')
            ->select('COUNT (anomaly) AS totalAnomalies')
            ->where('anomaly.process = :process')
            ->setParameter('process', $process);
        $result = $builder->getQuery()->getResult();

        if (isset($result[0]) && isset($result[0]['totalAnomaly'])) {
            return $result[0]['totalAnomaly'];
        }

        return 0;
    }

    /**
     * @param Process $process
     *
     * @return int
     */
    public function getGroupedByContentCount(Process $process): int
    {
        $qb = $this->createQueryBuilder('anomaly');

        $qb
            ->select('COUNT (DISTINCT anomaly.content)')
            ->where('anomaly.process = :process')
            ->setParameter('process', $process);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
