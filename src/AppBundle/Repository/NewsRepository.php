<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Faq;
use AppBundle\Entity\FaqTheme;
use AppBundle\Entity\News;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class NewsRepository.
 */
class NewsRepository extends EntityRepository
{
    public function findByYearMonth($year, $month)
    {
        $sql = 'SELECT news.* FROM news WHERE EXTRACT(YEAR FROM date) = ? AND EXTRACT(MONTH FROM date) = ? ORDER BY date DESC, id DESC';
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata(News::class, 'f');
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $query->setParameters([$year, $month]);
        return $query->getResult();
    }
}
