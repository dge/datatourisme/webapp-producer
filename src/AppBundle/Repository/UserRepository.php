<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserRepository.
 */
class UserRepository extends \Datatourisme\Bundle\WebAppBundle\Repository\UserRepository
{
    /**
     * @return QueryBuilder
     */
    public function getIndexQueryBuilder()
    {
        return $this->createQueryBuilder('u')
            ->groupBy('u.id')
            // last login
            ->addSelect('MAX(l.datetime) AS lastLogin')
            ->leftJoin('AppBundle:LoginHistory', 'l', Join::WITH, 'l.user = u')
            // organisations
            ->leftJoin('AppBundle:Organization', 'o', Join::WITH, 'o = u.organization')
            ->addGroupBy('o.id');
    }

}
