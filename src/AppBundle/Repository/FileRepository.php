<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Process;
use Doctrine\ORM\EntityRepository;

/**
 * Class FileRepository.
 */
class FileRepository extends EntityRepository
{
    /**
     * Get total file size.
     *
     * @param Process $process
     *
     * @return int
     */
    public function getTotalSize(Process $process)
    {
        $builder = $this->createQueryBuilder('f')
            ->select('SUM(f.size) AS totalSize')
            ->where('f.process = :process')
            ->setParameter('process', $process);
        $result = $builder->getQuery()->getResult();
        if (isset($result[0]) && isset($result[0]['totalSize'])) {
            return $result[0]['totalSize'];
        }

        return 0;
    }

    /**
     * Get total file Items.
     *
     * @param Process $process
     *
     * @return int
     */
    public function getTotalPOI(Process $process)
    {
        $sql = 'SELECT sum((stats->>\'total\')::int) AS totalPOI FROM file as f WHERE f.process_id = :process';
        $params = array(
            'process' => addslashes($process->getId()),
        );
        $result = $this->getEntityManager()->getConnection()->executeQuery($sql, $params)->fetchAll();

        if (isset($result[0]) && isset($result[0]['totalpoi'])) {
            return $result[0]['totalpoi'];
        }

        return 0;
    }
}
