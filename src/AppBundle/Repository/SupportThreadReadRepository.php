<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\SupportThread;
use AppBundle\Entity\SupportThreadRead;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class SupportThreadReadRepository.
 */
class SupportThreadReadRepository extends EntityRepository
{
    /**
     * Creates or updates last read entry for given thread and user.
     *
     * @param SupportThread $thread
     * @param User          $user
     *
     * @return SupportThreadRead
     */
    public function updateLastRead(SupportThread $thread, User $user): SupportThreadRead
    {
        $em = $this->getEntityManager();
        $lastRead = $this->findOneBy(['user' => $user, 'thread' => $thread]);

        if (!$lastRead) {
            $lastRead = new SupportThreadRead();
            $lastRead->setUser($user);
            $lastRead->setThread($thread);
        }
        $lastRead->setDate(new \DateTime());
        $em->persist($lastRead);
        $em->flush();

        return $lastRead;
    }
}
