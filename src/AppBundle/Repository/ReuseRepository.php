<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class ReuseRepository.
 */
class ReuseRepository extends EntityRepository
{
    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /** @var TokenStorageInterface $tokenStorage */
    protected $tokenStorage;

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker($authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage($tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param $filters
     *
     * @return array
     */
    public function getResourcesByPoi($filters = [])
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $qb = $this->createQueryBuilder('r');
        $qb->select('distinct r.poiType, sum(r.poiNb) as nbPoi')
            ->groupBy('r.poiType');

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN', $user)) {
            $qb->where('r.organization = :organization');
            $qb->setParameter('organization', $user->getOrganization());
        }

        if(!empty($filters['creator'])) {
            $qb->andWhere('r.poiCreator = :creator');
            $qb->setParameter('creator', $filters['creator']);
        }

        $result = $qb->getQuery()->getResult();
        $total = 0;
        if (count($result)) {
            foreach ($result as $res) {
                $total += $res['nbPoi'];
            }
        }

        return [
            'result' => $result,
            'total' => $total
        ];
    }

    /**
     * @return QueryBuilder
     */
    public function getPoiByApplicationQueryBuilder()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $qb = $this->createQueryBuilder('r');
        $qb->select('r.appId, r.appName, r.appDesc, r.appUrl, r.consumerId, r.consumerName, r.consumerUrl, sum(r.poiNb) as nbPoi')
            ->groupBy('r.appId, r.appName, r.appDesc, r.appUrl, r.consumerId, r.consumerName, r.consumerUrl');

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN', $user)) {
            $qb->where('r.organization = :organization');
            $qb->setParameter('organization', $user->getOrganization());
        }

        return $qb;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $qb = $this->createQueryBuilder('r');
        $qb->select('COUNT(DISTINCT r.appConsumerId)');

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN', $user)) {
            $qb->where('r.organization = :organization');
            $qb->setParameter('organization', $user->getOrganization());
        }

        return $qb->getQuery()->getSingleScalarResult();
    }


    /**
     *
     */
    public function getCreatorChoices()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $qb = $this->createQueryBuilder('r');
        $qb->select('DISTINCT r.poiCreator');
        $qb->where('r.poiCreator IS NOT NULL');

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN', $user)) {
            $qb->andWhere('r.organization = :organization');
            $qb->setParameter('organization', $user->getOrganization());
        }
        $qb->orderBy('r.poiCreator');
        $result = $qb->getQuery()->getResult();
        $choices = array_map(function($r) { return $r['poiCreator']; }, $result);
        return array_combine($choices, $choices);
    }
}
