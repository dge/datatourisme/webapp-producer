<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Flux;
use AppBundle\Entity\Organization;
use Doctrine\ORM\QueryBuilder;

/**
 * Class FluxStatsEntryRepository.
 */
class FluxStatsEntryRepository extends AbstractStatsEntryRepository
{
    const NO_ENTRY_MSG = 'Stats entry not found for given day.';

    /**
     * @param Flux           $flux
     * @param \DateTime|null $date
     * @param bool           $getQb
     *
     * @return array|QueryBuilder
     */
    public function findByFlux(Flux $flux, \DateTime $date = null, bool $getQb = false)
    {
        $qb = $this->createQueryBuilder('entry');

        $qb->where('entry.flux = :flux');
        $qb->setParameter('flux', $flux);

        if ($date) {
            $qb->andWhere('entry.date = :date');
            $qb->setParameter('date', $date);
        }

        return $getQb ? $qb : $qb->getQuery()->getResult();
    }

    /**
     * @param Organization   $organization
     * @param \DateTime|null $date
     * @param bool           $getQb
     *
     * @return array|QueryBuilder
     */
    public function findByOrganization(Organization $organization, \DateTime $date = null, bool $getQb = false)
    {
        $qb = $this->createQueryBuilder('entry');

        $qb->where('$entry.organization = :organization');
        $qb->setParameter('$organization', $organization);

        if ($date) {
            $qb->andWhere('entry.date = :date');
            $qb->setParameter('date', $date);
        }

        return $getQb ? $qb : $qb->getQuery()->getResult();
    }

    /**
     * @param string            $status
     * @param \DateTime|null    $date
     * @param Organization|null $organization
     *
     * @return int|QueryBuilder
     */
    public function getCountByFluxStatus(string $status, \DateTime $date = null,
                                         Organization $organization = null, bool $getQb = false
    ) {
        $qb = $this->createQueryBuilder('entry');

        $qb->select('COUNT(entry)');

        $qb->where('entry.fluxStatus = :status');
        $qb->setParameter('status', $status);

        $qb->andWhere('entry.date = :day');
        $qb->setParameter('day', $date ?: new \DateTime());

        if ($organization === null) {
            $this->restrictQueryBuilder($qb);
        } else {
            $this->checkOrganizationAccess($organization);
        }

        return $getQb ? $qb : $qb->getQuery()->getSingleScalarResult();
    }
}
