<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Process;
use AppBundle\Entity\RdfResource;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class FluxRepository.
 */
class FluxRepository extends EntityRepository
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker($authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * This SQL snippet is re-used in the filter forms.
     *
     * @var string
     */
    public static $statusCaseSqlStatement = 'CASE
        WHEN (flux.status = :prod_status)
        THEN process.status
        ELSE flux.status
    END';

    /**
     * Time travelling findAll().
     *
     * @param \DateTime|null $date
     *
     * @return array
     */
    public function findAllLivingOnDate(\DateTime $date = null): array
    {
        $qb = $this->createQueryBuilder('flux');

        $qb->where('DATE(flux.createdAt) <= DATE(:date)');
        $qb->setParameter('date', $date ?: new \DateTime());

        return $qb->getQuery()->getResult();
    }

    /**
     * @return int
     */
    public function getProductionCount(): int
    {
        $qb = $this->createQueryBuilder('flux');
        $qb->select('COUNT (flux)');

        $qb->where('flux.status in (:s)');
        $qb->setParameter('s', Flux::productionStatusList());

        if (false === $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $qb->andWhere('flux.organization = :o');
            $qb->setParameter('o', $this->tokenStorage->getToken()->getUser()->getOrganization());
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return mixed
     */
    public function getAllowedFluxIds()
    {
        $qb = $this->createQueryBuilder('flux');
        $qb->select('flux.id');
        $qb->groupBy('flux.id');

        // limit to user organization
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $user = $this->tokenStorage->getToken()->getUser();
            $qb->andWhere('flux.organization = :organization');
            $qb->setParameter('organization', $user->getOrganization());
        }

        return $qb->getQuery()->execute();
    }

    public function hasPendingRevision(Flux $flux): bool
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->from(Flux::class, 'flux');
        $qb->where('flux = :flux');
        $qb->setParameter('flux', $flux);

        $qb->leftJoin('flux.alignmentRuleset', 'alignmentRuleset');
        $qb->leftJoin(
            'alignmentRuleset.revisions',
            'draft_revision',
            Join::WITH,
            'draft_revision.version = alignmentRuleset.draftVersion'
        );

        $qb->addSelect('
            CASE
                WHEN draft_revision.status = :pending_status
                THEN true
                ELSE false
            END
        ');
        $qb->setParameter('pending_status', AlignmentRulesetRevision::STATUS_PENDING);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return QueryBuilder
     */
    public function createProductionQueryBuilder(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('flux');
        $qb->groupBy('flux.id');

        // join last process
        $qb->addSelect('process.id as last_process_id');
        $qb->addSelect('process.createdAt as last_process_date');
        $qb->addSelect($this::$statusCaseSqlStatement.' AS status');
        $qb->setParameter('prod_status', Flux::STATUS_PRODUCTION);
        $qb->leftJoin(Process::class, 'process', 'WITH', 'process.id = (
            SELECT MAX(p.id) FROM AppBundle:Process AS p WHERE p.flux = flux
        )');
        $qb->addGroupBy('process.id');

        // poi count
        $qb->addSelect('COUNT(DISTINCT poi.id) AS nb_rdf_resources');
        $qb->leftJoin(RdfResource::class, 'poi', Join::WITH, 'poi.flux = flux AND poi.status = :published_status');
        $qb->setParameter('published_status', RdfResource::STATUS_PUBLISHED);

        // limit by flux status
        $qb->andWhere('flux.status IN (:status_list)');
        $qb->setParameter('status_list', Flux::productionStatusList());

        // ____ HAS PENDING REVISION
        $qb->addSelect('
            CASE
                WHEN draft_revision.status = :pending_status
                THEN true
                ELSE false
            END
            AS has_pending_revision
        ');
        $qb->setParameter('pending_status', AlignmentRulesetRevision::STATUS_PENDING);
        $qb->leftJoin('flux.alignmentRuleset', 'alignmentRuleset');
        $qb->leftJoin(
            'alignmentRuleset.revisions',
            'draft_revision',
            Join::WITH,
            'draft_revision.version = alignmentRuleset.draftVersion'
        );
        $qb->addGroupBy('draft_revision.id');
        // ________

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            // add organization join
            $qb->leftJoin('flux.organization', 'org');
            $qb->addGroupBy('org.id');
        } else {
            // limit to user organization
            $user = $this->tokenStorage->getToken()->getUser();
            $qb->andWhere('flux.organization = :organization');
            $qb->setParameter('organization', $user->getOrganization());
        }

        return $qb;
    }

    /**
     * @return QueryBuilder
     */
    public function createConceptionQueryBuilder(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('flux');
        $qb->groupBy('flux.id');

        $qb->leftJoin('flux.alignmentRuleset', 'alignmentRuleset');
        $qb->leftJoin(AlignmentRulesetRevision::class, 'draft_revision', Join::WITH,
            'draft_revision.alignmentRuleset = alignmentRuleset
            AND draft_revision.version = alignmentRuleset.draftVersion'
        );
        $qb->addGroupBy('draft_revision.id');

        $qb->addSelect('draft_revision.status AS draft_revision_status');
        $qb->where('draft_revision.status = :pending_status');
        $qb->setParameter('pending_status', AlignmentRulesetRevision::STATUS_PENDING);

        if (false === $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $qb->orWhere('draft_revision.status IN (:status_list)');
            $qb->setParameter('status_list', Flux::conceptionStatusList());

            $qb->orWhere('flux.alignmentRuleset IS NULL');

            $qb->andWhere('flux.organization = :organization');
            $qb->setParameter('organization', $this->tokenStorage->getToken()->getUser()->getOrganization());
        } else {
            $qb->leftJoin('flux.organization', 'organization');
            $qb->addGroupBy('organization.id');
        }

        return $qb;
    }
}
