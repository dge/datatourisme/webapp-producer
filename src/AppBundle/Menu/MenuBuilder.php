<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Menu;

use AppBundle\Thesaurus\Ontology;
use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, AuthorizationChecker $authorizationChecker)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        // Dashboard
        $menu->addChild('Tableau de bord', array(
            'route' => 'homepage',
            'extras' => array(
                'icon' => 'fa fa-dashboard',
            ),
        ));

        // Thesaurus
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $thesaurus = $menu->addChild('Thésaurus', array(
                'uri' => '#',
                'extras' => array(
                    'uri_prefix' => '/thesaurus',
                    'icon' => 'fa fa-list',
                ),
            ));
            $thesaurus->addChild('Types de POI', array('route' => 'thesaurus.align', 'routeParameters' => array('rdfClass' => 'PointOfInterest', 'type' => Ontology::CLASSES)));
            $thesaurus->addChild(null, array('attributes' => array('class' => 'divider')));
            $thesaurus->addChild('Audiences', array('route' => 'thesaurus.align', 'routeParameters' => array('rdfClass' => 'Audience', 'type' => Ontology::INDIVIDUALS)));
            $thesaurus->addChild('Équipements', array('route' => 'thesaurus.align', 'routeParameters' => array('rdfClass' => 'Amenity', 'type' => Ontology::INDIVIDUALS)));
            $thesaurus->addChild('Thèmes', array('route' => 'thesaurus.align', 'routeParameters' => array('rdfClass' => 'Theme', 'type' => Ontology::INDIVIDUALS)));
            $thesaurus->addChild('Géographique', array('route' => 'thesaurus.align', 'routeParameters' => array('rdfClass' => 'France', 'type' => Ontology::GEOGRAPHIC)));
            $thesaurus->addChild('Autres', array('route' => 'thesaurus.align', 'routeParameters' => array('rdfClass' => 'NamedIndividual', 'type' => Ontology::OTHER)));
        }

        // Flux
        $flux = $menu->addChild('Flux', array(
            'uri' => '#',
            'extras' => array(
                'uri_prefix' => ['/flux', '/processes'],
                'icon' => 'fa fa-rss',
            ),
        ));
        $flux->addChild('En production', array('route' => 'flux.production'));
        $flux->addChild(
            $this->authorizationChecker->isGranted('ROLE_ADMIN') ? 'En attente de validation' : 'En cours de conception',
            array('route' => 'flux.conception')
        );
        $flux->addChild('Traitements', array('route' => 'processes'));

        // Exploitation
        $exploit = $menu->addChild('Exploitation', array(
            'uri' => '#',
            'extras' => array(
                'uri_prefix' => '/exploitation',
                'icon' => 'fa fa-recycle',
            ),
        ));
        $exploit->addChild('Réutilisations', array('route' => 'reuse.index'));
        $exploit->addChild('Traductions', array('route' => 'translation.index'));

        // Administration
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $admin = $menu->addChild('Administration', array(
                'uri' => '#',
                'extras' => array(
                    'uri_prefix' => '/admin',
                    'icon' => 'fa fa-cogs',
                ),
            ));
            $admin->addChild('Utilisateurs', array('route' => 'user.index'));
            $admin->addChild('Structures', array('route' => 'organization.index'));
        }

        // Aide
        $help = $menu->addChild('Aide', array(
            'uri' => '#',
            'extras' => array(
                'uri_prefix' => '/help',
                'icon' => 'fa fa-question-circle',
            ),
        ));
        $help->addChild('Actualités', array('route' => 'news.index'));
        $help->addChild('Foire aux questions', array('route' => 'faq.index'));
        $help->addChild('Centre de support', array('route' => 'support.index'));

        return $menu;
    }
}
