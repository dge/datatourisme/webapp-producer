<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/notification")
 * @Security("has_role('ROLE_USER')")
 */
class NotificationController extends Controller
{
    /**
     * @Route("", name="notification.index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $days = 15;
        $seconds = $days * 24 * 60 * 60;
        $params = $this->getDates($seconds);
        $dates = $params['dates'];

        $this->getUser()->setNotificationReadTime(new \DateTime());
        $this->getDoctrine()->getManager()->flush();

        return $this->render('notification/index.html.twig', [
            'dates' => $dates,
            'days' => $days,
        ]);
    }

    /**
     * @Route("/dropdown-menu", name="notification.dropdown-menu", options={ "expose": true })
     * @Security("is_granted('ROLE_USER')")
     *
     * @return Response
     */
    public function dropdownMenuAction()
    {
        $days = 15;
        $seconds = $days * 24 * 60 * 60;
        $user = $this->getUser();
        if ($this->getUser()->getNotificationReadTime()) {
            $now = new \DateTime();
            $diffSeconds = $now->getTimestamp() - $this->getUser()->getNotificationReadTime()->getTimestamp();
            $seconds = ($diffSeconds > $seconds) ? $seconds : $diffSeconds;
        }
        $params = $this->getDates($seconds);
        $dates = $params['dates'];
        $notificationsCount = $params['count'];

        foreach ($dates as $keyDate => $date) {
            /** @var Notification $notification */
            foreach ($date as $keyNotification => $notification) {
                if ($this->getUser()->getNotificationReadTime() && $this->getUser()->getNotificationReadTime() > $notification->getDateTime()) {
                    unset($dates[$keyDate][$keyNotification]);
                    --$notificationsCount;
                }
            }
        }

        return $this->render('notification/dropdown-menu.html.twig', [
            'notificationCount' => $notificationsCount,
            'dates' => $dates,
            'days' => $days,
        ]);
    }

    /**
     * @param int $seconds
     *
     * @return array
     */
    protected function getDates($seconds)
    {
        $repository = $this->getDoctrine()->getRepository(Notification::class);
        $notifications = $repository->findByUser($this->getUser(), "$seconds seconds");
        $count = count($notifications);

        $dates = array();
        /** @var Notification $notification */
        foreach ($notifications as $notification) {
            $date = $notification->getDateTime()->format('Y-m-d H:i:s');
            $dates[$date][] = $notification;
        }

        return ['count' => $count, 'dates' => $dates];
    }
}
