<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Process;
use AppBundle\Form\Filter\ProcessFilter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/processes")
 */
class ProcessController extends Controller
{
    /**
     * @Route ("", name="processes")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function historyAction(Request $request): Response
    {
        $pageSize = 15;
        $paginationOptions = array(
            'defaultSortFieldName' => 'process.createdAt',
            'defaultSortDirection' => 'desc',
            'distinct' => false,
        );

        $processRepository = $this->get('app.repository.process');

        $qb = $processRepository->createPaginationQueryBuilder();

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, ProcessFilter::class)
            ->setQueryHint($processRepository->getCount())
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->handleRequest($request);

        return $this->render('flux/processes.html.twig', array(
            'list' => $list,
            'items' => $list->getItems(),
            'page_size' => $pageSize,
        ));
    }
}
