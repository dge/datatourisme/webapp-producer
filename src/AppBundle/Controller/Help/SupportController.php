<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Help;

use AppBundle\Entity\Organization;
use AppBundle\Entity\SupportPost;
use AppBundle\Entity\SupportThread;
use AppBundle\Entity\SupportThreadRead;
use AppBundle\Entity\User;
use AppBundle\Form\Type\SupportPostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/help/support")
 */
class SupportController extends Controller
{
    /**
     * @Route("", name="support.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $restricted = $this->isGranted('support.see.all_threads') === false;
        $user = $this->getUser();

        $em = $this->get('doctrine.orm.entity_manager');
        $threadReadRepository = $em->getRepository(SupportThreadRead::class);
        $threadRepository = $em->getRepository(SupportThread::class);
        $postRepository = $em->getRepository(SupportPost::class);

        $query = $threadRepository->getQueryLastPostDates($user, $restricted);

        $pagination = $this->get('knp_paginator');
        $pagination = $pagination->paginate(
            $query,
            $request->query->getInt('page', 1),
            15
        );

        $threads = array();
        $items = $pagination->getItems();
        $count = count($items);
        for ($i = 0; $i < $count; ++$i) {
            $threads[$i]['thread'] = $items[$i];
            $threads[$i]['last_read'] = $threadReadRepository->findOneBy(['user' => $user, 'thread' => $items[$i]]);
            $threads[$i]['first_post'] = $postRepository->findOneBy(['thread' => $items[$i]], ['createdAt' => 'ASC']);
            $threads[$i]['last_post'] = $postRepository->findOneBy(['thread' => $items[$i]], ['createdAt' => 'DESC']);
        }

        $newCount = $threadRepository->countUnreadThreads($user, $restricted);

        return $this->render('help/support/index.html.twig', array(
            'threads' => $threads,
            'pagination' => $pagination,
            'new_count' => $newCount,
        ));
    }

    /**
     * @Route("/thread/{id}", name="support.thread", requirements={"id": "\d+"})
     * @Security("is_granted('support.see.thread', thread)")
     *
     * @param SupportThread $thread
     * @param Request       $request
     *
     * @return Response
     */
    public function threadAction(SupportThread $thread, Request $request): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $post = new SupportPost();
        $post->setThread($thread);

        $form = $this->createForm(SupportPostType::class, $post);
        if ($this->isGranted('support.add.post', $thread)) {
            $post->setThread($thread);
            $post->setUser($this->getUser());

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($post);
                $em->flush();
                $this->addFlash('success', 'Le message a bien été ajouté à la discussion.');
                if ($post->getId()) {
                    $this->newPostMail($post);

                    return $this->redirectToRoute('support.thread', array('id' => $thread->getId(), '_fragment' => $post->getId()));
                }
            }
        }

        $timelines = array();
        $posts = $thread->getPosts();
        $count = count($posts);
        for ($i = 0; $i < $count; ++$i) {
            $date = $posts[$i]->getCreatedAt()->format('dmY');
            $timelines[$date]['posts'][] = $posts[$i];
            $timelines[$date]['date'] = $posts[$i]->getCreatedAt();
        }

        $lastRead = $em->getRepository(SupportThreadRead::class)->updateLastRead($thread, $this->getUser());

        return $this->render('help/support/thread.html.twig', array(
            'thread' => $thread,
            'timelines' => $this->sortTimelines($timelines),
            'last_read' => $lastRead,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/thread/{thread_id}/post/{post_id}/download", name="support.thread.post.download", requirements={"id": "\d+"})
     * @Security("is_granted('support.see.thread', thread)")
     *
     * @ParamConverter("thread", options={"mapping": {"thread_id" : "id"}})
     * @ParamConverter("post", options={"mapping": {"post_id"   : "id"}})
     *
     * @param SupportThread $thread
     * @param SupportPost   $post
     *
     * @return Response
     */
    public function downloadAction(SupportThread $thread, SupportPost $post)
    {
        if ($post->getFile()) {
            $response = new BinaryFileResponse($post->getFile());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $post->getFileName());
            return $response;
        }
        throw $this->createNotFoundException();
    }

    /**
     * @Route("/thread/add/{id}", name="support.thread.add", defaults={"id": null}, requirements={"id": "\d+"})
     * @Security("is_granted('support.add.thread', organization)")
     *
     * @param Organization $organization
     * @param Request      $request
     *
     * @return Response
     */
    public function addThreadAction(Organization $organization = null, Request $request): Response
    {
        $thread = new SupportThread();
        $thread->setOrganization($organization);

        $post = new SupportPost();
        $post->setUser($this->getUser());
        $post->setThread($thread);

        $form = $this->createForm(SupportPostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'La discussion a bien été créée.');
            if ($post->getId()) {
                $this->newPostMail($post);
            }

            return $this->redirectToRoute('support.thread', array('id' => $post->getThread()->getId(), '_fragment' => $post->getId()));
        }

        return $this->render('help/support/add.post.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/dropdown-menu",
     *     name="support.dropdown-menu",
     *     options={ "expose": true }
     * )
     * @Security("is_granted('ROLE_USER')")
     *
     * @return Response
     */
    public function dropdownMenuAction(): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $this->getUser();
        $restricted = $this->isGranted('support.see.all_threads') ? false : true;

        $threadRepository = $em->getRepository(SupportThread::class);
        $postRepository = $em->getRepository(SupportPost::class);

        $posts = array();
        $newMsgCount = $threadRepository->countUnreadThreads($user, $restricted);
        $threads = $threadRepository->findUnreadThreads($user, $restricted, 'DESC', 5);

        $count = count($threads);
        for ($i = 0; $i < $count; ++$i) {
            $posts[] = $postRepository->findOneBy(['thread' => $threads[$i]], ['createdAt' => 'DESC']);
        }

        return $this->render(':help/support:dropdown-menu.html.twig', array(
            'posts' => $posts,
            'new_msg_count' => $newMsgCount,
        ));
    }

    /**
     * Sorts timelines / posts inside timelines by dates (DESC).
     *
     * @param array $timelines
     *
     * @return array
     */
    private function sortTimelines(array $timelines): array
    {
        usort($timelines, function ($a, $b) {
            return $a['date'] < $b['date'] ? -1 : 1;
        });

        $count = count($timelines);
        for ($i = 0; $i < $count; ++$i) {
            usort($timelines[$i]['posts'], function ($a, $b) {
                return date_format($a->getCreatedAt(), 'His') < date_format($b->getCreatedAt(), 'His') ? -1 : 1;
            });
        }

        return $timelines;
    }

    /**
     * Sends mail to defined recipients when new message is posted in Support Center.
     *
     * @param SupportPost $post
     *
     * @return int
     */
    private function newPostMail(SupportPost $post): int
    {
        $user = $post->getUser();
        $organization = $post->getThread()->getOrganization();
        $recipients = $organization->getUsers()->toArray();
        $excludes = array();

        if (!$this->isGranted('ROLE_ADMIN', $user)) {
            // if the author is not an admin, notify all admin and exclude it self
            $recipients = array_merge(
                $this->get('app.repository.user')->findByRole('ROLE_ADMIN'),
                $recipients
            );
            $excludes = array($user);
        }

        return $this->get('webapp.mailer')->send('support.new_post', $recipients, array(
            'post' => $post,
        ), $excludes);
    }
}
