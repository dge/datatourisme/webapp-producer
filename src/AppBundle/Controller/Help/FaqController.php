<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Help;

use AppBundle\Entity\Faq;
use AppBundle\Entity\FaqTheme;
use AppBundle\Form\Type\FaqSearchType;
use AppBundle\Form\Type\FaqType;
use AppBundle\Notification\Type\FaqCreateType;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/help/faq")
 */
class FaqController extends Controller
{
    /**
     * @Route("", name="faq.index")
     * @Route("/{id}", name="faq.index.theme", defaults={"id": null}, requirements={"id": "\d+"})
     *
     * @param FaqTheme $faqTheme
     * @param Request  $request
     *
     * @return Response
     */
    public function indexAction(FaqTheme $faqTheme = null, Request $request): Response
    {
        $form = $this->createForm(FaqSearchType::class, null, [
            'action' => $this->get('router')->generate('faq.index.theme'),
            'method' => 'GET',
        ]);
        $form->handleRequest($request);

        $search = $form->getData()['keywords'] ?: $request->query->get('faq_search')['keywords'] ?: null;

        $sql = 'SELECT * FROM faq';
        if ($search) {
            $sql .= ' WHERE to_tsvector(\'french\', question ||\' \'|| answer) @@ to_tsquery(\'french\', ?)';
            $sql .= ' ORDER BY ts_rank_cd(to_tsvector(\'french\', question ||\' \'|| answer), to_tsquery(\'french\',?)) DESC';
        } else {
            $sql .= ' ORDER BY position';
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(Faq::class, 'f');
        $query = $em->createNativeQuery($sql, $rsm);
        if ($search) {
            $query->setParameters(["'".addslashes($search)."'", "'".addslashes($search)."'"]);
        }
        $faqs = $query->getResult();

        $sections = array();
        $count = count($faqs);
        for ($i = 0; $i < $count; ++$i) {
            $id = $faqs[$i]->getTheme()->getId();
            $url['id'] = $faqs[$i]->getTheme()->getId();
            $url[$form->getName()] = $form->getData();
            $sections[$id]['url'] = $this->get('router')->generate('faq.index.theme', $url);
            $sections[$id]['theme'] = $faqs[$i]->getTheme();
            $sections[$id]['faqs'][] = $faqs[$i];
        }
        uasort($sections, function ($a, $b) {
            $aCount = count($a['theme']->getFaqs());
            $bCount = count($b['theme']->getFaqs());

            return ($aCount > $bCount) ? -1 : 1;
        });

        return $this->render('help/faq/index.html.twig', array(
            'form' => $form->createView(),
            'search' => $search,
            'active_theme' => $faqTheme ?? reset($sections)['theme'],
            'sections' => $sections,
        ));
    }

    /**
     * @Route("/add/{id}", name="faq.add", defaults={"id": null}, requirements={"id": "\d+"})
     * @Security("is_granted('faq.add')")
     *
     * @param FaqTheme $faqTheme
     * @param Request  $request
     *
     * @return Response
     */
    public function addAction(FaqTheme $faqTheme = null, Request $request): Response
    {
        $faq = new Faq();
        if ($faqTheme) {
            $faq->setTheme($faqTheme);
        }

        return $this->editAction($faq, $request);
    }

    /**
     * @Route("/edit/{id}", name="faq.edit", requirements={"id": "\d+"})
     * @Security("is_granted('faq.edit', faq)")
     *
     * @param Faq     $faq
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Faq $faq, Request $request): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $new = empty($faq->getId());
        $form = $this->createForm(FaqType::class, $faq);
        $initialCount = $faq->getTheme() ? count($faq->getTheme()->getFaqs()) : null;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $faqThemeRepository = $em->getRepository(FaqTheme::class);

            $initialFaqData = $em->getUnitOfWork()->getOriginalEntityData($faq);
            $initialTheme = null;
            if (!empty($initialFaqData)) {
                $initialTheme = $faqThemeRepository->find($initialFaqData['theme']->getId());
                if ($initialTheme->getId() !== $faq->getTheme()->getId()) {
                    if ($initialCount === 1) {
                        $em->remove($initialTheme);
                    }
                }
            }
            $theme = $faq->getTheme();
            $faqs = $theme->getFaqs()->toArray();
            $count = count($faqs);
            if ($faq->getPosition() === null || ($initialTheme && $theme !== $initialTheme)) {
                $faq->setPosition($count > 0 ? $faqs[$count - 1]->getPosition() + 2 : 1);
            }
            $em->persist($faq);
            $em->flush();
            $faqThemeRepository->sortTheme($theme);
            if ($initialTheme && $theme !== $initialTheme) {
                $faqThemeRepository->sortTheme($initialTheme);
            }
            $this->addFlash('success', $new ? 'La question a bien été ajoutée.' : 'La question a bien été modifiée.');

            if($new) {
                $this->get('notifier')->dispatch(FaqCreateType::class, $faq);
            }

            return $this->redirectToRoute('faq.index.theme', ['id' => $theme->getId()]);
        }

        return $this->render('help/faq/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/remove/{id}", name="faq.remove", requirements={"id": "\d+"})
     * @Security("is_granted('faq.remove', faq)")
     *
     * @param Faq     $faq
     * @param Request $request
     *
     * @return Response
     */
    public function removeAction(Faq $faq, Request $request): Response
    {
        if ($request->getMethod() === 'POST') {
            $em = $this->getDoctrine()->getManager();
            $theme = $faq->getTheme();
            if (count($theme->getFaqs()) < 2) {
                $em->remove($theme);
            }
            $em->remove($faq);
            $em->flush();

            if ($theme) {
                $em->getRepository(FaqTheme::class)->sortTheme($theme);
            }

            $this->addFlash('success', 'La question a bien été supprimée.');

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('faq.index'),
            ], 200);
        }

        return $this->render('help/faq/remove.html.twig', array(
            'faq' => $faq,
        ));
    }

    /**
     * @Route("/reorder/{id}", name="faq.reorder", requirements={"id": "\d+"})
     * @Security("is_granted('faq.reorder', faq)")
     *
     * @param Faq     $faq
     * @param Request $request
     *
     * @return Response
     */
    public function reorderAction(Faq $faq, Request $request): Response
    {
        if ($request->getMethod() === 'POST') {
            $em = $this->get('doctrine.orm.entity_manager');
            $theme = $faq->getTheme();
            $em->getRepository(FaqTheme::class)->sortTheme($theme);
            $action = $request->request->get('way') === 'up' ? -1 : +1;
            $pos = $faq->getPosition();
            $pairedFaq = $this
                ->getDoctrine()
                ->getRepository(Faq::class)
                ->findOneBy(['theme' => $faq->getTheme(), 'position' => $pos + $action]);
            $faq->setPosition($pos + $action);
            $pairedFaq->setPosition($pos);
            $em->flush();
        }

        return $this->redirectToRoute('faq.index.theme', array(
            'id' => $faq->getTheme()->getId(),
        ));
    }
}
