<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Help;

use AppBundle\Entity\Faq;
use AppBundle\Entity\FaqTheme;
use AppBundle\Entity\News;
use AppBundle\Form\Type\NewsType;
use AppBundle\Notification\Type\NewsCreateType;
use AppBundle\Repository\NewsRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/help/news")
 */
class NewsController extends Controller
{
    /**
     * @Route("", name="news.index")
     * @Route("/{month}", name="news.index.month", defaults={"month": null}, requirements={"month": "\d{4}-\d{2}"})
     *
     * @param string $month
     * @param Request  $request
     *
     * @return Response
     */
    public function indexAction(string $month = null, Request $request): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');

        // get month/year distribution
        $sql = "SELECT to_char( date, 'YYYY-MM'), count(id) FROM news GROUP BY to_char( date, 'YYYY-MM') ORDER BY to_char( date, 'YYYY-MM') DESC";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $months = $stmt->fetchAll(\PDO::FETCH_COLUMN|\PDO::FETCH_UNIQUE);
        $total = array_sum($months);

        /** @var NewsRepository $repository */
        $repository = $this->getDoctrine()->getRepository(News::class);
        $news = [];
        if (!empty($month)) {
            list($y, $m) = explode("-", $month);
            $news = $repository->findByYearMonth($y, $m);
        } else {
            $news = $repository->findBy([], ['date' => 'DESC', 'id' => 'DESC']);
        }

        return $this->render('help/news/index.html.twig', array(
            'months' => $months,
            'active_month' => $month,
            'total' => $total,
            'news_list' => $news
        ));
    }

    /**
     * @Route("/news/{id}", name="news.detail", requirements={"id": "\d+"})
     *
     * @param News $news
     * @param Request       $request
     *
     * @return Response
     */
    public function detailAction(News $news, Request $request)
    {
        return $this->render('help/news/detail.html.twig', array(
            'news' => $news
        ));
    }

    /**
     * @Route("/add", name="news.add")
     * @Security("is_granted('news.add')")
     *
     * @param Request  $request
     *
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        $news = new News();
        return $this->editAction($news, $request);
    }

    /**
     * @Route("/edit/{id}", name="news.edit", requirements={"id": "\d+"})
     * @Security("is_granted('news.edit', news)")
     *
     * @param News     $news
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(News $news, Request $request): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $new = empty($news->getId());
        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($news);
            if ($form->has('removeFile') && $form->get('removeFile')->getData()) {
                $news->removeUpload();
            }
            $em->flush();
            $this->addFlash('success', $new ? "L'actualité a bien été ajoutée." : "L'actualité a bien été modifiée.");
            if($new) {
                $this->get('notifier')->dispatch(NewsCreateType::class, $news);
            }
            return $this->redirectToRoute('news.index', ['id' => $news->getId()]);
        }

        return $this->render('help/news/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/download", name="news.download", requirements={"id": "\d+"})
     *
     * @param News $news
     *
     * @return Response
     */
    public function downloadAction(News $news)
    {
        if ($news->getFile()) {
            $response = new BinaryFileResponse($news->getFile());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $news->getFileName());
            return $response;
        }
        throw $this->createNotFoundException();
    }

    /**
     * @Route("/remove/{id}", name="news.remove", requirements={"id": "\d+"})
     * @Security("is_granted('news.remove', news)")
     *
     * @param News     $news
     * @param Request $request
     *
     * @return Response
     */
    public function removeAction(News $news, Request $request): Response
    {
        if ($request->getMethod() === 'POST') {
            $em = $this->getDoctrine()->getManager();
            $em->remove($news);
            $em->flush();

            $this->addFlash('success', "L'actualité a bien été supprimée.");

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('news.index'),
            ], 200);
        }

        return $this->render('help/news/remove.html.twig', array(
            'news' => $news,
        ));
    }
}
