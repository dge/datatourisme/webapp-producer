<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Anomaly;
use AppBundle\Entity\File;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Process;
use AppBundle\Entity\RdfResource;
use AppBundle\Form\Filter\ProcessFilter;
use AppBundle\Form\Filter\RdfResourceFilter;
use AppBundle\Form\Type\FluxDetailType;
use AppBundle\JavaWorker\JavaWorkerClient;
use GuzzleHttp\Exception\RequestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * @Route("/flux/{id}", requirements={"id": "\d+"})
 */
class FluxDetailController extends Controller
{
    /**
     * Report tab.
     *
     * @Route("", name="flux.detail")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux $flux
     *
     * @return Response
     */
    public function reportAction(Flux $flux): Response
    {
        $d = $this->getDoctrine();
        if ($flux->getStatus() === Flux::STATUS_DRAFT || $flux->getLastActiveProcess() === null) {
            return $this->redirectToRoute('flux.ruleset.index', ['id' => $flux->getId()]);
        }

        return $this->render('flux/detail/report.html.twig', array(
            'flux' => $flux,
            'total_files_size' => $d->getRepository(File::class)->getTotalSize($flux->getLastActiveProcess()),
            'total_files_poi' => $d->getRepository(File::class)->getTotalPOI($flux->getLastActiveProcess()),
            'resources_by_poi' => $d->getRepository(RdfResource::class)->getByPoi($flux),
            'poi_count' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux),
            'poi_published' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux, RdfResource::STATUS_PUBLISHED),
            'anomaly_count' => $d->getRepository(Anomaly::class)->getCountByFlux($flux),
            'has_pending_revision' => $this->get('app.repository.flux')->hasPendingRevision($flux),
        ));
    }

    /**
     * Download last source.
     *
     * @Route("/download/source", name="flux.download.source")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux $flux
     *
     * @return Response
     */
    public function downloadSourceAction(Flux $flux): Response
    {
        return $this->get('app.java_worker.client')->downloadSource($flux);
    }

    /**
     * Get current status (for Ajax call).
     *
     * @Route("/status", name="flux.status")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux $flux
     *
     * @return Response
     */
    public function statusAction(Flux $flux): Response
    {
        $process = $flux->getLastActiveProcess();
        $status = [
            'process' => $process->getId(),
            'status' => $process->getStatus(),
        ];

        if ($process->getStatus() == Process::STATUS_RUNNING) {
            $status['progress'] = $process->getProgress();
            $status['progression'] = $process->getProgression();

            $javaWorkerClient = $this->get('app.java_worker.client');
            try {
                $log = $javaWorkerClient->jobLog($flux, $process->getId());
                $status['log'] = $log;
            } catch (RequestException $e) {
                JavaWorkerClient::handleRequestException($e);
            }
        }

        return JsonResponse::create($status);
    }

    /**
     * Parameters tab.
     *
     * @Route("/parameters", name="flux.parameters")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function parametersAction(Request $request, Flux $flux): Response
    {
        $em = $this->getDoctrine()->getManager();

        // clone flux to avoid context change
        $fluxClone = clone $flux;

        $form = $this->createForm(FluxDetailType::class, $flux);

        $form = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && !$this->isGranted('ROLE_ADMIN')) {
            $em->flush();
            $this->addFlash('success', 'Le flux a été mis à jour.');

            return $this->redirectToRoute('flux.parameters', array('id' => $flux->getId()));
        }

        return $this->render('flux/detail/parameters.html.twig', array(
            'form' => $form->createView(),
            'flux' => $fluxClone,
            'poi_count' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux),
            'anomaly_count' => $em->getRepository(Anomaly::class)->getCountByFlux($flux),
            'has_pending_revision' => $this->get('app.repository.flux')->hasPendingRevision($flux),
        ));
    }

    /**
     * List of POI.
     *
     * @Route("/resources", name="flux.resources")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function resourcesAction(Flux $flux, Request $request): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $lastActiveProcess = $flux->getLastActiveProcess();
        if ($lastActiveProcess === null || !$this->isGranted('flux.see.resources', $lastActiveProcess)) {
            throw $this->createAccessDeniedException();
        }

        $pageSize = 15;
        $paginationOptions = array(
            'defaultSortFieldName' => 'rdf_resource.city',
            'defaultSortDirection' => 'asc',
        );

        $qb = $em->getRepository(RdfResource::class)->createByFluxQueryBuilder($flux);
        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, RdfResourceFilter::class, null, ['flux' => $flux])
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->handleRequest($request);

        return $this->render('flux/detail/resources.html.twig', array(
            'flux' => $flux,
            'list' => $list,
            'items' => $list->getItems(),
            'page_size' => $pageSize,
            'poi_count' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux),
            'anomaly_count' => $em->getRepository(Anomaly::class)->getCountByProcess($lastActiveProcess),
            'has_pending_revision' => $this->get('app.repository.flux')->hasPendingRevision($flux),
        ));
    }

    /**
     * (Modal)
     * List of anomalies for a POI.
     *
     * @Route("/resources/{resource_id}/anomalies", name="flux.resources.anomalies", requirements={"resource_id": "\d+"})
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux        $flux
     * @param RdfResource $rdfResource
     *
     * @ParamConverter("flux", class="AppBundle:Flux")
     * @ParamConverter("rdfResource", class="AppBundle:RdfResource", options={"id" = "resource_id"})
     *
     * @return Response
     */
    public function resourcesAnomaliesAction(Flux $flux, RdfResource $rdfResource): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');

        if (!$this->isGranted('flux.see.resources', $flux->getLastActiveProcess())) {
            throw $this->createAccessDeniedException();
        }

        $anomalies = $em->getRepository(Anomaly::class)->findBy(['rdfResource' => $rdfResource], ['breaker' => 'desc']);

        return $this->render('flux/detail/resources.anomalies.html.twig', array(
            'flux' => $flux,
            'rdf_resource' => $rdfResource,
            'anomalies' => $anomalies,
        ));
    }

    /**
     * List of anomalies.
     *
     * @Route("/anomalies", name="flux.anomalies")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function anomaliesAction(Flux $flux, Request $request): Response
    {
        $lastActiveProcess = $flux->getLastActiveProcess();
        if ($lastActiveProcess === null || !$this->isGranted('flux.see.anomalies', $lastActiveProcess)) {
            throw $this->createAccessDeniedException();
        }

        $pageSize = 20;
        $em = $this->get('doctrine.orm.entity_manager');
        $anomalyRepository = $em->getRepository(Anomaly::class);
        $rdfResourceRepository = $em->getRepository(RdfResource::class);

        $query = $anomalyRepository->createByProcessQuery($lastActiveProcess);
        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate($query, $request->query->getInt('page', 1), $pageSize, ['distinct' => false]);

        $items = $list->getItems();

        return $this->render('flux/detail/anomalies.html.twig', array(
            'flux' => $flux,
            'list' => $list,
            'items' => $items,
            'last_active_process' => $lastActiveProcess,
            'page_size' => $pageSize,
            'poi_count' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux),
            'anomaly_count' => $anomalyRepository->getCountByProcess($lastActiveProcess),
            'has_pending_revision' => $this->get('app.repository.flux')->hasPendingRevision($flux),
        ));
    }

    /**
     * (CSV File)
     * Downloads full and flat list of anomalies.
     *
     * @Route("/anomalies/download", name="flux.anomalies.download")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux $flux
     *
     * @return Response
     */
    public function anomaliesDownloadAction(Flux $flux): Response
    {
        $lastActiveProcess = $flux->getLastActiveProcess();
        if ($lastActiveProcess === null || !$this->isGranted('flux.see.anomalies', $lastActiveProcess)) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $anomalies = $em->getRepository(Anomaly::class)->findBy(['process' => $lastActiveProcess], ['rdfResource' => 'asc', 'breaker' => 'desc']);

        $response = new StreamedResponse();
        $response->setCallback(function () use ($anomalies) {
            $flatAnomalies = array();
            $count = count($anomalies);
            for ($i = 0; $i < $count; ++$i) {
                /** @var RdfResource */
                $resource = $anomalies[$i]->getRdfResource();
                $flatAnomalies[$i]['message'] = $anomalies[$i]->getContent();
                $flatAnomalies[$i]['bloquante'] = $anomalies[$i]->isBreaker() ? 'Oui' : 'Non';
                $flatAnomalies[$i]['ressource'] = $resource->getUri();
                $flatAnomalies[$i]['identifiant'] = $resource->getIdentitySIT();
                $flatAnomalies[$i]['libelle'] = $resource->getLabel();
                $flatAnomalies[$i]['type'] = $resource->getType();
                $flatAnomalies[$i]['type2'] = $resource->getType2();
                $flatAnomalies[$i]['creator'] = $resource->getCreator();
                $flatAnomalies[$i]['postalCode'] = $resource->getPostalCode();
                $flatAnomalies[$i]['city'] = $resource->getCity();
            }
            $handle = fopen('php://output', 'w+');
            fputcsv($handle, array_keys($flatAnomalies[0]));
            foreach ($flatAnomalies as $anomaly) {
                fputcsv($handle, $anomaly);
            }
            fclose($handle);
        });
        $response->setStatusCode(Response::HTTP_OK);

        $fileName = 'anomalies-'.$lastActiveProcess->getId().'-'.strtolower(str_replace(' ', '-', $flux->getName())).'.csv';
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$fileName.'"');
        $response->headers->set('Content-Type', 'text/csv');

        return $response;
    }

    /**
     * (Modal)
     * List of POI for an anomaly type (defined by content and breaker respect).
     *
     * @Route("/anomalies/resources", name="flux.anomalies.resources")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function anomaliesResourcesAction(Flux $flux, Request $request): Response
    {
        $lastActiveProcess = $flux->getLastActiveProcess();
        if ($lastActiveProcess === null || !$this->isGranted('flux.see.anomalies', $lastActiveProcess)) {
            throw $this->createAccessDeniedException();
        }

        $content = $request->query->get('content');
        $isBreaker = $request->query->get('is_breaker');
        if ($content === null || ((int) $isBreaker !== 0 && (int) $isBreaker !== 1)) {
            throw $this->createNotFoundException('No valid parameters provided to route.');
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $rdfResources = $em->getRepository(RdfResource::class)->findByAnomalyContent($lastActiveProcess, $content);

        return $this->render('flux/detail/anomalies.resources.html.twig', array(
            'content' => $content,
            'is_breaker' => $isBreaker,
            'rdf_resources' => $rdfResources,
        ));
    }

    /**
     * List of Flux's processes.
     *
     * @Route("/processes", name="flux.processes")
     * @Security("is_granted('flux.see', flux)")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function processesAction(Flux $flux, Request $request): Response
    {
        $pageSize = 15;
        $paginationOptions = array(
            'defaultSortFieldName' => 'process.createdAt',
            'defaultSortDirection' => 'desc',
        );

        $qb = $this->get('app.repository.process')->createPaginationQueryBuilder($flux);

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, ProcessFilter::class)
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->handleRequest($request);

        return $this->render('flux/detail/processes.html.twig', array(
            'flux' => $flux,
            'list' => $list,
            'items' => $list->getItems(),
            'page_size' => $pageSize,
            'poi_count' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux),
            'anomaly_count' => $this->get('doctrine.orm.entity_manager')->getRepository(Anomaly::class)->getCountByFlux($flux),
            'has_pending_revision' => $this->get('app.repository.flux')->hasPendingRevision($flux),
        ));
    }

    /**
     * Action for administrator on the flux.
     *
     * @Route("/administrate", name="flux.admin")
     *
     * @param Flux $flux
     *
     * @return Response
     */
    public function administrationAction(Flux $flux): Response
    {
        return $this->render('admin/flux/administrate.html.twig', array(
            'flux' => $flux,
            'poi_count' => $this->get('app.repository.rdf_resource')->getCountByFlux($flux),
            'anomaly_count' => $this->get('doctrine.orm.entity_manager')->getRepository(Anomaly::class)->getCountByFlux($flux),
            'has_pending_revision' => $this->get('app.repository.flux')->hasPendingRevision($flux),
        ));
    }
}
