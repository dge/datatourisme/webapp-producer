<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Notification\Type\UserCreateType;
use AppBundle\Utils\SSO\SSOPayload;
use AppBundle\Utils\SSO\SSOServer;
use Datatourisme\Bundle\WebAppBundle\Form\StrongPasswordType;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Validator\Constraints\Email;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security.login")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("cgu", name="security.cgu")
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function cguAction(Request $request)
    {
        $cguPath = $this->getParameter('cgu_md_path');
        $content = file_get_contents($cguPath);

        $form = $this->createFormBuilder()
            ->add('accept', CheckboxType::class, array(
                'label' => 'J\'accepte les conditions générales d\'utilisation',
                'required' => true,
                'data' => $this->getUser()->getOrganization()->isCgu(),
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $accept = $form->get('accept')->getData();
            if ($accept) {
                $this->getUser()->getOrganization()->setCgu(true);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirect('/');
            }
            $this->addFlash('error', "Vous devez accepter les conditions générales d'utilisation pour utiliser la plateforme.");
        }

        return $this->render('security/cgu.html.twig', array(
            'content' => $content,
            'version' => $this->getParameter('cgu_version'),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("cgu/download", name="security.cgu.download")
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function cguDownloadAction(Request $request)
    {
        $cguPath = $this->getParameter('cgu_pdf_path');
        $cguVersion = $this->getParameter('cgu_version');
        $response = new BinaryFileResponse($cguPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'datatourisme-cgu-producteur-v'.$cguVersion.'.pdf');

        return $response;
    }

    /**
     * @Route("/reset-password", name="security.reset_password")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'Email',
                ),
                'constraints' => array(new Email()),
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // get email
            $email = $form->get('email')->getData();

            /** @var QueryBuilder $qb */
            $qb = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('u');
            $qb->where('lower(u.email) = :email')
                ->setParameter('email', mb_strtolower($email, 'UTF-8'));
            $result = $qb->setMaxResults(1)->getQuery()->execute();
            $user = null;
            if (count($result) > 0) {
                $user = current($result);
            }

            if ($user && $user->isAccountNonLocked()) {
                // save lower email if it is not already
                if ($user->getEmail() !== mb_strtolower($email, 'UTF-8')) {
                    $user->setEmail(mb_strtolower($email, 'UTF-8'));
                    $this->getDoctrine()->getManager()->flush();
                }

                // generate new token
                $now = new \DateTime();
                $bcrypt = $this->container->get('security.password_encoder')->encodePassword($user, $now->getTimestamp().$user->getPassword());

                // send link
                $this->get('webapp.mailer')->send('account.reset_password', $user, array(
                    'bcrypt' => $bcrypt,
                    'expiration' => $this->getParameter('reset_password_expiration'),
                    'timestamp' => $now->getTimestamp(),
                ));
            }
            $this->addFlash('success', 'Un email vous a été envoyé pour réinitialiser votre mot de passe');

            return $this->redirectToRoute('security.login');
        }

        return $this->render('security/reset_password.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Check reset link and manage user update password.
     *
     * @param Request $request
     * @param User    $user
     * @param string  $timestamp
     * @param string  $bcrypt
     *
     * @Route("/reset-password/{email}/{timestamp}/{bcrypt}", requirements={"bcrypt" = ".+"}, name="security.reset_password.process")
     * @ParamConverter("user", class="AppBundle:User", options={"email" = "email"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordProcessAction(Request $request, User $user, $timestamp, $bcrypt)
    {
        // compute number of seconds since link has been sent
        $elapsed = time() - $timestamp;

        // token is not expired
        if ($elapsed < $this->getParameter('reset_password_expiration')) {
            // check bcrypt validity with user salt and password
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
            $bcryptIsValid = $encoder->isPasswordValid($bcrypt, $timestamp.$user->getPassword(), $user->getSalt());
            if ($bcryptIsValid) {
                $form = $this->createFormBuilder($user)
                    ->add('plainPassword', StrongPasswordType::class, array('user' => $user))
                    ->getForm();

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $user->updateTimestamps();
                    $user->setCredentialsNonExpired(true);

                    // if user is new, its account is enabled
                    if ($user->getLastLogin() === null && !$user->isEnabled()) {
                        $user->setEnabled(true);
                        // dispatch notification
                        $this->get('notifier')->dispatch(UserCreateType::class, $user);
                    }

                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('success', 'Votre mot de passe a bien été mis à jour.');

                    return $this->redirectToRoute('security.login');
                }

                return $this->render('security/reset_password_process.html.twig', [
                    'form' => $form->createView(),
                    'user' => $user,
                ]);
            }
        }

        $this->addFlash('warning', 'Le lien fourni n\'est plus valide. Vous devez faire une nouvelle demande de réinitialisation de mot de passe.');

        return $this->redirectToRoute('security.reset_password');
    }


    /**
     * @Route("/sso", name="security.sso")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function ssoAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->redirectToRoute('security.login', ['_target_path' => $request->getRequestUri()]);
        }

        $error = false;
        if ($this->isGranted('ROLE_PREVIOUS_ADMIN')) {
            // you cannot use this feature as impersonated user
            $error = "Vous ne pouvez pas vous connecter pendant une substitution d'authentification";
        }

        $sso = $request->get('sso');
        $signature = $request->get('sig');
        $secret = $this->getParameter('sso_secret');

        try {
            $payload = new SSOPayload($sso);
            $payload->validate($secret, $signature);
            $server = new SSOServer($payload, $secret);
            if ($error) {
                $server->setError($error);
            } else {
                $server->setUserData($user);
            }
            $data = $server->build();
        } catch (\Exception $e) {
            $this->get('logger')->err($e->getMessage());
            throw new BadRequestHttpException(null, $e);
        }

        $redirect_urls = $this->getParameter('sso_redirect_urls');
        $redirect_url = $request->get('redirect_url');
        if (!in_array($redirect_url, $redirect_urls)) {
            $this->get('logger')->err("Invalid redirect url");
            throw new BadRequestHttpException("Invalid redirect url");
        }

        return new RedirectResponse($redirect_url.'?'.$data);
    }

}
