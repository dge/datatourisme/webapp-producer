<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\AccountParamsType;
use AppBundle\Form\Type\OrganizationType;
use AppBundle\Form\Type\AccountType;
use Datatourisme\Bundle\WebAppBundle\Form\UpdatePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/account")
 * @Security("has_role('ROLE_USER')")
 */
class AccountController extends Controller
{
    /**
     * @Route("", name="account.user")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userAction(Request $request)
    {
        $userForm = $this->createForm(AccountType::class, $this->getUser());

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre compte a bien été mis à jour.');

            return $this->redirectToRoute('account.user');
        }

        return $this->render('account/user.html.twig', [
            'form' => $userForm->createView(),
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/params", name="account.params")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function paramsAction(Request $request)
    {
        $userForm = $this->createForm(AccountParamsType::class, $this->getUser());

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre compte a bien été mis à jour.');

            return $this->redirectToRoute('account.params');
        }

        return $this->render('account/params.html.twig', [
            'form' => $userForm->createView(),
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/organization", name="account.organization")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function organizationAction(Request $request)
    {
        $organizationForm = $this->createForm(OrganizationType::class, $this->getUser()->getOrganization());
        $organizationForm->handleRequest($request);
        if ($organizationForm->isSubmitted() && $organizationForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre structure a bien été mise à jour.');
        }

        return $this->render('account/organization.html.twig', [
            'form' => $organizationForm->createView(),
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/administrate", name="account.administrate")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administrateAction()
    {
        return $this->render('account/administrate.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/administrate/password", name="account.administrate.password")
     * @Security("is_granted('modify_password')")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administratePasswordAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(UpdatePasswordType::class, $user);
        $em = $this->getDoctrine()->getManager();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->updateTimestamps();
            $em->flush();
            $this->addFlash('success', 'Votre mot de passe a bien été mis à jour.');

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('account/administrate/password.html.twig', [
            'form' => $form->createView(),
            'user' => $this->getUser(),
        ]);
    }
}
