<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\LoginHistory;
use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use AppBundle\Form\Filter\UserFilter;
use AppBundle\Form\Type\UserParamsType;
use AppBundle\Form\Type\UserType;
use Datatourisme\Bundle\WebAppBundle\Form\DateRangePickerType;
use Datatourisme\Bundle\WebAppBundle\Utils\CsvWriter;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin/user")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends Controller
{
    /**
     * @Route("", name="user.index")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->getRepository(User::class)->getIndexQueryBuilder();

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($query, UserFilter::class)
            //->setPageSize(1)
            ->handleRequest($request);

        $organization = null;
        // if there is a organization filter, select it
        $filters = $list->getPagination()->getParams();
        if (!empty($filters['user_filter']['organization'])) {
            $organization = $em->getRepository(Organization::class)->find((int) $filters['user_filter']['organization']);
        }

        return $this->render('admin/user/index.html.twig', array(
            'organization' => $organization,
            'list' => $list,
        ));
    }

    /**
     * @Route("/export", name="user.export")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function exportAction(Request $request): Response
    {
        $writer = new CsvWriter([
            'Nom' => '[0].lastName',
            'Prénom' => '[0].firstName',
            'Email' => '[0].email',
            'Structure - Nom' => '[0].organization.name',
            'Structure - Type' => '[0].organization.type.name',
            'Structure - Adresse' => '[0].organization.address',
            'Structure - Code postal' => '[0].organization.zip',
            'Structure - Ville' => '[0].organization.city',
            'Structure - Téléphone' => '[0].organization.phoneNumber',
            'Structure - Site web' => '[0].organization.website',
            'Connexion' => '[lastLogin]',
            'Statut' => '[0].accountStatus[label]',
        ]);

        $em = $this->get('doctrine.orm.entity_manager');
        $qb = $em->getRepository(User::class)->getIndexQueryBuilder();

        // apply filter
        $form = $this->createForm(UserFilter::class);
        $form->submit($request->query->get($form->getName()));
        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $qb);

        $rows = $qb->getQuery()->execute();
        $writer->writeObjects($rows);

        $writer->output("producteurs.csv");
        exit;
    }

    /**
     * @Route("/login-export", name="user.loginExport")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function loginExportAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('datePicker', DateRangePickerType::class, array(
                'label' => 'Sélectionnez une période',
                'required' => false,
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $start = $form->get('datePicker')->get('start')->getData();
            $end = $form->get('datePicker')->get('end')->getData();

            /** @var EntityRepository $repository */
            $repository = $this->getDoctrine()->getManager()->getRepository(LoginHistory::class);
            $qb = $repository->createQueryBuilder('lh')
                ->leftJoin('AppBundle:User', 'u', Join::WITH, 'lh.user = u');
            if ($start) {
                $qb->andWhere('lh.datetime >= :start')
                    ->setParameter('start', $start);
            }
            if ($end) {
                $end->modify('+23 hours');
                $end->modify('+59 minutes');
                $end->modify('+59 seconds');
                $qb->andWhere('lh.datetime <= :end')
                    ->setParameter('end', $end);
            }
            $entities = $qb->getQuery()->execute();

            $writer = new CsvWriter([
                'Id' => 'id',
                'Nom de la structure' => 'user.organization.name',
                'Nom de l\'utilisateur' => 'user.lastName',
                'Prénom de l\'utilisateur' => 'user.firstName',
                'Date/Heure' => 'datetime'
            ]);

            $writer->writeObjects($entities);
            $writer->output("rapport-connexion.csv");
            exit;
        }

        return $this->render('admin/user/login_export.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/add/{id}", name="user.add", defaults={"id": null})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Organization $organization = null)
    {
        // init a new User
        $user = new User();
        $user->setPlainPassword(strval(rand(10000000, 9999999999999)));
        if ($organization) {
            $user->setOrganization($organization);
        }

        $flow = $this->get('app.form.flow.user');
        $flow->bind($user);
        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                // flow finished
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $flow->reset();

                // generate new token
                $now = new \DateTime();
                $bcrypt = $this->container->get('security.password_encoder')->encodePassword($user, $now->getTimestamp().$user->getPassword());

                // send link
                $this->get('webapp.mailer')->send('account.welcome', $user, array(
                    'bcrypt' => $bcrypt,
                    'expiration' => $this->getParameter('reset_password_expiration'),
                    'timestamp' => $now->getTimestamp(),
                ));

                $this->addFlash('success', 'L\'utilisateur a bien été créé. Un email lui a été envoyé pour l\'inviter à finaliser la procédure.');

                return $this->redirectToRoute('user.edit', array('id' => $user->getId()));
            }
        }

        return $this->render('admin/user/add.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="user.edit")
     * @Security("is_granted('edit', account)")
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, User $account)
    {
        $userForm = $this->createForm(UserType::class, $account);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'L\'utilisateur a bien été mis à jour.');

            return $this->redirectToRoute('user.edit', array('id' => $account->getId()));
        }

        return $this->render('admin/user/edit.html.twig', [
            'form' => $userForm->createView(),
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/params", name="user.params")
     * @Security("is_granted('edit', account)")
     *
     * @param Request $request
     * @param User $account
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function paramsAction(Request $request, User $account)
    {
        $form = $this->createForm(UserParamsType::class, $account);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'L\'utilisateur a bien été mis à jour.');

            return $this->redirectToRoute('user.params', array('id' => $account->getId()));
        }

        return $this->render('admin/user/params.html.twig', [
            'form' => $form->createView(),
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate", name="user.administrate")
     * @Security("is_granted('administrate', account)")
     *
     * @param User|null $user
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administrateAction(User $account)
    {
        return $this->render('admin/user/administrate.html.twig', [
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/block", name="user.administrate.block")
     * @Security("is_granted('block', account)")
     *
     * @param Request $request
     * @param User    $account
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blockAction(Request $request, User $account)
    {
        if ($request->getMethod() === 'POST') {
            $account->setAccountNonLocked(!$account->isAccountNonLocked());
            $this->getDoctrine()->getManager()->flush();
            $this->get('webapp.mailer')->send('account.blocked', $account);
            $this->addFlash((!$account->isAccountNonLocked() ? 'warning' : 'success'), 'L\'utilisateur a été '.(!$account->isAccountNonLocked() ? 'bloqué.' : 'débloqué.'));

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('admin/user/administrate/block.html.twig', [
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/remove", name="user.administrate.remove")
     * @Security("is_granted('delete', account)")
     *
     * @param Request $request
     * @param User    $account
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function removeAction(Request $request, User $account)
    {
        if ($request->getMethod() === 'POST') {
            $this->get('webapp.mailer')->send('account.deleted', $account);
            $em = $this->getDoctrine()->getManager();
            $em->remove($account);
            $em->flush();
            $this->addFlash('warning', 'L\'utilisateur a bien été supprimé.');

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('user.index'),
            ], 200);
        }

        return $this->render(':admin/user:administrate/remove.html.twig', [
            'user' => $account,
        ]);
    }
}
