<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\ThesaurusAlignment;
use AppBundle\Form\Type\AlignmentType;
use AppBundle\Thesaurus\Ontology;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Validator\Constraints as Constraint;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/thesaurus")
 * @Security("has_role('ROLE_USER')")
 */
class ThesaurusController extends Controller
{
    /**
     * @Route("/align/{type}/{rdfClass}", name="thesaurus.align", requirements={"rdfClass" = ".+"})
     *
     * @param Request $request
     * @param string  $type
     * @param string  $rdfClass
     *
     * @return Response
     */
    public function alignAction(Request $request, $type, $rdfClass = null)
    {
        if (!$this->getUser()->getOrganization()) {
            throw new AccessDeniedException('You are not a member of any organization');
        }

        $redirect = $this->redirectRdfClass($rdfClass, $type);
        if ($redirect) {
            return $redirect;
        }

        // get storage
        $storage = $this->get('app.thesaurus_storage');

        // create parent form
        $formBuilder = $this->createFormBuilder(null, ['csrf_protection' => false]);

        // go
        $expand = !empty($request->query->get('expand')) && $type != 'geographic';
        $processClass = function($uri) use (&$processClass, $type, $formBuilder, $storage, $expand) {
            $term = $this->get('app.thesaurus_hierarchy')->get($type, $uri);
            $formBuilder->add(md5($uri), AlignmentType::class, [
                'label' => $term['name'],
                'children' => $term['children'],
                'breadcrumb' => $term['breadcrumb'],
                'type' => $term['type'],
                'values' => $storage->getAlignments($this->getUser()->getOrganization(), array_keys($term['children']))
            ]);
            if ($expand) {
                foreach($term['currentHierarchy']['hierarchy'] as $uri => $child) {
                    if ($child['nbrChild'] > 0) {
                        $processClass($uri);
                    }
                }
            }
            return $term;
        };
        
        $term = $processClass($rdfClass);

        // finalize form
        $form = $formBuilder->getForm();

        // save form values in storage
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach(array_keys($form->getData()) as $key) {
                $values = $form->get($key)->getConfig()->getOption('values');
                $values = $storage->updateValues($form->get($key), $values);
                $storage->saveValues($values);
            }
            $this->addFlash('success', 'Les correspondances ont été enregistrées.');
            return $this->redirectToRoute('thesaurus.align', array_merge(['type' => $type, 'rdfClass' => $rdfClass], $request->query->all()));
        }

        // twig params
        return $this->render('thesaurus/edit.html.twig', [
            'form' => $form->createView(),
            'term' => $term,
            'type' => $type
        ]);
    }

    /**
     * Import new values from YML file.
     *
     * @Route("/import/{type}/{rdfClass}", requirements={"rdfClass" = ".+"}, name="thesaurus.import")
     *
     * @param Request $request
     * @param string  $type
     * @param string  $rdfClass
     *
     * @return Response
     */
    public function importAction(Request $request, $type, $rdfClass)
    {
        $form = $this->createFormBuilder(array('mode' => 'append'))
            ->add('file', FileType::class, array(
                'label' => 'Fichier',
                'constraints' => new Constraint\File(array(
                    'mimeTypes' => array('text/plain'),
                    'mimeTypesMessage' => 'Le fichier fourni n\'est pas un fichier YAML',
                )),
            ))
            ->add('mode', ChoiceType::class, array(
                'label' => 'Option de l\'importation',
                'expanded' => true,
                'required' => true,
                'choices' => array(
                    'Ajouter les valeurs aux valeurs existantes' => 'append',
                    'Remplacer toutes les valeurs existantes' => 'replace',
                ),
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var File $file */
            $file = $form->get('file')->getData();
            $mode = $form->get('mode')->getData();
            try {
                // get uploaded file content
                $fileContent = file_get_contents($file->getRealPath());
                $newValues = Yaml::parse($fileContent);

                if (!is_array($newValues)) {
                    throw new ParseException('Values are not an array');
                }

                // save file values
                $storage = $this->get('app.thesaurus_storage');
                $alignments = $storage->getAlignments($this->getUser()->getOrganization());
                $em = $this->getDoctrine()->getManager();

                /* @var ThesaurusAlignment $value */
                foreach ($newValues as $iri => $values) {
                    if (isset($alignments[$iri])) {
                        $alignment = $alignments[$iri];
                    } else {
                        $alignment = new ThesaurusAlignment($this->getUser()->getOrganization(), $iri, array());
                        $em->persist($alignment);
                    }
                    if ('replace' == $mode) {
                        $alignment->setValues($values);
                    } else {
                        $alignment->addValues($values);
                    }
                }

                $em->flush();
                if ('replace' == $mode) {
                    $this->addFlash('success', 'Les valeurs ont été remplacées.');
                } else {
                    $this->addFlash('success', 'Les valeurs ont été ajoutées.');
                }
            } catch (ParseException $e) {
                $this->addFlash('warning', 'Le fichier n\'est pas dans un format YAML valide');
            }

            return $this->redirectToRoute('thesaurus.align', array('type' => $type, 'rdfClass' => $rdfClass));
        }

        return $this->render('thesaurus/import.html.twig', array('form' => $form->createView()));
    }

    /**
     * Export alignment as YML file.
     *
     * @Route("/export/{type}/{rdfClass}/{allExport}", name="thesaurus.export")
     *
     * @param string $type
     * @param string $rdfClass
     * @param int    $allExport
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportAction($type, $rdfClass, $allExport)
    {
        // get list iris for current page iris export
        $storage = $this->get('app.thesaurus_storage');
        $alignmentIris = array();
        if (!$allExport) {
            $alignmentIris = $this->get('app.thesaurus_export')->get($type, $rdfClass);
        }

        // get values from storage
        $content = $storage->getExport($this->getUser()->getOrganization(), $alignmentIris);
        if (empty($content)) {
            $content = null;
        } else {
            $content = Yaml::dump($content);
        }

        if (!$content) {
            $this->addFlash('warning', 'Aucune valeur n\'a été trouvée');

            return $this->redirectToRoute('thesaurus.align', array('type' => $type, 'rdfClass' => $rdfClass));
        }

        return new Response($content, 200, array(
            'Content-Type' => 'application/yml; charset=cp-1252',
            'Content-Disposition' => 'attachment; filename="thesaurus.yml"',
            'Content-Transfer-Encoding' => 'application/octet-stream',
            'Content-Length' => strlen($content),
        ));
    }

    /**
     * @param $rdfClass
     * @param $type
     *
     * @return string|RedirectResponse
     */
    protected function redirectRdfClass($rdfClass, $type)
    {
        $parts = explode(':', $rdfClass);
        if (1 == count($parts) && $parts[0] === $rdfClass) {
            if (Ontology::GEOGRAPHIC === $type) {
                $this->get('app.thesaurus.hierarchy.individuals_builder')->loadOntology();
                $rdfClass = $this->get('app.thesaurus.hierarchy.individuals_builder')->shortIri(
                    $this->getParameter('namespaces')['thesaurus'] . $rdfClass
                );
            } elseif (Ontology::OTHER === $type) {
                $this->get('app.thesaurus.hierarchy.classes_builder')->loadOntology();
                $rdfClass = $this->get('app.thesaurus.hierarchy.classes_builder')->shortIri(
                    'http://www.w3.org/2002/07/owl#'.$rdfClass
                );
            } else {
                $this->get('app.thesaurus.hierarchy.classes_builder')->loadOntology();
                $rdfClass = $this->get('app.thesaurus.hierarchy.classes_builder')->shortIri(
                    $this->getParameter('namespaces')['ontology'] . $rdfClass
                );
            }

            return $this->redirectToRoute('thesaurus.align', array('type' => $type, 'rdfClass' => $rdfClass));
        }

        return null;
    }
}
