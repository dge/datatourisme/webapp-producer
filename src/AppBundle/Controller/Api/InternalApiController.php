<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Organization;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Process;
use AppBundle\Entity\RdfResource;
use AppBundle\Process\Report\ReportJobMessage;
use AppBundle\Process\Report\StatusJobMessage;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/internal/api", defaults={ "_format": "json" })
 */
class InternalApiController extends Controller
{
    /**
     * Launch a process to the worker.
     *
     * @Route("/run/{id}", name="internal.api.run")
     *
     * @param Flux $flux
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function runAction(Flux $flux)
    {
        if ($flux->getMode() != Flux::MODE_PULL) {
            throw new BadRequestHttpException('The flux must be in the PULL mode to perform this action.');
        }
        $this->get('app.task_runner')->start($flux);

        return new JsonResponse(['success' => true]);
    }

    /**
     * monitor messages from beanstalk server.
     *
     * @Route("/status/{type}", name="internal.api.status")
     *
     * @param Request $request
     * @param $type
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function statusAction(Request $request, $type)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $sm = $this->get('state_machine.process_operation');
        $updateStats = false;
        $isScheduledForDelete = false;

        $payload = json_decode($request->getContent(), true);

        if (!isset($payload['uuid'])) {
            throw new \Exception('process doesn\'t exist');
        }

        // --- of beanstalk generated uuid, find or create process ---
        if (!is_numeric($payload['uuid'])) {
            $process = $em->getRepository(Process::class)->findOneBy(['uuid' => $payload['uuid']]);
            if (!$process) {
                $flux = $em->getRepository(Flux::class)->find((int) $payload['flux']);
                $process = new Process($flux);
                $process->setUuid($payload['uuid']);
                $em->persist($process);
                $em->flush();
            }
            $payload['uuid'] = $process->getId();
        }
        // --- for debug ---

        $processId = (int) $payload['uuid'];
        /** @var Process $process */
        $process = $em->getRepository(Process::class)->find($processId);
        if (!$process) {
            throw new \Exception('process doesn\'t exist');
        }

        $em->getConnection()->beginTransaction();
        try {
            switch ($type) {
                case 'start':
                    $jobMessage = new StatusJobMessage($payload);
                    $jobMessage->updateProcess($process);
                    break;
                case 'success':
                    $jobMessage = new ReportJobMessage($payload);

                    // delete all resources
                    $qb = $em->createQueryBuilder();
                    $query = $qb->delete(RdfResource::class, 'r')
                        ->where('r.flux = :flux')
                        ->setParameter('flux', $process->getFlux())
                        ->getQuery();
                    $query->execute();

                    // handle reports
                    $process = $jobMessage->handleReports($em, $process);
                    $process->setDuration(round($jobMessage->getTotalDuration() / 1000));
                    $isScheduledForDelete = $em->getUnitOfWork()->isScheduledForDelete($process->getFlux());

                    if (filter_var($process->getFlux()->getSuccessUrl(), FILTER_VALIDATE_URL)) {
                        try {
                            $client = new Client();
                            $stats = $jobMessage->getPersistStats();
                            $client->request(
                                'POST',
                                $process->getFlux()->getSuccessUrl(),
                                [
                                    'timeout' => 5,
                                    'connect_timeout' => 5,
                                    'form_params' => ['stats' => json_encode($stats)],
                                ]
                            );
                        } catch (\Exception $e) {
                        }
                    }

                    $updateStats = true;
                    break;
                case 'error':
                    $jobMessage = new ReportJobMessage($payload);
                    $process = $jobMessage->handleReports($em, $process);
                    if (filter_var($process->getFlux()->getFailUrl(), FILTER_VALIDATE_URL)) {
                        try {
                            $client = new Client();
                            $client->request('POST', $process->getFlux()->getFailUrl(), array(
                                'timeout' => 5,
                                'connect_timeout' => 5,
                            ));
                        } catch (\Exception $e) {
                        }
                    }
                    break;
            }
        } catch (\Exception $e) {
            $this->get('monolog.logger.status_api')->error('Exception caught', array(
                //'payload' => $payload,
                'exception' => $e,
            ));
            $em->getConnection()->rollBack();

            return new JsonResponse(['error' => ['message' => $e->getMessage(), 'code' => 500]], 500);
        }

        $em->flush();
        $em->getConnection()->commit();

        if (!$isScheduledForDelete) {
            $sm->apply($process, $type);
            $em->flush();
        }

        if($updateStats) {
            // update stats
            $this->get('app.utils.stats_entry_manager')->createFluxStatsEntry($process->getFlux(), null, true, true);
        }

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route(
     *     "/{organization_id}/thesaurus",
     *     name="internal.api.thesaurus",
     *     requirements={"organization_id": "\d+"}
     * )
     * @Method({"GET"})
     *
     * @ParamConverter("organization", class="AppBundle:Organization", options={"id" = "organization_id"})
     *
     * @param Organization $organization
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function thesaurusAction(Organization $organization): JsonResponse
    {
        $thesaurus = $this->get('app.thesaurus_storage')->getValues($organization);

        return new JsonResponse($thesaurus);
    }
}
