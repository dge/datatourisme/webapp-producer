<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Api;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\JavaWorker\JavaWorkerClient;
use GuzzleHttp\Exception\RequestException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/api", defaults={ "_format": "json" })
 */
class ApiController extends Controller
{
    /**
     * @Route(
     *     "/{flux_id}/xpath-schema/{rdfResourceName}",
     *     name="api.xpath_schema",
     *     requirements={
     *         "flux_id": "\d+"
     *     },
     *     defaults={
     *         "rdfResourceName": null
     *     }
     * )
     * @Method({"GET"})
     * @Security("is_granted('flux.edit', flux)")
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     *
     * @param Flux        $flux
     * @param string|null $rdfResourceName
     *
     * @return JsonResponse
     */
    public function xPathSchemaAction(Flux $flux, string $rdfResourceName = null): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        try {
            $schema = $javaWorkerClient->xpathSchema($flux, $rdfResourceName);

            return JsonResponse::create($schema);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route(
     *     "/{flux_id}/expr-raw-extract",
     *     name="api.expr_raw_extract",
     *     requirements={
     *         "flux_id": "\d+"
     *     }
     * )
     * @Method({"POST"})
     * @Security("is_granted('flux.edit', flux)")
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function exprRawExtractAction(Flux $flux, Request $request): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        $params = json_decode($request->getContent(), true);
        try {
            $schema = $javaWorkerClient->exprRawExtract($flux, $params);

            return JsonResponse::create($schema);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route(
     *     "/{flux_id}/expr-preview/{source}",
     *     name="api.expr_preview",
     *     requirements={
     *         "flux_id": "\d+"
     *     },
     *     defaults={
     *         "source": "dao"
     *     }
     * )
     * @Method({"POST"})
     * @Security("is_granted('flux.edit', flux)")
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function exprPreviewAction(Flux $flux, $source, Request $request): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        $params = json_decode($request->getContent(), true);
        try {
            $schema = $javaWorkerClient->exprExtract($flux, $source, $params);

            return JsonResponse::create($schema);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route( "/ontology/{profile}", name="api.ontology", defaults={"profile" = null})
     * @Method({"GET"})
     *
     * @return JsonResponse
     */
    public function ontologyAction($profile = null): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        try {
            $ontology = $javaWorkerClient->ontology($profile);

            return JsonResponse::create($ontology);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route(
     *     "/{flux_id}/stats",
     *     name="api.stats",
     *     requirements={
     *         "flux_id": "\d+"
     *     }
     * )
     * @Security("is_granted('flux.edit', flux)")
     * @Method({"GET"})
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function statsAction(Flux $flux, Request $request): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        $query = '
                SELECT
                    COUNT(items.id) AS count,
                    attrs.value AS value
                FROM attrs
                LEFT JOIN items
                    ON items.id = attrs.id
                    AND attrs.name = \'type\'
                GROUP BY attrs.value
            ';

        try {
            $stats = [];
            $results = $javaWorkerClient->executeSQL($flux, $query);
            foreach ($results as $item) {
                $prefix = $this->getParameter('namespaces')['ontology'];
                $stats[$prefix.$item['value']] = (int) $item['count'];
            }

            return JsonResponse::create($stats);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route(
     *     "/{flux_id}/rules/{revision_id}",
     *     name="api.rules",
     *     requirements={"flux_id": "\d+", "revision_id": "\d+"},
     *     defaults={"revision_id": null}
     * )
     * @Method({"GET"})
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @param Flux                          $flux
     * @param AlignmentRulesetRevision|null $revision
     * @param Request                       $request
     *
     * @return JsonResponse
     */
    public function rulesAction(Flux $flux, AlignmentRulesetRevision $revision = null, Request $request): JsonResponse
    {
        // ____ Defining alignment ruleset
        $alignmentRuleset = $flux->getAlignmentRuleset();
        if (!$alignmentRuleset) {
            throw new NotFoundHttpException('Given Flux not bound to any alignment ruleset.');
        }

        // ____ Flux and Revision must match
        if ($revision instanceof AlignmentRulesetRevision && $alignmentRuleset->getId() !== $revision->getAlignmentRuleset()->getId()) {
            throw new BadRequestHttpException('No matching relationship found for given Flux (id '.$flux->getId().') and Revision (id '.$revision->getId().').');
        }

        // ____ Getting default revision if not provided
        if (!$revision) {
            $version = $alignmentRuleset->getActiveVersion();
            if (!$version) {
                throw new NotFoundHttpException('No available revision found with current version parameter.');
            }
            $revisionRepository = $this->get('doctrine.orm.entity_manager')->getRepository(AlignmentRulesetRevision::class);
            $result = $revisionRepository->findBy([
                'alignmentRuleset' => $alignmentRuleset,
                'version' => $version,
            ]);
            if (!$result) {
                throw new NotFoundHttpException('No ruleset found for given parameters (Flux of id '.$flux->getId().' and revision version n°'.$version.').');
            } elseif (count($result) > 1) {
                throw new \LogicException('Error - Returned more than one result with Flux of id '.$flux->getId().' and revision version n°'.$version.').');
            }

            $revision = &$result[0];
        }

        // ____ Returning JSON from Revision's content
        return JsonResponse::create($revision->getContent());
    }

    /**
     * @Route(
     *     "/{flux_id}/rules/{revision_id}",
     *     name="api.rules.save",
     *     requirements={"flux_id": "\d+", "revision_id": "\d+"},
     * )
     * @Method({"POST"})
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     * @ParamConverter("revision", class="AppBundle:AlignmentRulesetRevision", options={"id" = "revision_id"})
     *
     * @param Flux                     $flux
     * @param AlignmentRulesetRevision $revision
     * @param Request                  $request
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function saveRulesAction(Flux $flux, AlignmentRulesetRevision $revision, Request $request): JsonResponse
    {
        // ____ Defining alignment ruleset
        $alignmentRuleset = $flux->getAlignmentRuleset();
        if (!$alignmentRuleset) {
            throw new NotFoundHttpException('Given Flux not bound to any alignment ruleset.');
        }

        // ____ Flux and Revision must match
        if ($alignmentRuleset->getId() !== $revision->getAlignmentRuleset()->getId()) {
            throw new BadRequestHttpException('No matching relationship found for given Flux (id '.$flux->getId().') and Revision (id '.$revision->getId().').');
        }

        // ____ Modified revision must be in draft state
        if ($revision->getVersion() !== $flux->getAlignmentRuleset()->getDraftVersion()) {
            throw new \Exception('Cannot modify ruleset which is not currently in draft state (AlignmentRulesetRevision of id '.$revision->getId().').');
        }

        // ____ Handling data
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            throw new \UnexpectedValueException('Provided ruleset content must be valid JSON. Error: "'.json_last_error_msg().'".');
        }
        if (!isset($data['rules']) || !isset($data['@context'])) {
            throw new BadRequestHttpException('POST request to this path must provide rules and @context.');
        }

        // ____ Persisting modifications
        $content = $revision->getContent();
        $content['@context'] = $data['@context'];
        $content['rules'] = $data['rules'];
        $revision->setContent($content);
        $this->get('doctrine.orm.entity_manager')->flush();

        // ____ Returning JSON from Revision's content
        return JsonResponse::create($revision->getContent());
    }

    /**
     * @Route( "/atomic", name="api.atomic" )
     * @Method({"GET"})
     *
     * @return Response
     */
    public function atomicAction(): Response
    {
        // IMPORTANT
        throw new NotFoundHttpException('Not implemented');
    }

    /**
     * @Route( "/atomic/preview", name="api.atomic.preview" )
     * @Method({"POST"})
     *
     * @return JsonResponse
     */
    public function atomicPreviewAction(Request $request): Response
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        $params = json_decode($request->getContent(), true);
        try {
            $preview = $javaWorkerClient->previewAtomicChain($params['values'], $params['chain']);

            return JsonResponse::create($preview);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route(
     *     "/{flux_id}/preview",
     *     name="api.preview",
     *     requirements={
     *         "flux_id": "\d+"
     *     }
     * )
     * @Security("is_granted('flux.preview', flux)")
     * @Method({"GET"})
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function previewAction(Flux $flux, Request $request): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        try {
            $index = $javaWorkerClient->previewIndex($flux, $request->query->all());

            return JsonResponse::create($index);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route(
     *     "/{flux_id}/preview/{id}",
     *     name="api.preview.resource",
     *     requirements={
     *         "flux_id": "\d+"
     *     }
     * )
     * @Security("is_granted('flux.preview', flux)")
     * @Method({"POST"})
     *
     * @ParamConverter("flux", class="AppBundle:Flux", options={"id" = "flux_id"})
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function previewResourceAction(Flux $flux, $id, Request $request): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        try {
            $alignment = json_decode($request->getContent(), true);
            $thesaurus = $this->get('app.thesaurus_storage')->getValues($flux->getOrganization());
            $resource = $javaWorkerClient->previewResource($flux, $id, $alignment, $thesaurus);

            return JsonResponse::create($resource);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }
}
