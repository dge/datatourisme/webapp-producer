<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Exception;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * TemporarilyBlockedAccountException is thrown if the user account is blocked.
 **/
class TemporarilyBlockedAccountException extends AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'Account is blocked for an hour.';
    }
}
