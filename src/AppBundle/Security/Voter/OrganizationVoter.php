<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Process;
use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class FluxVoter.
 */
class OrganizationVoter extends AbstractVoter
{
    const DELETE = 'organization.delete';

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::DELETE:
                return $this->canDelete($user, $subject);
        }

        throw new \LogicException('This code should not be reached.');
    }

    /**
     * @param User $user
     * @param Organization $organization
     * @return bool
     * @internal param Flux $flux
     *
     */
    private function canDelete(User $user, Organization $organization)
    {
        if (!$this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }

        // allow deletion only if there is no more users and flux
        return $organization->getUsers()->count() == 0 && $organization->getFlux()->count() == 0;
    }
}
