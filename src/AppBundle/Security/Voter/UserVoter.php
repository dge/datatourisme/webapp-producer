<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserVoter extends AbstractVoter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const ADMINISTRATE = 'administrate';
    const IMPERSONATE = 'impersonate';
    const BLOCK = 'block';
    const MODIFY_PASSWORD = 'modify_password';

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Post object, thanks to supports
        /** @var User $account */
        $account = $subject ? $subject : $user;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($account, $user);
            case self::EDIT:
                return $this->canEdit($account, $user);
            case self::DELETE:
                return $this->canDelete($account, $user);
            case self::ADMINISTRATE:
                return $this->canAdministrate($account, $user);
            case self::BLOCK:
                return $this->canBlock($account, $user);
            case self::IMPERSONATE:
                return $this->canImpersonate($account, $user);
            case self::MODIFY_PASSWORD:
                return $this->canModifyPassword($account, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canView(User $account, User $user)
    {
        // if they can edit, they can view
        return $this->canEdit($account, $user);
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canEdit(User $account, User $user)
    {
        // only user himself or admin can edit
       if ($this->hasRole('ROLE_ADMIN', $user)) {
           return true;
       }

        return $account->getId() == $user->getId();
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canAdministrate(User $account, User $user)
    {
        // only admin can administrate
        if (!$this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }
        // only superadmin can administrate admin
        if ($this->hasRole('ROLE_ADMIN', $account) && !$this->hasRole('ROLE_SUPER_ADMIN', $user)) {
            return false;
        }

        // cannot administrate himself
        return $account->getId() != $user->getId();
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canDelete(User $account, User $user)
    {
        return $this->canAdministrate($account, $user);
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canBlock(User $account, User $user)
    {
        return $this->canAdministrate($account, $user);
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canImpersonate(User $account, User $user)
    {
        return $account->isLoginable() && $this->canAdministrate($account, $user);
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canModifyPassword(User $account, TokenInterface $token)
    {
        if ($this->isImpersonated($token)) {
            return false;
        }

        return $account->getId() === $token->getUser()->getId();
    }
}
