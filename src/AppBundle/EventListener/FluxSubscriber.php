<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\Flux;
use AppBundle\Entity\FluxCrawler;
use AppBundle\Process\TaskRunner;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class FluxSubscriber.
 */
class FluxSubscriber implements EventSubscriber
{
    /** @var TaskRunner */
    private $container;

    /**
     * FluxSubscriber constructor.
     *
     * Container is injected to avoid ServiceCircularReferenceException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
            'preRemove',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        return $this->preUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $flux = $args->getObject();
        if($flux instanceof FluxCrawler) {
            $flux = $flux->getFlux();
        }

        if ($flux instanceof Flux) {
            // schedule flux if mode = pull
            if ($flux->getMode() === Flux::MODE_PULL && $flux->getStatus() == Flux::STATUS_PRODUCTION) {
                $hour = $flux->getFluxCrawler()->getUpdateHour();
                $this->container->get('app.task_runner')->schedule($flux, $hour === null ? -1 : $hour);
            } else {
                $this->container->get('app.task_runner')->unschedule($flux);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $flux = $args->getObject();
        if ($flux instanceof Flux) {
            $this->container->get('app.task_runner')->unschedule($flux);
        }
    }
}
