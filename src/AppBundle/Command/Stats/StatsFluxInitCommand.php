<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Stats;

use AppBundle\Entity\Process;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StatsFluxInitCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:stats:flux:init')
            ->setAliases(['stats:flux:init']) // legacy
            ->setDescription("Initializes Flux' stats")
            ->setHelp("This command initializes Flux' stats")
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        /** @var Process $firstProcess */
        $firstFlux = $container->get('app.repository.flux')->findOneBy([], ['createdAt' => 'ASC']);

        $firstActivityTime = $firstFlux->getCreatedAt()->getTimestamp();
        $daysNb = floor((time() - $firstActivityTime) / 86400);

        $updateCommand = $this->getApplication()->find('stats:flux:update');
        for ($i = 0; $i < $daysNb; ++$i) {
            $dayArg = date(StatsFluxUpdateCommand::DAY_FORMAT, strtotime("-$i days"));
            $updateCommand->run(new ArrayInput(['--day' => $dayArg]), $output);
        }
    }
}
