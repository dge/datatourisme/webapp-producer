<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Thesaurus;

use AppBundle\Entity\Organization;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DumpCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:thesaurus:dump')
            ->addArgument('org_id', InputArgument::REQUIRED, 'organization id');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $orgId = $input->getArgument('org_id');
        $em = $this->getContainer()->get('doctrine')->getManager();

        $organization = $em->getRepository(Organization::class)->find($orgId);
        if (!$organization) {
            throw new InvalidArgumentException("L'organisation $orgId est introuvable");
        }

        $thesaurus = $this->getContainer()->get('app.thesaurus_storage')->getValues($organization);
        $output->writeln(json_encode($thesaurus, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    }
}
