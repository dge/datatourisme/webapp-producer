<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Flux;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ValidateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:flux:validate')
            ->addArgument('flux_id', InputArgument::REQUIRED, 'flux id');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id_flux = $input->getArgument('flux_id');
        $em = $this->getContainer()->get('doctrine')->getManager();

        $flux = $em->getRepository(Flux::class)->find($id_flux);
        if (!$flux) {
            throw new InvalidArgumentException('Le flux est introuvable');
        }

        $alignment = $flux->getAlignmentRuleset();
        $draft = $alignment->getDraft();
        $active = $alignment->getActive();
        if ($draft == null) {
            throw new RuntimeException('aucun alignement en brouillon');
        }
        if ($active) {
            $active->setStatus(AlignmentRulesetRevision::STATUS_ARCHIVED);
        }
        $flux->setNewChecksum();
        $alignment->setActiveVersion($draft->getVersion());
        $alignment->setDraftVersion(null);
        $flux->setStatus(Flux::STATUS_PRODUCTION);

        $em->flush();
        $output->writeln("Le flux a été validé, attention, aucune tâche rundeck n'a été planifiée");
    }
}
