<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Reuse;

use Doctrine\ORM\EntityManagerInterface;
use PDO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PersistCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:reuse:persist')
            ->setDescription('Persist reuse data from the database into gzipped csv on disk');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rootDir = $this->getContainer()->getParameter('reuse_files_path');
        @mkdir($rootDir, 0755, true);
        
        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $conn = $em->getConnection();

        // get the existing months in the reuse table
        $stmt = $conn->executeQuery("SELECT DISTINCT DATE_TRUNC('month', download_at) FROM reuse");
        $months = $stmt->fetchAll(PDO::FETCH_COLUMN);
        sort($months);

        // get the corresponding filenames
        $files = [];
        foreach($months as $m) {
            $files[$m] = sprintf('%s/%s.csv.gz', $rootDir, substr($m, 0, 7));
        }

        // try to determinate the most recent processed month, based on file existence
        $mostRecentMonth = array_reduce(array_keys($files),
            function($carry, $m) use ($files) {
                return file_exists($files[$m]) ? $m : $carry;
            }
        );

        // if there is a most recent month, don't process older
        // (each file will be rewrited from the beginning, that's why we dont want fully processed month)
        if ($mostRecentMonth) {
            $files = array_filter($files, function($m) use ($mostRecentMonth) {
                return $m >= $mostRecentMonth;
            }, ARRAY_FILTER_USE_KEY);
        }
        
        // prepare progress bar
        $progressBar = new ProgressBar($output, count($files));
        $progressBar->start();

        // finally, process the filtered files
        foreach ($files as $month => $file) {
            // each file is rewrited from the beginning
            $fp = gzopen($file, "w");
            $stmt = $conn->executeQuery("SELECT * FROM reuse WHERE DATE_TRUNC('month', download_at) = ? ORDER BY download_at", [$month]);
            $first = true;
            while($row = $stmt->fetch()) {
                if ($first) {
                    fputcsv($fp, array_keys($row));
                    $first = false;
                }
                fputcsv($fp, array_values($row));
            }
            fclose($fp);
            $progressBar->advance();
        }

        $progressBar->finish();
        $output->writeln('');
    }
}
