<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Reuse;

use AppBundle\Entity\Organization;
use AppBundle\Notification\Type\ReuseReportNotifyType;
use Datatourisme\Bundle\WebAppBundle\Notification\NotificationDispatcher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:reuse:notify')
            ->setDescription('Notify users for downloadable archive for the previous month');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rootDir = $this->getContainer()->getParameter('reuse_files_path');
        $date = new \DateTime();
        $date->modify('first day of last month');

        $file = sprintf('%s/%s.csv.gz', $rootDir, $date->format("Y-m"));
        if (!file_exists($file)) {
            throw new \Exception('The archive does not exists : ' . basename($file));
        }
        
        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $repository = $em->getRepository(Organization::class);
        $organizations = $repository->findAll();
        $progressBar = new ProgressBar($output, count($organizations));
        $progressBar->start();

        /** @var NotificationDispatcher $notifier */
        $notifier = $this->getContainer()->get('notifier');

        foreach ($organizations as $organization) {

            $notifier->dispatch(ReuseReportNotifyType::class, (object) [
                'organization' => $organization,
                'date' => $date
            ]);
            $progressBar->advance();
        }

        $progressBar->finish();
        $output->writeln('');
    }
}
