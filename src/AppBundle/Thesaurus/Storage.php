<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus;

use AppBundle\Entity\ThesaurusAlignment;
use AppBundle\Entity\Organization;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Form;

/**
 * Class Storage.
 */
class Storage
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * Storage constructor.
     *
     * @param EntityManager $em
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * Get stored alignments.
     *
     * @param $organization
     * @param array $iris
     *
     * @return ThesaurusAlignment[]
     */
    public function getAlignments($organization, $iris = array())
    {
        $criteria = array(
            'organization' => $organization,
        );
        if ($iris) {
            $criteria['iri'] = $iris;
        }

        $entities = $this->em->getRepository(ThesaurusAlignment::class)->findBy($criteria);
        $values = array();
        foreach ($entities as $entity) {
            $values[$entity->getIri()] = $entity;
        }

        // return in the same order as IRIS array to keep label sorting
        if (!empty($iris)) {
            $alignments = array();
            foreach ($iris as $iri) {
                $alignment = null;
                if (!isset($values[$iri])) {
                    $alignment = new ThesaurusAlignment($organization, $iri, array());
                } else {
                    $alignment = $values[$iri];
                }
                $alignments[$iri] = $alignment;
            }

            $values = $alignments;
        }

        return $values;
    }

    /**
     * Get stored values.
     *
     * @param $organization
     * @param array $iris
     *
     * @return array
     */
    public function getValues($organization, $iris = array())
    {
        return array_map(function ($alignment) {
            /* @var ThesaurusAlignment $alignment */
            return $alignment->getValues();
        }, $this->getAlignments($organization, $iris));
    }

    /**
     * @param Form  $form
     * @param array $children
     * @param array $alignments
     *
     * @return array
     */
    public function updateValues($form, $alignments)
    {
        foreach($alignments as $uri => $alignment) {
            $key = md5($uri);
            if($form->has($key)) {
                $values = array_unique($form->get($key)->getData());
                $alignment->setValues($values);
            }
        }

        return $alignments;
    }

    /**
     * @param Organization $organization
     * @param array        $values
     * @param string       $alignmentType
     *
     * @return array
     */
    public function addValues(Organization $organization, $values)
    {
        $alignments = $this->getAlignments($organization, array_keys($values));

        /** @var ThesaurusAlignment $alignment */
        foreach ($alignments as $alignment) {
            $newValues = $values[$alignment->getIri()];
            $alignment->addValues($newValues);
            if (!$alignment->getId() && !empty($alignment->getValues())) {
                $this->em->persist($alignment);
            }
        }
        $this->em->flush();
    }

    /**
     * @param array $alignments
     */
    public function saveValues($alignments)
    {
        /** @var ThesaurusAlignment $alignment */
        foreach ($alignments as $alignment) {
            if (!$alignment->getId() && $alignment->getValues()) {
                $this->em->persist($alignment);
            } elseif ($alignment->getId() && !$alignment->getValues()) {
                $this->em->remove($alignment);
            }
        }
        $this->em->flush();
    }

    /**
     * @param $organization
     * @param $iris
     *
     * @return string
     */
    public function getExport($organization, $iris = null)
    {
        $alignments = $this->getAlignments($organization, $iris);

        $values = array();
        /** @var ThesaurusAlignment $alignment */
        foreach ($alignments as $alignment) {
            if ($alignment->getValues()) {
                $values[$alignment->getIri()] = $alignment->getValues();
            }
        }

        return $values;
    }
}
