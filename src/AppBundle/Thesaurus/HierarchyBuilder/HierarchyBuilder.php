<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus\HierarchyBuilder;

use AppBundle\Thesaurus\Ontology;
use AppBundle\Thesaurus\RdfNamespace;

/**
 * Class HierarchyBuilder.
 */
abstract class HierarchyBuilder
{
    /**
     * @var Ontology
     */
    protected $ontology;

    /**
     * @var string
     */
    protected $parentProperty;

    /**
     * @var string
     */
    protected $subProperty;

    /**
     * @var array
     */
    protected $allowedPrefixes;

    /**
     * @var array
     */
    protected $defaultNamespaces;

    /**
     * Ontology constructor.
     *
     * @param Ontology $ontology
     * @param $subProperty
     * @param $parentProperty
     * @param $defaultNamespaces
     */
    public function __construct(Ontology $ontology, $parentProperty, $subProperty, $defaultNamespaces)
    {
        $this->ontology = $ontology;
        $this->parentProperty = $this->shortProperties($parentProperty);
        $this->subProperty = $this->shortProperties($subProperty);
        $this->allowedPrefixes = null;
        $this->defaultNamespaces = $defaultNamespaces;
    }

    /**
     * @param  $cacheDir
     *
     * @return HierarchyBuilder
     */
    public function loadOntology($cacheDir = null)
    {
        $this->ontology->load($cacheDir);

        return $this;
    }

    /**
     * @return array
     */
    public function getOntologyContext()
    {
        return $this->ontology->getContext();
    }

    /**
     * @return array
     */
    public function getOntologyValues()
    {
        return $this->ontology->getValues();
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function isOntologyClassExists($class)
    {
        return $this->ontology->classExists($class);
    }

    /**
     * @param $iri
     *
     * @return int|string
     */
    protected function getPrefix($iri)
    {
        return RdfNamespace::getPrefix($this->allowedPrefixes, $iri);
    }

    /**
     * @param string $class
     *
     * @return string
     */
    public function getOntologyClassLabel($class)
    {
        return $this->ontology->getClassLabel($class);
    }

    /**
     * Build complete hierarchy.
     *
     * @param $parent
     * @param $maxDepth
     *
     * @return array
     */
    public function getHierarchy($parent = null, $maxDepth = -1)
    {
        if ($parent) {
            $parent = $this->extendIri($parent);
        }

        $hierarchy = array();
        // get top level
        foreach ($this->getOntologyValues() as $class => $info) {
            if (!$this->isNamespaceAllowed($class)) {
                continue;
            }
            $parentValues = isset($info[$this->parentProperty]) ? $info[$this->parentProperty] : array(null);
            foreach ($parentValues as $parentValue) {
                if ($parentValue == $parent || !$this->isOntologyClassExists($parentValue)) {
                    $hierarchy[$class] = array(
                        'label' => $this->getOntologyClassLabel($class),
                        'shortIri' => $this->shortIri($class),
                        'hierarchy' => array(),
                        'hasChild' => 0,
                    );
                }
            }
        }

        // compute unlimited children level hierarchy for each top level item
        foreach ($hierarchy as $class => $info) {
            $subHierarchy = $this->getChildren($class, $maxDepth);
            $hierarchy[$class]['shortIri'] = $this->shortIri($class);
            $hierarchy[$class]['hierarchy'] = 0 != $maxDepth ? $subHierarchy : array();
            $hierarchy[$class]['hasChild'] = count($subHierarchy) > 0 ? 1 : 0;
        }

        if ($parent) {
            $hierarchy = array($parent => array(
                'label' => $this->getOntologyClassLabel($parent),
                'shortIri' => $this->shortIri($parent),
                'hierarchy' => $hierarchy,
                'hasChild' => count($hierarchy) > 0 ? 1 : 0,
            ));
        }

        return $hierarchy;
    }

    /**
     * Return flat ascendant hierarchy for breadcrumb and navigation menu.
     *
     * @param $class
     *
     * @return array
     */
    public function getParents($class)
    {
        $parents = array();
        $continue = true;
        $this->extendIri($class);

        if ($class) {
            while ($continue) {
                $label = $this->getOntologyClassLabel($class);
                $parents[$class] = $label;
                $continue = false;
                if (isset($this->getOntologyValues()[$class][$this->parentProperty])) {
                    // get parent classes but put datatourisme classes in preference for thesaurus menu building
                    $parentClasses = $this->getOntologyValues()[$class][$this->parentProperty];
                    usort($parentClasses, function ($a, $b) {
                        if (strstr($a, $this->defaultNamespaces['ontology'])) {
                            return 1;
                        }

                        return 0;
                    });

                    foreach ($parentClasses as $parentClass) {
                        if ($this->isNamespaceAllowed($parentClass) && is_string($parentClass) && strlen($parentClass) >= 1) {
                            $class = $parentClass;
                            $continue = true;
                        }
                    }
                }
            }
        }
        $parents = $this->extendIris($parents);

        return array_reverse($parents);
    }

    /**
     * Get descendant hierarchy for form.
     *
     * @param $class
     * @param int $maxDepth
     * @param int $depth
     *
     * @return array
     */
    public function getChildren($class, $maxDepth = 1, $depth = 0)
    {
        $class = $this->extendIri($class);
        $children = $this->getOntologySubClasses($class);
        $hierarchy = array();
        if (($maxDepth < 0 || $depth < $maxDepth) && count($children) > 0) {
            foreach ($children as $subClass) {
                $hierarchy[$subClass] = $this->getChildren($subClass, $maxDepth, $depth + 1)[$subClass];
            }
        }

        $subClasses = array(
            'label' => $this->getOntologyClassLabel($class),
            'shortIri' => $this->shortIri($class),
            'hierarchy' => $hierarchy,
            'hasChild' => count($children) > 0 ? 1 : 0,
        );

        return array($class => $subClasses);
    }

    /**
     * @param array $allowedPrefixes
     */
    public function setAllowedPrefixes($allowedPrefixes)
    {
        $this->allowedPrefixes = $allowedPrefixes;
    }

    /**
     * @param string $iri
     *
     * @return string|null
     */
    public function getPrefixName($iri)
    {
        return RdfNamespace::getPrefixName($this->getOntologyContext(), $iri);
    }

    /**
     * @param array $iris
     *
     * @return array
     */
    public function extendIris($iris)
    {
        return RdfNamespace::extendIris($this->getOntologyContext(), $iris);
    }

    /**
     * @param string $iri
     *
     * @return string
     */
    public function extendIri($iri)
    {
        return RdfNamespace::extendIri($this->getOntologyContext(), $iri);
    }

    /**
     * @param string $iri
     *
     * @return string
     */
    public function shortIri($iri)
    {
        return RdfNamespace::shortIri($this->getOntologyContext(), $iri);
    }

    /**
     * @param array $iris
     *
     * @return array
     */
    public function shortIris($iris)
    {
        return RdfNamespace::shortIris($this->getOntologyContext(), $iris);
    }

    /**
     * @param $iri
     *
     * @return bool
     */
    public function isNamespaceAllowed($iri)
    {
        if (isset($this->getOntologyValues()[$iri]['thesaurus']) && false === $this->getOntologyValues()[$iri]['thesaurus']) {
            return false;
        }

        if (null === $this->allowedPrefixes) {
            return true;
        }

        $iri = RdfNamespace::extendIri($this->allowedPrefixes, $iri);
        $prefix = RdfNamespace::getPrefix($this->allowedPrefixes, $iri);

        return in_array($prefix, array_keys($this->allowedPrefixes));
    }

    /**
     * @param $properties
     *
     * @return string
     */
    protected function shortProperties($properties)
    {
        if (is_array($properties) && count($properties) > 0) {
            foreach ($properties as $key => $property) {
                $parentProperty[$key] = $property;
            }

            return implode('||', $properties);
        }

        return $properties;
    }

    /**
     * @param $class
     *
     * @return array
     */
    protected function getOntologySubClasses($class)
    {
        $values = array();
        if ($this->subProperty) {
            $ontologyValues = $this->getOntologyValues();
            if (!isset($ontologyValues[$class])) {
                return $values;
            }
            $values = $ontologyValues[$class];
            $values = $this->getValues($values, $this->subProperty);
        } else {
            // if class has not defined sub classes
            $class = $this->extendIri($class);
            foreach ($this->getOntologyValues() as $rdfClass => $value) {
                if (isset($value[$this->parentProperty]) && in_array($class, $value[$this->parentProperty])) {
                    $values[] = $rdfClass;
                }
            }
        }

        foreach ($values as $key => $value) {
            if (!$this->isNamespaceAllowed($value)) {
                unset($values[$key]);
            }
        }

        return $values;
    }

    /**
     * @param $values
     * @param $property
     *
     * @return mixed
     */
    protected function getValues($values, $property)
    {
        $properties = explode('||', $property);
        foreach ($properties as $propertyPart) {
            $propertyPart = $this->extendIri($propertyPart);
            if (isset($values[$propertyPart])) {
                $values = $values[$propertyPart];
            } else {
                $values = array();
            }
        }

        return $values;
    }
}
