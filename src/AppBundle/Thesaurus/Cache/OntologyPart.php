<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus\Cache;

use AppBundle\Thesaurus\RdfNamespace;

/**
 * Class OntologyPart.
 */
class OntologyPart extends AbstractCacheManager
{
    /**
     * @param string $contextPropertyPath
     * @param string $valuesPropertyPath
     * @param string $cacheDir
     *
     * @return array|mixed
     */
    public function get($contextPropertyPath, $valuesPropertyPath, $cacheDir = null)
    {
        // get params in cache
        $thesaurusCache = new Cache();
        $cache = $thesaurusCache->get($this->getNamespace($valuesPropertyPath), $this->environment, $cacheDir);

        // compute alignment params if it is not stored in cache
        if (!$cache || !$cache->get('#value')) {
            $params = $this->computeCacheParams($contextPropertyPath, $valuesPropertyPath, $cacheDir);
            $thesaurusCache->save($params);

            return $params;
        }

        return $cache->get('#value');
    }

    /**
     * @param string $contextPropertyPath
     * @param string $valuesPropertyPath
     * @param string $cacheDir
     *
     * @return array
     */
    protected function computeCacheParams($contextPropertyPath, $valuesPropertyPath, $cacheDir)
    {
        $fc = $this->container->get('app.java_worker.client')->ontology(false);

        return array(
            $contextPropertyPath => $fc[$contextPropertyPath],
            $valuesPropertyPath => RdfNamespace::extendIris($fc[$contextPropertyPath], $fc[$valuesPropertyPath]),
        );
    }

    /**
     * @param $valuesPropertyPath
     *
     * @return string
     */
    protected function getNamespace($valuesPropertyPath)
    {
        $valuesPropertyPath = 'ontology_'.$valuesPropertyPath;

        return parent::getNamespace($valuesPropertyPath);
    }
}
