<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus\Cache;

/**
 * Class Export.
 */
class Export extends AbstractCacheManager
{
    /**
     * @param $rdfClass
     * @param $type
     * @param $cacheDir
     *
     * @return array
     */
    protected function computeCacheParams($type, $rdfClass, $cacheDir)
    {
        $this->initializeBuilders($type, $cacheDir);

        $alignmentIris = array();
        if ($rdfClass) {
            $hierarchy = $this->hierarchyBuilder->getChildren($rdfClass, -1);
            if (isset($hierarchy['http://schema.org/Country'])) {
                $hierarchy['http://schema.org/Country']['hierarchy'][$this->container->getParameter('namespaces')['thesaurus'] . 'France']['hierarchy'] = array();
                $hierarchy['http://schema.org/Country']['hierarchy'][$this->container->getParameter('namespaces')['thesaurus'] . 'France']['hierarchy'] = array();
            }
            $menuIris = $this->getMenuClasses($hierarchy);
            foreach ($menuIris as $class) {
                $children = $this->childrenBuilder->getChildren($class);
                $alignmentIris = array_merge($alignmentIris, $this->getMenuClasses($children));
            }
            $alignmentIris = array_unique($alignmentIris);
        }

        return $alignmentIris;
    }

    /**
     * @param $hierarchy
     *
     * @return array
     */
    protected function getMenuClasses($hierarchy)
    {
        $iris = array();
        foreach ($hierarchy as $class => $info) {
            if (isset($info['hierarchy'])) {
                $iris[] = $class;
                if ($info['hasChild']) {
                    $iris = array_merge($iris, $this->getMenuClasses($info['hierarchy']));
                }
            }
        }

        return $iris;
    }

    /**
     * @param $rdfClass
     *
     * @return string
     */
    protected function getNamespace($rdfClass)
    {
        $rdfClass .= '_export';

        return parent::getNamespace($rdfClass);
    }
}
