<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus\Cache;

use AppBundle\Thesaurus\HierarchyBuilder\HierarchyBuilder;
use AppBundle\Thesaurus\Ontology;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class AbstractCacheManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    protected $environment;

    /**
     * @var HierarchyBuilder
     */
    protected $hierarchyBuilder;

    /**
     * @var HierarchyBuilder
     */
    protected $childrenBuilder;

    /**
     * AbstractCacheItemManager constructor.
     *
     * @param ContainerInterface $container
     * @param string             $environment
     */
    public function __construct(ContainerInterface $container, $environment)
    {
        $this->container = $container;
        $this->environment = $environment;
    }

    /**
     * @param string      $type
     * @param string      $rdfClass
     * @param string|null $cacheDir
     *
     * @return array|mixed
     */
    public function get($type, $rdfClass, $cacheDir = null)
    {
        // get params in cache
        $thesaurusCache = new Cache();
        $cache = $thesaurusCache->get($this->getNamespace($rdfClass), $this->environment, $cacheDir);

        // compute alignment params if it is not stored in cache
        if (!$cache || !$cache->get('#value')) {
            $params = $this->computeCacheParams($type, $rdfClass, $cacheDir);
            $thesaurusCache->save($params);

            return $params;
        }

        return $cache->get('#value');
    }

    /**
     * @param $type
     * @param $rdfClass
     * @param $cacheDir
     *
     * @return mixed
     */
    abstract protected function computeCacheParams($type, $rdfClass, $cacheDir);

    /**
     * @param $rdfClass
     *
     * @return string
     */
    protected function getNamespace($rdfClass)
    {
        return strtolower(str_replace(array('.', ':'), '_', $rdfClass ?: Ontology::OTHER));
    }

    /**
     * @param $type
     * @param $cacheDir
     *
     * @throws BadRequestHttpException
     */
    protected function initializeBuilders($type, $cacheDir)
    {
        // Align POI
        if (Ontology::CLASSES === $type) {
            $this->hierarchyBuilder = $this->childrenBuilder = $this->container->get('app.thesaurus.hierarchy.classes_builder');
            $this->hierarchyBuilder->loadOntology($cacheDir);
            $this->hierarchyBuilder->setAllowedPrefixes($this->getAllowedPrefixes($type));
        }
        // Align thesaurus
        elseif (Ontology::INDIVIDUALS === $type) {
            $this->hierarchyBuilder = $this->container->get('app.thesaurus.hierarchy.classes_builder');
            $this->childrenBuilder = $this->container->get('app.thesaurus.hierarchy.individuals_builder');

            $this->hierarchyBuilder->loadOntology($cacheDir);
            $this->childrenBuilder->loadOntology($cacheDir);

            $this->hierarchyBuilder->setAllowedPrefixes($this->getAllowedPrefixes($type));
            $this->childrenBuilder->setAllowedPrefixes($this->getAllowedPrefixes($type));
        }
        // Align countries
        elseif (Ontology::GEOGRAPHIC === $type) {
            $this->hierarchyBuilder = $this->childrenBuilder = $this->container->get('app.thesaurus.hierarchy.individuals_builder');
            $this->hierarchyBuilder->loadOntology($cacheDir);
            $this->hierarchyBuilder->setAllowedPrefixes($this->getAllowedPrefixes($type));
        }
        // Align other
        elseif (Ontology::OTHER === $type) {
            $this->hierarchyBuilder = $this->container->get('app.thesaurus.hierarchy.classes_builder');
            $this->childrenBuilder = $this->container->get('app.thesaurus.hierarchy.individuals_builder');

            $this->hierarchyBuilder->loadOntology($cacheDir);
            $this->childrenBuilder->loadOntology($cacheDir);

            $this->hierarchyBuilder->setAllowedPrefixes($this->getAllowedPrefixes($type));
            $this->childrenBuilder->setAllowedPrefixes($this->getAllowedPrefixes($type));
        } else {
            throw new BadRequestHttpException("The type $type does not exists");
        }
    }

    /**
     * @param $type
     * @param string|null $builder
     *
     * @return array
     */
    protected function getAllowedPrefixes($type, $builder = null)
    {
        $ontologyContext = $this->container->getParameter('namespaces')['ontology'];
        $individualsContext = $this->container->getParameter('namespaces')['thesaurus'];

        $ontologyContext = array($this->hierarchyBuilder->getPrefixName($ontologyContext) => $ontologyContext);
        $individualsContext = array($this->childrenBuilder->getPrefixName($individualsContext) => $individualsContext);
        $schemaContext = array($this->childrenBuilder->getPrefixName('http://schema.org/') => 'http://schema.org/');
        $skosContext = array($this->childrenBuilder->getPrefixName('http://www.w3.org/2004/02/skos/core#') => 'http://www.w3.org/2004/02/skos/core#');
        $owlContext = array($this->childrenBuilder->getPrefixName('http://www.w3.org/2002/07/owl#') => 'http://www.w3.org/2002/07/owl#');
        $ebucoreContext = array($this->childrenBuilder->getPrefixName('http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#') => 'http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#');

        if (Ontology::CLASSES === $type) {
            return $ontologyContext;
        }
        // Align thesaurus
        elseif (Ontology::INDIVIDUALS === $type) {
            if ('hierarchy' === $builder) {
                return array_merge(
                    $ontologyContext,
                    $schemaContext
                );
            }

            return array_merge(
                $ontologyContext,
                $individualsContext,
                $skosContext
            );
        }
        // Align countries
        elseif (Ontology::GEOGRAPHIC === $type) {
            return array_merge(
                $individualsContext,
                $ontologyContext,
                $skosContext,
                $schemaContext
            );
        } elseif (Ontology::OTHER === $type) {
            return array_merge(
                $individualsContext,
                $ontologyContext,
                $skosContext,
                $schemaContext,
                $owlContext,
                $ebucoreContext
            );
        }

        return array();
    }
}
