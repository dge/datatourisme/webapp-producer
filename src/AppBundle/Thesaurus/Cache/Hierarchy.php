<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus\Cache;

use AppBundle\Thesaurus\Ontology;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Hierarchy extends AbstractCacheManager
{
    /**
     * @param $type
     * @param $rdfClass
     * @param $cacheDir
     *
     * @return array
     */
    protected function computeCacheParams($type, $rdfClass, $cacheDir)
    {
        $this->initializeBuilders($type, $cacheDir);

        // get default rdfClass
        $defaultHierarchy = array();
        if (Ontology::OTHER === $type && !$rdfClass) {
            $defaultHierarchy = $this->hierarchyBuilder->getHierarchy(null, 0);
            $defaultHierarchy = $this->excludedMenuClasses($defaultHierarchy);
            $defaultHierarchy = $this->countNumberOfChildren($defaultHierarchy);
            if (!$rdfClass) {
                $defaultHierarchy = $this->sortByLabel($defaultHierarchy);
                foreach ($defaultHierarchy as $class => $info) {
                    if (isset($info['nbrChild']) && $info['nbrChild'] > 0) {
                        $rdfClass = $this->hierarchyBuilder->shortIri($class);
                        break;
                    }
                }
            }
        }
        if (!$rdfClass) {
            throw new BadRequestHttpException('The rdfClass is required');
        }
        $extendedRdfClass = $this->hierarchyBuilder->extendIri($rdfClass);

        // generate menu
        $parentHierarchyItems = $this->hierarchyBuilder->getParents($extendedRdfClass);
        $menu = $this->getMenu($parentHierarchyItems, $defaultHierarchy, $type);

        // compute form fields
        $formChildren = $this->childrenBuilder->getChildren($extendedRdfClass, 1);
        $formChildren = isset($formChildren[$extendedRdfClass]) && isset($formChildren[$extendedRdfClass]['hierarchy']) ? $formChildren[$extendedRdfClass]['hierarchy'] : $formChildren;

        $name = $this->hierarchyBuilder->getOntologyClassLabel($extendedRdfClass);
        $breadcrumb = $this->getBreadcrumb($parentHierarchyItems, $extendedRdfClass, $type);

        // update menu order and informations
        $menu = $this->updateHasFormItemsChildren($menu);
        $menu = $this->sortByLabel($menu);
        $formChildren = $this->sortByLabel($formChildren);
        // update is form item label is clickable - add shortcut to class menu item
        foreach ($formChildren as $class => $info) {
            $formChildren[$class]['isClickable'] = $this->isClickable($menu, $rdfClass, $info['shortIri']);
        }

        // save computed variables in cache
        $params = array(
            'context' => $this->hierarchyBuilder->getOntologyContext(),
            'menu' => $menu,
            'children' => $formChildren,
            'name' => $name,
            'breadcrumb' => $breadcrumb,
            'type' => $type,
            'currentHierarchy' => $this->getCurrentHierarchy($menu, $rdfClass),
            'currentClass' => $rdfClass,
        );

        return $params;
    }

    /**
     * Build hierarchy from top parent and direct children for navigation menu.
     *
     * @param $parentHierarchyItems
     * @param $defaultHierarchy
     * @param $type
     *
     * @return array
     */
    public function getMenu($parentHierarchyItems, $defaultHierarchy, $type)
    {
        $hierarchy = array();

        // root items for thesaurus.other route
        foreach ($defaultHierarchy as $class => $info) {
            $subHierarchy = $this->hierarchyBuilder->getChildren($class, 0);
            $hierarchy[$class] = isset($subHierarchy[$class]) ? $subHierarchy[$class] : $subHierarchy;
        }

        // root classes
        foreach ($parentHierarchyItems as $class => $label) {
            $subHierarchy = $this->hierarchyBuilder->getChildren($class, 1);
            $hierarchy[$class] = isset($subHierarchy[$class]) ? $subHierarchy[$class] : $subHierarchy;
        }

        // merge hierarchies
        $hierarchy = array_reverse($hierarchy);
        $continue = count($hierarchy) > 1;
        while ($continue) {
            $continue = false;
            $classToMerge = array_keys($hierarchy)[0];
            foreach (array_keys($hierarchy) as $class) {
                if ($class !== $classToMerge) {
                    if (isset($hierarchy[$class]['hierarchy'][$classToMerge])) {
                        $hierarchy[$class]['hierarchy'][$classToMerge] = $hierarchy[$classToMerge];
                        $hierarchy[$classToMerge]['hasChild'] = 1;
                        unset($hierarchy[$classToMerge]);
                        $continue = true;
                    }
                }
            }
            $continue = $continue && count($hierarchy) > 1;
        }
        $hierarchy = $this->hierarchyBuilder->shortIris($hierarchy);

        // make other countries be after France for geographic alignment
        if (Ontology::GEOGRAPHIC === $type) {
            $shortSchemaIri = $this->hierarchyBuilder->shortIri('http://schema.org/Country');
            $hierarchy[$shortSchemaIri] = array(
                'label' => 'Pays',
                'shortIri' => $shortSchemaIri,
                'hierarchy' => array(),
                'hasChild' => 1,
            );

            $franceIri = $this->container->getParameter('namespaces')['thesaurus'] . 'France';
            $shortFranceIri = $this->hierarchyBuilder->shortIri($franceIri);
            if (!isset($hierarchy[$shortFranceIri])) {
                $hierarchy[$shortFranceIri] = $this->hierarchyBuilder->getChildren($franceIri, 1)[$franceIri];
                $hierarchy[$shortFranceIri] = $this->hierarchyBuilder->shortIris($hierarchy[$shortFranceIri]);
            }
        }
        if (Ontology::OTHER === $type) {
            $hierarchy = $this->excludedMenuClasses($hierarchy);
        }

        // add nbr of children information to display it in menu
        $hierarchy = $this->countNumberOfChildren($hierarchy);

        return $hierarchy;
    }

    /**
     * @param $parentClasses
     * @param $rdfClass
     * @param $type
     *
     * @return array
     */
    public function getBreadcrumb($parentClasses, $rdfClass, $type)
    {
        $breadcrumb = array();
        foreach ($parentClasses as $class => $label) {
            if ($this->hierarchyBuilder->isNamespaceAllowed($class)) {
                $breadcrumb[] = ($class == $rdfClass ? $label : array($label, 'thesaurus.align', array('type' => $type, 'rdfClass' => $this->hierarchyBuilder->shortIri($class))));
            }
        }

        return $breadcrumb;
    }

    /**
     * @param array $hierarchy
     *
     * @return array
     */
    protected function updateHasFormItemsChildren($hierarchy)
    {
        foreach ($hierarchy as $class => $info) {
            if (isset($info['hasChild'])) {
                $children = $this->childrenBuilder->getChildren($this->hierarchyBuilder->extendIri($class));
                $hierarchy[$class]['hasChild'] = count($children) > 0 ? 1 : 0;
            }
            if (isset($hierarchy[$class]['hierarchy'])) {
                $hierarchy[$class]['hierarchy'] = $this->updateHasFormItemsChildren($info['hierarchy']);
                foreach ($hierarchy[$class]['hierarchy'] as $childClass => $childInfo) {
                    if (1 == $childInfo['hasChild']) {
                        $hierarchy[$class]['hasChild'] = 1;
                        break;
                    }
                }
            }
        }

        return $hierarchy;
    }

    /**
     * @param array $menu
     *
     * @return array
     */
    protected function countNumberOfChildren($menu)
    {
        foreach ($menu as $class => $info) {
            $extendedClass = $this->hierarchyBuilder->extendIri($class);
            $subItems = $this->hierarchyBuilder->getChildren($class, -1)[$this->hierarchyBuilder->extendIri($class)]['hierarchy'];
            if (empty($subItems)) {
                $subItems = $this->childrenBuilder->getChildren($extendedClass);
                if (isset($subItems[$extendedClass])) {
                    $subItems = $subItems[$extendedClass];
                }
            }
            $menu[$class]['nbrChild'] = isset($subItems['hierarchy']) ? count($subItems['hierarchy']) : count($subItems);
            $menu[$class]['hierarchy'] = isset($info['hierarchy']) ? $this->countNumberOfChildren($info['hierarchy']) : 0;
        }

        return $menu;
    }

    /**
     * @param array  $hierarchy
     * @param string $currentClass
     * @param string $rdfClass
     *
     * @return bool
     */
    protected function isClickable($hierarchy, $currentClass, $rdfClass)
    {
        $ret = 0;
        foreach ($hierarchy as $class => $info) {
            if ($class === $currentClass) {
                return isset($info['hierarchy'][$rdfClass]) && $info['hierarchy'][$rdfClass]['nbrChild'] > 0;
            }

            if (is_array($info)) {
                $ret = $this->isClickable($info, $currentClass, $rdfClass);
            }

            if ($ret) {
                return $ret;
            }
        }

        return $ret;
    }

    /**
     * @param $hierarchy
     * @param $rdfClass
     *
     * @return mixed
     */
    protected function getCurrentHierarchy($hierarchy, $rdfClass)
    {
        if (is_array($hierarchy)) {
            foreach ($hierarchy as $class => $info) {
                if ($class === $rdfClass) {
                    return $info;
                }

                $currentHierarchy = $this->getCurrentHierarchy($info['hierarchy'], $rdfClass);
                if ($currentHierarchy) {
                    return $currentHierarchy;
                }
            }
        }

        return null;
    }

    /**
     * @param $array
     *
     * @return mixed
     */
    protected function sortByLabel($array)
    {
        uasort($array, function ($a, $b) {
            return strtolower($this->removeAccents($a['label'])) > strtolower($this->removeAccents($b['label']));
        });

        foreach ($array as $class => $info) {
            if (isset($info['hierarchy'])) {
                $array[$class]['hierarchy'] = $this->sortByLabel($info['hierarchy']);
            }
        }

        return $array;
    }

    /**
     * @param array $hierarchy
     *
     * @return array
     */
    protected function excludedMenuClasses($hierarchy)
    {
        $hierarchy = $this->excludeClassesFromHierarchy($hierarchy, array(
            $this->container->getParameter('namespaces')['ontology'] . 'PointOfInterest',
            $this->container->getParameter('namespaces')['ontology'] . 'Audience',
            $this->container->getParameter('namespaces')['ontology'] . 'Amenity',
            $this->container->getParameter('namespaces')['ontology'] . 'Theme',
            $this->container->getParameter('namespaces')['ontology'] . 'Department',
            $this->container->getParameter('namespaces')['ontology'] . 'City',
            $this->container->getParameter('namespaces')['ontology'] . 'Region',
            $this->container->getParameter('namespaces')['ontology'] . 'Country',
            'http://schema.org/Country',
        ));

        return $hierarchy;
    }

    /**
     * @param array $hierarchy
     * @param array $classesToExclude
     *
     * @return array
     */
    protected function excludeClassesFromHierarchy($hierarchy, $classesToExclude)
    {
        foreach ($classesToExclude as $key => $class) {
            $classesToExclude[$key] = $this->hierarchyBuilder->extendIri($class);
        }

        foreach ($hierarchy as $class => $info) {
            if (in_array($this->hierarchyBuilder->extendIri($class), $classesToExclude)) {
                unset($hierarchy[$class]);
            } elseif ($hierarchy[$class]['hasChild'] == 1 && isset($hierarchy[$class]['hierarchy'])) {
                $hierarchy[$class]['hierarchy'] = $this->excludeClassesFromHierarchy($hierarchy[$class]['hierarchy'], $classesToExclude);
            }
        }

        return $hierarchy;
    }

    /**
     * @param $str
     *
     * @return string
     */
    protected function removeAccents($str)
    {
        $unwanted_array = array('Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
            'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
            'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', );

        return strtr($str, $unwanted_array);
    }
}
