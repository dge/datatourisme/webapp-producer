<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus;

/**
 * Class RdfNamespace.
 */
class RdfNamespace
{
    /**
     * Short all IRIs of an array.
     *
     * @param $context
     * @param $values
     *
     * @return array
     */
    public static function shortIris($context, $values)
    {
        return self::manageIris($context, $values, array(self::class, 'shortIris'), array(self::class, 'shortIri'));
    }

    /**
     * Extend all IRIs of an array.
     *
     * @param $context
     * @param $values
     *
     * @return array
     */
    public static function extendIris($context, $values)
    {
        return self::manageIris($context, $values, array(self::class, 'extendIris'), array(self::class, 'extendIri'));
    }

    /**
     * Extend all IRIs of an array.
     *
     * @param array  $context
     * @param string $subject
     *
     * @return string
     */
    public static function extendIri($context, $subject)
    {
        foreach ($context as $prefix => $iri) {
            if (0 === strpos($subject, $prefix.':')) {
                return str_replace($prefix.':', $iri, $subject);
            }
        }

        return $subject;
    }

    /**
     * Short an IRI.
     *
     * @param array  $context
     * @param string $iri
     *
     * @return string
     */
    public static function shortIri($context, $iri)
    {
        foreach ($context as $prefix => $extendedPrefix) {
            if (is_string($prefix) && 0 === strpos($iri, $extendedPrefix)) {
                return str_replace($extendedPrefix, $prefix.':', $iri);
            }
        }

        return $iri;
    }

    /**
     * Return prefix name of a context.
     *
     * @param array $context
     * @param $iri
     *
     * @return string
     */
    public static function getPrefixName($context, $iri)
    {
        foreach ($context as $name => $prefix) {
            if ($prefix === $iri) {
                return $name;
            }
        }

        return null;
    }

    /**
     * Return the prefix of an extended iri.
     *
     * @param $context
     * @param $iri
     *
     * @return int|string
     */
    public static function getPrefix($context, $iri)
    {
        if (!is_array($context)) {
            return $iri;
        }

        foreach ($context as $prefix => $baseIri) {
            if (is_string($prefix) && 0 === strpos($iri, $baseIri)) {
                return $prefix;
            }
        }

        return $iri;
    }

    /**
     * @param $context
     * @param $values
     * @param $functionIris
     * @param $functionIri
     *
     * @return array
     */
    protected static function manageIris($context, $values, $functionIris, $functionIri)
    {
        if (is_array($values)) {
            $extendedValues = array();
            foreach ($values as $iri => $value) {
                $extendedValues[$functionIri($context, $iri)] = $functionIris($context, $value);
            }

            return $extendedValues;
        }

        return $functionIri($context, $values);
    }
}
