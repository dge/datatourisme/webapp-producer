<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Thesaurus;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Ontology.
 */
class Ontology
{
    const CLASSES = 'classes';
    const INDIVIDUALS = 'individuals';
    const GEOGRAPHIC = 'geographic';
    const OTHER = 'other';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $context;

    /**
     * @var array
     */
    protected $values;

    /**
     * @var string
     */
    protected $valuesPropertyPath;

    /**
     * JsonFile constructor.
     *
     * @param ContainerInterface $container
     * @param $valuesPropertyPath
     */
    public function __construct(ContainerInterface $container, $valuesPropertyPath)
    {
        $this->values = null;
        $this->container = $container;
        $this->valuesPropertyPath = $valuesPropertyPath;
    }

    /**
     * @param  $cacheDir
     *
     * @return bool
     */
    public function load($cacheDir = null)
    {
        if (null !== $this->values) {
            return false;
        }
        $ontology = $this->container->get('app.thesaurus_ontology_part')->get('@context', $this->valuesPropertyPath, $cacheDir);
        $this->context = $ontology['@context'];
        $this->values = $ontology[$this->valuesPropertyPath];

        return true;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @return array
     */
    public function getClasses()
    {
        return array_keys($this->values);
    }

    /**
     * @param $class
     *
     * @return bool
     */
    public function classExists($class)
    {
        $class = RdfNamespace::extendIri($this->context, $class);

        return isset($this->values[$class]);
    }

    /**
     * @param $class
     *
     * @return string
     */
    public function getClassLabel($class)
    {
        $class = RdfNamespace::extendIri($this->context, $class);
        $label = isset($this->values[$class]['label']) ? $this->values[$class]['label'] : $class;

        // not in ontology
        if ('http://schema.org/Country' === $label) {
            $label = 'Pays';
        } elseif ('http://www.w3.org/2002/07/owl#NamedIndividual' === $class) {
            $label = 'Entités nommées';
        }

        return $label;
    }
}
