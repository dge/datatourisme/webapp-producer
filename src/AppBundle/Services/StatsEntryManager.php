<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Services;

use AppBundle\Entity\AbstractStatsEntry;
use AppBundle\Entity\Flux;
use AppBundle\Entity\FluxStatsEntry;
use AppBundle\Entity\Process;
use AppBundle\Entity\RdfResource;
use AppBundle\Repository\FluxStatsEntryRepository;
use AppBundle\Repository\ProcessRepository;
use AppBundle\Repository\RdfResourceRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class StatsEntryManager.
 */
class StatsEntryManager
{
    const DATE_FORMAT = 'Y/m/d';

    /** @var EntityManager $em */
    protected $em;

    /** @var RdfResourceRepository $rdfResourceRepository */
    protected $rdfResourceRepository;

    /** @var ProcessRepository $processRepository */
    protected $processRepository;

    /** @var FluxStatsEntryRepository $fluxStatsEntryRepository */
    protected $fluxStatsEntryRepository;

    /**
     * StatsEntryManager constructor.
     *
     * @param RdfResourceRepository $rdfResourceRepository
     */
    public function __construct(EntityManager $em, RdfResourceRepository $rdfResourceRepository,
                                ProcessRepository $processRepository,
                                FluxStatsEntryRepository $fluxStatsEntryRepository
    ) {
        $this->em = $em;
        $this->rdfResourceRepository = $rdfResourceRepository;
        $this->processRepository = $processRepository;
        $this->fluxStatsEntryRepository = $fluxStatsEntryRepository;
    }

    /**
     * @param Flux      $flux
     * @param \DateTime $date
     * @param bool      $update  If true, updates entry if already exists for flux and date. Else, just adds a new entry.
     * @param bool      $persist
     *
     * @return FluxStatsEntry
     */
    public function createFluxStatsEntry(Flux $flux, \DateTime $date = null,
                                         bool $update = true, bool $persist = false
    ): FluxStatsEntry {
        $day = $date ?: new \DateTime();
        $lastSuccessProcess = $this->processRepository->findLastProcessFromDay($flux, $day, Process::STATUS_SUCCESS);

        if (true === $update) {
            $entry = $this->fluxStatsEntryRepository->findOneBy(['flux' => $flux, 'date' => $day])
                ?: new FluxStatsEntry();
        } else {
            $entry = new FluxStatsEntry();
        }

        $entry->setFlux($flux);
        $entry->setOrganization($flux->getOrganization());
        $entry->setNbPublishedRdfResources(
            $lastSuccessProcess ? $lastSuccessProcess->getNbPublishedRdfResources() : 0
        );
        $entry->setPublicationRatio((function () use (&$flux): float {
            /** @var RdfResourceRepository $rdfResourceRepository */
            $publishedCount = $this->rdfResourceRepository->getCountByStatus(
                RdfResource::STATUS_PUBLISHED, $flux, false
            );
            $totalCount = $this->rdfResourceRepository->getCountByStatus(null, $flux, false);

            return self::calculatePublicationRatio($publishedCount, $totalCount, 15);
        })());
        $entry->setFluxStatus($flux->getStatus());
        $entry->setDate($day);

        if ($persist === true) {
            $this->persistStatsEntry($entry);
        }

        return $entry;
    }

    /**
     * @param AbstractStatsEntry $statsEntry
     *
     * @return AbstractStatsEntry
     */
    public function persistStatsEntry(AbstractStatsEntry $statsEntry): AbstractStatsEntry
    {
        $this->em->persist($statsEntry);
        $this->em->flush($statsEntry);

        return $statsEntry;
    }

    /**
     * @param int $publishedCount
     * @param int $totalCount
     * @param int $precision
     *
     * @return float
     */
    public static function calculatePublicationRatio(int $publishedCount, int $totalCount, int $precision = 0): float
    {
        return $totalCount <= 0 ? 0 : round($publishedCount / $totalCount * 100, $precision);
    }

    /**
     * @param AbstractStatsEntry[] $entries
     *
     * @return array
     */
    public static function groupEntriesByDays(array $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            if (!$entry instanceof AbstractStatsEntry) {
                $type = is_object($entry) ? get_class($entry) : gettype($entry);
                throw new \UnexpectedValueException(AbstractStatsEntry::class."expected, $type given.");
            }

            $date = $entry->getCreatedAt()->format(self::DATE_FORMAT);
            $result[$date][] = $entry;
        }

        return $result;
    }

    /**
     * Flattens given date to its midnight.
     *
     * @param \DateTime $date Date to be flattened
     *
     * @return \DateTime Result
     */
    public static function dayizeDate(\DateTime $date): \DateTime
    {
        return \DateTime::createFromFormat(
            self::DATE_FORMAT,
            strtotime($date->format(self::DATE_FORMAT)),
            $date->getTimezone()
        );
    }
}
