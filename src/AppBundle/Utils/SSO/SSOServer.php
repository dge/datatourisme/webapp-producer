<?php
/**
 * This file is part of the DATAtourisme project.
 *
 * @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Utils\SSO;

use AppBundle\Entity\User;

class SSOServer
{
    private $payload;
    private $secret;
    private $parameters = [];

    public function __construct(SSOPayload $payload, String $secret)
    {
        $this->payload = $payload;
        $this->secret = $secret;
    }

    public function setUserData(User $user)
    {
        $nonce = $this->payload->getNonce();

        $parameters = [
            'nonce' => $nonce,
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'qa_access' => $user->getQaPlatformAccess(),
        ];

        if ($organization = $user->getOrganization()) {
            $parameters['organization'] = [
                'id' => $organization->getId(),
                'name' => $organization->getName(),
                'type' => $organization->getType() ? $organization->getType()->getId() : 0
            ];
        }

        $this->parameters = $parameters;
    }

    public function setError(string $error)
    {
        $nonce = $this->payload->getNonce();
        
        $this->parameters = [
            'nonce' => $nonce,
            'error' => $error,
        ];
    }

    public function build()
    {
        $string = base64_encode(http_build_query($this->parameters));
        $data = [
            'sso' => $string,
            'sig' => $this->payload->encode($string, $this->secret),
        ];

        return http_build_query($data);
    }
}
