<?php
/**
 * This file is part of the DATAtourisme project.
 *
 * @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Utils\SSO;

/**
 * Payload of a sso query.
 */
class SSOPayload
{
    private $sso;

    public function __construct(String $sso = '')
    {
        $this->sso = $sso;
    }

    public function encode(String $value, String $secret)
    {
        return hash_hmac('sha256', $value, $secret);
    }

    /**
     * @return mixed
     *
     * @throws \Exception
     */
    public function getNonce()
    {
        $sso = urldecode($this->sso);
        $queryString = [];
        parse_str(base64_decode($sso), $queryString);
        if (!array_key_exists('nonce', $queryString)) {
            throw new \Exception('Not found nonce in payload');
        }

        return $queryString['nonce'];
    }

    /**
     * @param $secret
     * @param $signature
     *
     * @throws \Exception
     */
    public function validate(string $secret, string $signature)
    {
        $sso = urldecode($this->sso);
        if (!hash_equals($this->encode($sso, $secret), $signature)) {
            throw new \Exception('invalid Signature');
        }
    }
}
