<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(
 *     name = "anomaly",
 *     indexes={
 *         @ORM\Index(name = "anomaly_content", columns={"content"}),
 *         @ORM\Index(name = "anomaly_composite", columns={"id", "content"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnomalyRepository")
 */
class Anomaly
{
    use Timestampable;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=1023, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=1023)
     */
    private $content;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     * @Assert\NotBlank()
     */
    private $breaker = false;

    /**
     * @var Process
     * @ORM\ManyToOne(targetEntity="Process", inversedBy="anomalies")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $process;

    /**
     * @var RdfResource
     * @ORM\ManyToOne(targetEntity="RdfResource", inversedBy="anomalies")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $rdfResource;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = substr($content, 0, 1023);
    }

    /**
     * @return bool
     */
    public function isBreaker()
    {
        return $this->breaker;
    }

    /**
     * @param bool $breaker
     */
    public function setBreaker(bool $breaker)
    {
        $this->breaker = $breaker;
    }

    /**
     * @return Process
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param Process $process
     */
    public function setProcess(Process $process)
    {
        $this->process = $process;
    }

    /**
     * @return RdfResource
     */
    public function getRdfResource()
    {
        return $this->rdfResource;
    }

    /**
     * @param RdfResource $rdfResource
     */
    public function setRdfResource(RdfResource $rdfResource)
    {
        $this->rdfResource = $rdfResource;
    }
}
