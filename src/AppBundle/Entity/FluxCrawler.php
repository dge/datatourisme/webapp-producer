<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="flux_crawler")
 * @ORM\Entity()
 */
class FluxCrawler
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=511, nullable=true)
     * @Assert\Length(max=511)
     */
    private $url;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     * @Assert\All({
     *     @Assert\Url
     * })
     */
    private $i18nUrls;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updateHour;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $sshKey;

    /**
     * @var Flux
     * @ORM\OneToOne(targetEntity="Flux", inversedBy="fluxCrawler")
     * @ORM\JoinColumn(name="flux_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $flux;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getI18nUrls()
    {
        return $this->i18nUrls;
    }

    /**
     * @param array $i18nUrls
     */
    public function setI18nUrls($i18nUrls)
    {
        $this->i18nUrls = is_array($i18nUrls) ? array_filter($i18nUrls) : $i18nUrls;
    }

    /**
     * @return mixed
     */
    public function getUpdateHour()
    {
        return $this->updateHour;
    }

    /**
     * @param mixed $updateHour
     */
    public function setUpdateHour($updateHour)
    {
        $this->updateHour = $updateHour;
    }

    /**
     * @return string
     */
    public function getSshKey()
    {
        return $this->sshKey;
    }

    /**
     * @param string $sshKey
     */
    public function setSshKey($sshKey)
    {
        $this->sshKey = $sshKey;
    }

    /**
     * @return Flux
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param Flux $flux
     */
    public function setFlux($flux)
    {
        $this->flux = $flux;
    }
}
