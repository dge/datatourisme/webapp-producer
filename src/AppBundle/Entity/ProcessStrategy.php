<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="process_strategy")
 * @ORM\Entity()
 */
class ProcessStrategy
{
    const COMPLETE = 1;
    const DIFFERENTIAL = 2;

    /**
     * @ORM\Column(type="smallint")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=63, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=63)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=511, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=511)
     */
    private $description;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Flux", mappedBy="processStrategy")
     */
    private $flux;

    /**
     * FluxStatusConception constructor.
     */
    public function __construct()
    {
        $this->flux = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param ArrayCollection $flux
     */
    public function setFlux(ArrayCollection $flux)
    {
        $this->flux = $flux;
    }
}
