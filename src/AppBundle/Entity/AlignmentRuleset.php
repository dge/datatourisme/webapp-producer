<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="alignment_ruleset")
 * @ORM\Entity()
 */
class AlignmentRuleset
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $activeVersion;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $draftVersion;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Flux", mappedBy="alignmentRuleset")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $flux;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AlignmentRulesetRevision", mappedBy="alignmentRuleset", fetch="EAGER", cascade={"persist"})
     */
    private $revisions;

    /**
     * AlignmentRuleset constructor.
     */
    public function __construct($flux = null)
    {
        $this->flux = new ArrayCollection();
        $this->revisions = new ArrayCollection();

        if ($flux) {
            $this->flux->add($flux);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getActiveVersion()
    {
        return $this->activeVersion;
    }

    /**
     * @param int $activeVersion
     */
    public function setActiveVersion(int $activeVersion)
    {
        $this->activeVersion = $activeVersion;
    }

    /**
     * @return int
     */
    public function getDraftVersion()
    {
        return $this->draftVersion;
    }

    /**
     * @param int $draftVersion
     */
    public function setDraftVersion($draftVersion)
    {
        $this->draftVersion = $draftVersion;
    }

    /**
     * @return AlignmentRulesetRevision
     */
    public function getDraft()
    {
        return $this->getRevisionByVersion($this->getDraftVersion());
    }

    /**
     * @return AlignmentRulesetRevision|null
     */
    public function getActive()
    {
        return $this->getRevisionByVersion($this->getActiveVersion());
    }

    /**
     * @return ArrayCollection
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param ArrayCollection $flux
     */
    public function setFlux(ArrayCollection $flux)
    {
        $this->flux = $flux;
    }

    /**
     * @param AlignmentRulesetRevision $revision
     *
     * @return bool
     */
    public function addRevision(AlignmentRulesetRevision $revision)
    {
        if (!$this->revisions->contains($revision)) {
            if (!$this->revisions->count() && $revision->getVersion()) {
                $this->setDraftVersion($revision->getVersion());
            }
            $revision->setAlignmentRuleset($this);
            $this->revisions->add($revision);

            return true;
        }

        return false;
    }

    /**
     * @return AlignmentRulesetRevision[]
     */
    public function getRevisions()
    {
        return $this->revisions;
    }

    /**
     * @param ArrayCollection $revisions
     */
    public function setRevisions($revisions)
    {
        $this->revisions = $revisions;
    }

    /**
     * @param AlignmentRulesetRevision $revision
     *
     * @return bool
     */
    public function removeRevision(AlignmentRulesetRevision $revision)
    {
        if ($this->revisions->contains($revision)) {
            $this->revisions->removeElement($revision);

            return true;
        }

        return false;
    }

    /**
     * @param int $version
     *
     * @return AlignmentRulesetRevision|null
     */
    private function getRevisionByVersion(int $version = null)
    {
        if ($version && $this->getRevisions()->count()) {
            foreach ($this->getRevisions() as $revision) {
                if ($revision->getversion() === $version) {
                    return $revision;
                }
            }
        }

        return null;
    }
}
