<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Infos\Status\StatusInfos;
use AppBundle\Entity\Infos\Status\StatusInfosTrait;
use AppBundle\Entity\Status\StatusableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(
 *     name = "rdf_resource",
 *     uniqueConstraints = {
 *         @ORM\UniqueConstraint( name = "rdf_resource_idx", columns = { "flux_id","uri" } )
 *     },
 *     indexes={
 *         @Index(name="rdf_resource_uri_idx", columns={"uri"}),
 *         @Index(name="rdf_resource_uri_status_idx", columns={"uri","status"})
 *     }
 * )
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\RdfResourceRepository"
 * )
 */
class RdfResource
{
    use Timestampable, StatusableTrait, StatusInfosTrait;

    const STATUS_PUBLISHED = 'published';
    const STATUS_REJECTED = 'rejected';

    public static $statusDefinitions = array(
        self::STATUS_PUBLISHED => [
            'Publié',
            'La ressoure est validée et a correctement été importée.',
        ],
        self::STATUS_REJECTED => [
            'Exclu',
            'La ressource n\'est pas valide et n\'a pas été importée.',
        ],
    );

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $identitySIT;

    /**
     * @var string
     * @ORM\Column(type="string", length=511, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=511)
     */
    private $uri;

    /**
     * @var string
     * @ORM\Column(type="string", length=63, nullable=true)
     * @Assert\Length(max=63)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=63, nullable=true)
     * @Assert\Length(max=63)
     */
    private $type2;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Assert\Length(max=16)
     */
    private $postalCode;

    /**
     * @var string
     * @ORM\Column(type="string", length=127, nullable=true)
     * @Assert\Length(max=127)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $creator;

    /**
     * @var string
     * @ORM\Column(type="string", length=63, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=63)
     */
    private $status;

    /**
     * @var Flux
     * @ORM\ManyToOne(targetEntity="Flux", inversedBy="rdfResources")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $flux;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Anomaly", mappedBy="rdfResource")
     */
    private $anomalies;

    /**
     * RdfResource constructor.
     */
    public function __construct()
    {
        $this->anomalies = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIdentitySIT()
    {
        return $this->identitySIT;
    }

    /**
     * @param string $identitySIT
     */
    public function setIdentitySIT(string $identitySIT)
    {
        $this->identitySIT = $identitySIT;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri(string $uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType2()
    {
        return $this->type2;
    }

    /**
     * @param string $type2
     */
    public function setType2($type2)
    {
        $this->type2 = $type2;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $city
     */
    public function setPostalCode(string $postalCode)
    {
        $this->postalCode = substr($postalCode, 0, 16);
    }
    /**
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     */
    public function setCreator(string $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return Flux
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param Flux $flux
     */
    public function setFlux(Flux $flux)
    {
        $this->flux = $flux;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnomalies()
    {
        return $this->anomalies;
    }

    /**
     * @param ArrayCollection $anomalies
     */
    public function setAnomalies(ArrayCollection $anomalies)
    {
        $this->anomalies = $anomalies;
    }

    /**
     * Fill the RdfResource with an Object Resource.
     *
     * @param $resource
     * @param Flux $flux
     */
    public function setResource($resource, Flux $flux)
    {
        (isset($resource->id)) ? $this->setId($resource->id) : false;
        (isset($resource->uri)) ? $this->setUri($resource->uri) : false;
        (isset($resource->sit)) ? $this->setIdentitySIT($resource->sit) : false;
        (isset($resource->type)) ? $this->setType($resource->type) : false;
        (isset($resource->label)) ? $this->setLabel($resource->label) : false;
        (isset($resource->city)) ? $this->setCity($resource->city) : false;
        (isset($resource->postalCode)) ? $this->setPostalCode($resource->postalCode) : false;
        (isset($resource->statut)) ? $this->setStatus($resource->statut) : false;
        $this->setFlux($flux);
    }

    /**
     * @return StatusInfos
     */
    public function getStatusInfos(): StatusInfos
    {
        if ($this->statusInfos) {
            return $this->statusInfos;
        }

        $status = $this->getStatus();
        $statusInfos = $this->createStatusInfos($status, self::$statusDefinitions[$status][0], self::$statusDefinitions[$status][1]);
        $this->setStatusInfos($statusInfos);

        return $statusInfos;
    }

    /**
     * @return ArrayCollection
     */
    public function getDownloads()
    {
        return $this->downloads;
    }

    /**
     * @param ArrayCollection $downloads
     */
    public function setDownloads($downloads)
    {
        $this->downloads = $downloads;
    }
}
