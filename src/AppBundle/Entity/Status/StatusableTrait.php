<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Status;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class StatusableTrait.
 */
trait StatusableTrait
{
    /**
     * @var string
     * @ORM\Column(type="string", length=31, nullable=false)
     * @Assert\Expression(
     *     "value in this.getStatusList()",
     *     message="Invalid status."
     * )
     * @Assert\Length(max=31)
     */
    private $status;

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public static function getStatusList(): array
    {
        $reflection = new \ReflectionClass(__CLASS__);
        $constants = $reflection->getConstants();
        $statusList = array();
        foreach ($constants as $key => $value) {
            if (strpos($key, 'STATUS_') === 0) {
                $statusList[$key] = $value;
            }
        }

        return $statusList;
    }

    /**
     * @return array
     */
    public static function statusList(): array
    {
        return self::getStatusList();
    }
}
