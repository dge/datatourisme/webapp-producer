<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="thesaurus_alignment")
 * @ORM\Entity()
 */
class ThesaurusAlignment
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Organization
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    protected $organization;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $iri;

    /**
     * @var string
     * @ORM\Column(type="array")
     * @Assert\NotBlank()
     */
    private $values;

    public function __construct($organization, $iri, $values)
    {
        $this->organization = $organization;
        $this->iri = $iri;
        $this->values = $values;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return string
     */
    public function getIri()
    {
        return $this->iri;
    }

    /**
     * @param string $iri
     */
    public function setIri($iri)
    {
        $this->iri = $iri;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @param string $value
     */
    public function addValue($value)
    {
        if (!in_array($value, $this->values)) {
            $value = trim($value);
            if (!empty($value)) {
                $this->values[] = $value;
            }
        }
    }

    /**
     * @param array $values
     */
    public function addValues($values)
    {
        foreach ($values as $value) {
            $this->addValue($value);
        }
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }
}
