<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(
 *     name="reuse",
 *     indexes={
 *          @Index(name="idx_reuse_poi_creator", columns={"poi_creator"}),
 *          @Index(name="idx_reuse_app_consumer_id", columns={"app_consumer_id"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReuseRepository")
 */
class Reuse
{
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** @ORM\ManyToOne(targetEntity="organization") */
    private $organization;

    /** @ORM\Column(type="string", length=16) */
    private $appConsumerId;

    /** @ORM\Column(type="integer", nullable=true) */
    private $appId;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $appName;

    /** @ORM\Column(type="text", nullable=true) */
    private $appDesc;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $appUrl;

    /** @ORM\Column(type="integer") */
    private $consumerId;

    /** @ORM\Column(type="string", length=255) */
    private $consumerName;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $consumerUrl;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $consumerType;

    /** @ORM\Column(type="string", length=128, nullable=true) */
    private $consumerZip;

    /** @ORM\Column(type="string", length=2, nullable=true) */
    private $consumerCountry;

    /** @ORM\Column(type="string", length=50, nullable=true) */
    private $poiType;

    /** @ORM\Column(type="string", length=50, nullable=true) */
    private $poiType2;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $poiCreator;

    /** @ORM\Column(type="datetime") */
    private $downloadAt;

    /** @ORM\Column(type="string", length=16) */
    private $downloadType;

    /** @ORM\Column(type="integer") */
    private $poiNb;

    /**
     * @param mixed $organization
     *
     * @return Reuse
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @param mixed $appConsumerId
     */
    public function setAppConsumerId($appConsumerId)
    {
        $this->appConsumerId = $appConsumerId;
    }

    /**
     * @param mixed $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @param mixed $appName
     *
     * @return Reuse
     */
    public function setAppName($appName)
    {
        $this->appName = $appName;

        return $this;
    }

    /**
     * @param mixed $appDesc
     *
     * @return Reuse
     */
    public function setAppDesc($appDesc)
    {
        $this->appDesc = $appDesc;

        return $this;
    }

    /**
     * @param mixed $appUrl
     *
     * @return Reuse
     */
    public function setAppUrl($appUrl)
    {
        $this->appUrl = $appUrl;

        return $this;
    }

    /**
     * @param mixed $consumerId
     */
    public function setConsumerId($consumerId)
    {
        $this->consumerId = $consumerId;
    }

    /**
     * @param mixed $consumerName
     *
     * @return Reuse
     */
    public function setConsumerName($consumerName)
    {
        $this->consumerName = $consumerName;

        return $this;
    }

    /**
     * @param mixed $consumerUrl
     *
     * @return Reuse
     */
    public function setConsumerUrl($consumerUrl)
    {
        $this->consumerUrl = $consumerUrl;

        return $this;
    }

    /**
     * @param mixed $consumerType
     */
    public function setConsumerType($consumerType)
    {
        $this->consumerType = $consumerType;
    }

    /**
     * @param mixed $consumerZip
     */
    public function setConsumerZip($consumerZip)
    {
        $this->consumerZip = $consumerZip;
    }

    /**
     * @return mixed
     */
    public function getConsumerCountry()
    {
        return $this->consumerCountry;
    }

    /**
     * @param mixed $consumerCountry
     */
    public function setConsumerCountry($consumerCountry)
    {
        $this->consumerCountry = $consumerCountry;
    }

    /**
     * @param mixed $poiType
     *
     * @return Reuse
     */
    public function setPoiType($poiType)
    {
        $this->poiType = $poiType;

        return $this;
    }

    /**
     * @param mixed $poiType2
     */
    public function setPoiType2($poiType2)
    {
        $this->poiType2 = $poiType2;
    }

    /**
     * @param mixed $poiCreator
     */
    public function setPoiCreator($poiCreator)
    {
        $this->poiCreator = $poiCreator;
    }

    /**
     * @param mixed $downloadAt
     */
    public function setDownloadAt($downloadAt)
    {
        $this->downloadAt = $downloadAt;
    }

    /**
     * @param mixed $downloadType
     */
    public function setDownloadType($downloadType)
    {
        $this->downloadType = $downloadType;
    }

    /**
     * @param mixed $poiNb
     *
     * @return Reuse
     */
    public function setPoiNb($poiNb)
    {
        $this->poiNb = $poiNb;

        return $this;
    }

}
