<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Infos\Status\StatusInfos;
use AppBundle\Entity\Infos\Status\StatusInfosTrait;
use AppBundle\Entity\Status\StatusableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="flux")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FluxRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Flux
{
    use Timestampable, StatusableTrait, StatusInfosTrait;

    const STATUS_DRAFT = 'draft';
    const STATUS_PAUSED = 'paused';
    const STATUS_BLOCKED = 'blocked';
    const STATUS_PRODUCTION = 'production';

    // ___ Cosmetic purposes (used when flux in production is displaying last process status)
    const STATUS_SUCCESS = Process::STATUS_SUCCESS;
    const STATUS_WAIT = Process::STATUS_WAIT;
    const STATUS_RUNNING = Process::STATUS_RUNNING;
    const STATUS_ERROR = Process::STATUS_ERROR;
    // ________

    const MODE_PULL = 'pull';
    const MODE_PUSH = 'push';
    const MODE_MANUAL = 'manual';

    public static $statusDefinitions = array(
        self::STATUS_DRAFT => [
            'Brouillon',
            'Le flux est en cours de conception.',
        ],
        self::STATUS_PAUSED => [
            'En pause',
            'Le flux est actuellement en pause : il n\'est pas mis à jour.',
        ],
        self::STATUS_BLOCKED => [
            'Bloqué',
            'Le flux est actuellement bloqué.',
        ],
        self::STATUS_PRODUCTION => [
            'En production',
            'Le flux est actuellement en production.',
        ],

        // ____
        self::STATUS_SUCCESS => [
            'Opérationnel',
            'Le flux fonctionne correctement',
        ],
        self::STATUS_WAIT => [
            'En attente',
            'Le flux est en attente de traitement.',
        ],
        self::STATUS_RUNNING => [
            'En cours',
            'Le flux est en cours de traitement.',
        ],
        self::STATUS_ERROR => [
            'Erreur',
            'Le flux a rencontré une ou plusieurs erreurs bloquantes empêchant sa mise à jour.',
        ],
        // ________
    );

    public static $modeDefinitions = array(
        self::MODE_PUSH => [
            'Envoi',
            'Les fichiers sont transmis à l\'initiative d\'un serveur tiers',
        ],
        self::MODE_PULL => [
            'Récupération',
            'Les fichiers sont récupérés sur un serveur tiers.',
        ],
        self::MODE_MANUAL => [
            'Manuel',
            'Les fichiers sont transférés directement par l\'utilisateur.',
        ],
    );

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Organization
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="flux")
     */
    private $organization;

    /**
     * @var string
     * @ORM\Column(type="string", length=63, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=63)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=1023, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=1023)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $mode;

    /**
     * @var string
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Length(max=32)
     */
    protected $checksum;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $apiId;

    /**
     * @var string
     * @ORM\Column(type="string", length=511, nullable=true)
     * @Assert\Length(max=511)
     */
    private $successUrl;

    /**
     * @var string
     * @ORM\Column(type="string", length=511, nullable=true)
     * @Assert\Length(max=511)
     */
    private $failUrl;

    /**
     * @var AlignmentRuleset
     * @ORM\ManyToOne(targetEntity="AlignmentRuleset", inversedBy="flux")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $alignmentRuleset;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Process", mappedBy="flux")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $processes;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="RdfResource", mappedBy="flux")
     */
    private $rdfResources;

    /**
     * @var FluxCrawler
     * @ORM\OneToOne(targetEntity="FluxCrawler", mappedBy="flux", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $fluxCrawler;

    /**
     * @var ProcessStrategy
     * @ORM\ManyToOne(targetEntity="ProcessStrategy", inversedBy="flux")
     */
    private $processStrategy;

    private $reasonInvalidate;

    /**
     * @return mixed
     */
    public function getReasonInvalidate()
    {
        return $this->reasonInvalidate;
    }

    /**
     * @param mixed $reasonInvalidate
     */
    public function setReasonInvalidate($reasonInvalidate)
    {
        $this->reasonInvalidate = $reasonInvalidate;
    }

    /**
     * Flux constructor.
     */
    public function __construct()
    {
        $this->processes = new ArrayCollection();
        $this->rdfResources = new ArrayCollection();
        $this->status = self::STATUS_DRAFT;
        $this->mode = self::MODE_MANUAL;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getSuccessUrl()
    {
        return $this->successUrl;
    }

    /**
     * @param string $successUrl
     */
    public function setSuccessUrl($successUrl)
    {
        $this->successUrl = $successUrl;
    }

    /**
     * @return string
     */
    public function getFailUrl()
    {
        return $this->failUrl;
    }

    /**
     * @param string $failUrl
     */
    public function setFailUrl($failUrl)
    {
        $this->failUrl = $failUrl;
    }

    /**
     * @return AlignmentRuleset
     */
    public function getAlignmentRuleset()
    {
        return $this->alignmentRuleset;
    }

    /**
     * @param AlignmentRuleset $alignmentRuleset
     */
    public function setAlignmentRuleset($alignmentRuleset)
    {
        $this->alignmentRuleset = $alignmentRuleset;
    }

    /**
     * @return Process
     */
    public function getLastProcess()
    {
        return $this->getLastActiveProcess(false);
    }

    /**
     * @param bool|string $ignoreStatus
     *
     * @return Process
     */
    public function getLastActiveProcess($ignoreStatus = Process::STATUS_WAIT)
    {
        if ($this->processes->count() === 0) {
            return null;
        }

        foreach ($this->processes as $process) {
            if ($ignoreStatus && $ignoreStatus === $process->getStatus()) {
                continue;
            }
            if (!isset($currentProcess)) {
                $currentProcess = $process;
            }
            if ($process->getCreatedAt() > $currentProcess->getCreatedAt()) {
                $currentProcess = $process;
            }
            if ($process->getCreatedAt() === $currentProcess->getCreatedAt() &&
                $process->getId() > $currentProcess->getId()
            ) {
                $currentProcess = $process;
            }
        }

        return $currentProcess ?? null;
    }

    /**
     * Return true if the flux is currently waiting or running.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->getLastProcess() && in_array($this->getLastProcess()->getStatus(), [Process::STATUS_WAIT, Process::STATUS_RUNNING]);
    }

    /**
     * @return ArrayCollection
     */
    public function getProcesses()
    {
        return $this->processes;
    }

    /**
     * @param ArrayCollection $processes
     */
    public function setProcesses($processes)
    {
        $this->processes = $processes;
    }

    /**
     * @param Process $process
     *
     * @return bool
     */
    public function addProcess($process)
    {
        if (!$this->processes->contains($process)) {
            $this->processes->add($process);

            return true;
        }

        return false;
    }

    /**
     * @return ArrayCollection
     */
    public function getRdfResources()
    {
        return $this->rdfResources;
    }

    /**
     * @param ArrayCollection $rdfResources
     */
    public function setRdfResources($rdfResources)
    {
        $this->rdfResources = $rdfResources;
    }

    /**
     * @return FluxCrawler
     */
    public function getFluxCrawler()
    {
        return $this->fluxCrawler;
    }

    /**
     * @param FluxCrawler $fluxCrawler
     */
    public function setFluxCrawler($fluxCrawler)
    {
        $this->fluxCrawler = $fluxCrawler;
        $this->fluxCrawler->setFlux($this);
    }

    /**
     * @return ProcessStrategy
     */
    public function getProcessStrategy()
    {
        return $this->processStrategy;
    }

    /**
     * @param ProcessStrategy $processStrategy
     */
    public function setProcessStrategy(ProcessStrategy $processStrategy)
    {
        $this->processStrategy = $processStrategy;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return string
     */
    public function getApiId()
    {
        return $this->apiId;
    }

    /**
     * @param string $apiId
     */
    public function setApiId($apiId)
    {
        $this->apiId = $apiId;
    }

    /**
     * @ORM\PreFlush()
     */
    public function preFlush()
    {
        $this->apiId = md5($this->getOrganization()->getId().'/'.$this->getId().'/'.$this->getCreatedAt()->getTimestamp());
    }

    /**
     * @return array
     */
    public static function conceptionStatusList(): array
    {
        return array(
            self::STATUS_DRAFT,
        );
    }

    /**
     * @return array
     */
    public static function productionStatusList(): array
    {
        return array(
            self::STATUS_PRODUCTION,
            self::STATUS_PAUSED,
            self::STATUS_BLOCKED,
        );
    }

    /**
     * @return Process|null
     */
    public function getPrevActiveProcess()
    {
        $lastActiveProcess = $this->getLastActiveProcess();
        if ($lastActiveProcess) {
            $tempFlux = new self();
            $tempFlux->setProcesses(clone $this->getProcesses());
            $tempFlux->getProcesses()->removeElement($lastActiveProcess);

            return $tempFlux->getLastActiveProcess();
        }

        return null;
    }

    /**
     * @return AlignmentRulesetRevision|null
     */
    public function getActiveAlignmentRevision()
    {
        $active = $this->getAlignmentRuleset()->getActiveVersion();
        if ($active !== null) {
            foreach ($this->getAlignmentRuleset()->getRevisions() as $revision) {
                /** @var $revision AlignmentRulesetRevision */
                if ($revision->getVersion() == $active) {
                    return $revision;
                }
            }
        }

        return null;
    }

    public function getOneProcessByDay()
    {
        $currentDay = -1;
        if ($this->getProcesses()->count()) {
            /** @var Process[] $processes */
            $tabProcess = [];
            foreach ($this->getProcesses() as $process) {
                if ($process->getStatus() === Process::STATUS_SUCCESS) {
                    $day = $process->getCreatedAt()->format('z');
                    if ($day != $currentDay) {
                        $tabProcess[] = $process;
                        $currentDay = $day;
                    }
                }
            }

            return $tabProcess;
        }

        return [];
    }

    /**
     * {@inheritdoc}
     *
     * Uses Flux's status, or last Flux's Process status if Flux is in production.
     */
    public function getStatusInfos(): StatusInfos
    {
        if ($this->statusInfos) {
            return $this->statusInfos;
        }

        $status = (function (): string {
            $status = $this->getStatus();
            if ($status === self::STATUS_PRODUCTION) {
                $lastActiveProcess = $this->getLastProcess();
                if ($lastActiveProcess instanceof Process) {
                    return $lastActiveProcess->getStatus();
                } else {
                    return self::STATUS_SUCCESS;
                }
            } else {
                return $status;
            }
        })();

        $label = self::$statusDefinitions[$status][0];
        $description = self::$statusDefinitions[$status][1];

        $statusInfos = $this->createStatusInfos($status, $label, $description);
        $this->setStatusInfos($statusInfos);

        return $statusInfos;
    }

    /**
     * @return string
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    public function setNewChecksum()
    {
        $this->checksum = md5(openssl_random_pseudo_bytes(16));
    }

    /**
     * @param string $checksum
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;
    }
}
