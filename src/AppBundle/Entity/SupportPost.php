<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Datatourisme\Bundle\WebAppBundle\Entity\UploadableTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="support_post")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 */
class SupportPost
{
    use Timestampable, UploadableTrait;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=4095, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=4095)
     */
    private $content;

    /**
     * @var SupportThread
     * @ORM\ManyToOne(targetEntity="SupportThread", inversedBy="posts", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $thread;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="supportPosts")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return int file max size
     */
    public static function getMaxFileSize()
    {
        return 5 * 1024 * 1024;
    }

    /**
     * @return array
     */
    public static function getAllowedMimeTypes()
    {
        return array(
            'application/xml',
            'application/zip',
            'application/x-gzip',
            'application/x-tar',
            'application/x-compressed',
            'application/pdf',
            'application/excel',
            'application/msword',
            'application/mspowerpoint',
            'application/vnd.oasis.opendocument.text',
            'application/vnd.oasis.opendocument.text-master',
            'application/vnd.oasis.opendocument.spreadsheet',
            'application/vnd.oasis.opendocument.presentation',
            'image/tiff',
            'image/png',
            'image/jpeg',
            'image/bmp',
            'image/gif',
            'text/rtf',
            'text/plain',
        );
    }

    /**
     * @return array
     */
    public static function getNotAllowedMimeTypeMessage()
    {
        return 'Extension de fichier non autorisée.';
    }

    /**
     * @return SupportThread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param SupportThread $thread
     */
    public function setThread($thread)
    {
        $this->thread = $thread;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getTemplatesRootDir()
    {
        return __DIR__.'/../../../web/uploads/support';
    }
}
