<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Infos\Mode;

use AppBundle\Entity\Infos\AbstractInfos;

/**
 * Class ModeInfos.
 */
class ModeInfos extends AbstractInfos
{
    /**
     * @var string
     */
    private $mode;

    /**
     * ModeInfos constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->mode = null;
    }

    /**
     * @return string|null
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     *
     * @return self
     */
    public function setMode(string $mode): self
    {
        $this->mode = $mode;

        return $this;
    }
}
