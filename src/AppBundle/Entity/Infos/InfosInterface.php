<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Infos;

interface InfosInterface
{
    /**
     * @return string|null
     */
    public function getLabel();

    /**
     * @param string $label
     *
     * @return AbstractInfos
     */
    public function setLabel(string $label): AbstractInfos;

    /**
     * @return string|null
     */
    public function getDescription();

    /**
     * @param string $description
     *
     * @return AbstractInfos
     */
    public function setDescription(string $description): AbstractInfos;
}
