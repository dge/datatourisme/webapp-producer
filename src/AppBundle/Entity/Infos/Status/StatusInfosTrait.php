<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Infos\Status;

trait StatusInfosTrait
{
    /**
     * @var StatusInfos
     */
    private $statusInfos;

    /**
     * @param StatusInfos $statusInfos
     */
    public function setStatusInfos(StatusInfos $statusInfos)
    {
        $this->statusInfos = $statusInfos;
    }

    /**
     * @param string $status
     * @param string $label
     * @param string $description
     *
     * @return StatusInfos
     */
    public static function createStatusInfos(string $status, string $label, string $description): StatusInfos
    {
        $statusInfos = new StatusInfos();
        $statusInfos
            ->setStatus($status)
            ->setLabel($label)
            ->setDescription($description);

        return $statusInfos;
    }

    /**
     * Based on self status definitions, returns a StatusInfos instance with following properties:.
     *
     * - "status":      Machine code.
     * - "label":       Human readable title.
     * - "description": Human readable description.
     *
     * @return StatusInfos
     */
    abstract public function getStatusInfos(): StatusInfos;
}
