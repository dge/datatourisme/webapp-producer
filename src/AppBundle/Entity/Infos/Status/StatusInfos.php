<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Infos\Status;

use AppBundle\Entity\Infos\AbstractInfos;

/**
 * Class StatusInfos.
 */
class StatusInfos extends AbstractInfos
{
    /**
     * @var string
     */
    private $status;

    /**
     * StatusInfos constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->status = null;
    }

    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
