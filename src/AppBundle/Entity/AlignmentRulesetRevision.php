<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Infos\Status\StatusInfos;
use AppBundle\Entity\Infos\Status\StatusInfosTrait;
use AppBundle\Entity\Status\StatusableTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="alignment_ruleset_revision")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlignmentRulesetRevisionRepository")
 */
class AlignmentRulesetRevision
{
    use Timestampable, StatusableTrait, StatusInfosTrait;

    const STATUS_DRAFT = 'draft';
    const STATUS_PENDING = 'pending';
    const STATUS_ACTIVE = 'active';
    const STATUS_ARCHIVED = 'archived';

    // TODO: To be removed
    const STATUS_NONE = 'none';

    public static $statusDefinitions = array(
        self::STATUS_PENDING => [
            'Attente de validation',
            'Cette version est actuellement en attente de validation.',
        ],
        self::STATUS_DRAFT => [
            'Brouillon',
            'Cette version est actuellement en cours de conception.',
        ],
        self::STATUS_ACTIVE => [
            'Active',
            'Cette version est actuellement utilisée par le flux.',
        ],
        self::STATUS_ARCHIVED => [
            'Archivée',
            'Cette version a été archivée.',
        ],

        // TODO: To be removed
        self::STATUS_NONE => [
            'À créer',
            'Le flux ne possède pas encore de règles d\'alignement.',
        ],
    );

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $content;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank()
     */
    private $version;

    /**
     * @var AlignmentRuleset
     * @ORM\ManyToOne(targetEntity="AlignmentRuleset", inversedBy="revisions")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $alignmentRuleset;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $author;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $updater;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $comment;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getContent()
    {
        return $this->cleanEmptyRules($this->content);
    }

    /**
     * @param array $content
     */
    public function setContent(array $content)
    {
        $this->content = $this->cleanEmptyRules($content);
    }

    /**
     * @return int
     */
    public function hasRules()
    {
        return isset($this->content['rules']);
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    /**
     * @return AlignmentRuleset
     */
    public function getAlignmentRuleset()
    {
        return $this->alignmentRuleset;
    }

    /**
     * @param AlignmentRuleset $alignmentRuleset
     */
    public function setAlignmentRuleset(AlignmentRuleset $alignmentRuleset)
    {
        $this->alignmentRuleset = $alignmentRuleset;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @return User
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * @param User $updater
     */
    public function setUpdater(User $updater)
    {
        $this->updater = $updater;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $comment = trim($comment);
        $this->comment = empty($comment) ? null : $comment;
    }

    /**
     * @return array
     */
    public function getStatusDefinitions(): array
    {
        return self::$statusDefinitions;
    }

    /**
     * getStatusInfos
     */
    public function getStatusInfos(): StatusInfos
    {
        if ($this->statusInfos) {
            return $this->statusInfos;
        }

        $status = $this->getStatus();
        $statusInfos = $this->createStatusInfos($status, self::$statusDefinitions[$status][0], self::$statusDefinitions[$status][1]);
        $this->setStatusInfos($statusInfos);

        return $this->statusInfos;
    }

    /**
     * Recursive method to remove empty rules array
     *
     * @param $content
     * @return array
     */
    private function cleanEmptyRules($content)
    {
        if(isset($content['rules'])) {
            if(empty($content['rules'])) {
                unset($content['rules']);
            } else {
                foreach($content['rules'] as $key => $rule) {
                    $content['rules'][$key] = $this->cleanEmptyRules($rule);
                }
            }
        }
        return $content;
    }
}
