<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Datatourisme\Bundle\WebAppBundle\Mailer\MailerRecipientInterface;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields="email",
 *     message="Il existe déjà un utilisateur avec cette adresse mail"
 * )
 */
class User implements AdvancedUserInterface, \Serializable, MailerRecipientInterface
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=60, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=60)
     * @Assert\Email(
     *     message = "L'email renseigné n'est pas valide."
     * )
     */
    private $email;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=255)
     */
    private $password;

    /**
     * Encrypted previous passwords.
     *
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $previousPasswords;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    private $plainPassword;

    /**
     * @var bool
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $enabled;

    /**
     * @var bool
     * @ORM\Column(name="account_non_expired", type="boolean")
     */
    private $accountNonExpired;

    /**
     * @var bool
     * @ORM\Column(name="credentials_non_expired", type="boolean")
     */
    private $credentialsNonExpired;

    /**
     * @var bool
     * @ORM\Column(name="account_non_locked", type="boolean")
     */
    private $accountNonLocked;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="LoginHistory", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $loginHistory;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"unsigned"=true}, nullable=true)
     */
    protected $failLoginNumberAttempts;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $failLoginDatetime;

    /**
     * @var array
     * @ORM\Column(type="string", length=24, nullable=false)
     * @Assert\Length(max=24)
     */
    private $role;

    /**
     * @var Organization
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    protected $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    protected $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    protected $function;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^(\+\d+(\s|-|.))?0\d(\s|-|.)?(\d{2}(\s|-|.)?){4}$/", message="Vous devez renseigner un numéro de téléphone valide")
     * @Assert\Length(max=20)
     */
    protected $phoneNumber;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $notificationReadTime;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $excludedMailerNotificationTypes;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $qaPlatformAccess = false;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->enabled = false;
        $this->accountNonExpired = true;
        $this->credentialsNonExpired = true;
        $this->accountNonLocked = true;
        $this->previousPasswords = array();
        $this->loginHistory = new ArrayCollection();
        $this->role = 'ROLE_USER';
    }

    /**
     * @return bool
     */
    public function isLoginable()
    {
        if ($this->failLoginDatetime > (new \DateTime())) {
            return false;
        }
        if (!$this->accountNonLocked) {
            return false;
        }
        if (!$this->credentialsNonExpired) {
            return false;
        }
        if (!$this->accountNonExpired) {
            return false;
        }
        if (!$this->enabled) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getAccountStatus()
    {
        if (!$this->accountNonLocked) {
            return array(
                'class' => 'danger',
                'label' => 'Bloqué',
            );
        }
        if (!$this->credentialsNonExpired) {
            return array(
                'class' => 'warning',
                'label' => 'Mot de passe expiré',
            );
        }
        if (!$this->accountNonExpired) {
            return array(
                'class' => 'warning',
                'label' => 'Expiré',
            );
        }
        if ($this->failLoginDatetime > (new \DateTime())) {
            return array(
                'class' => 'warning',
                'label' => 'Temporairement bloqué',
            );
        }
        if (!$this->enabled) {
            return array(
                'class' => 'default',
                'label' => 'En attente',
            );
        }

        return array(
            'class' => 'success',
            'label' => 'Validé',
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFullName();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function getFullName()
    {
        return $this->firstName.' '.mb_strtoupper($this->lastName, 'UTF-8');
    }

    public function getReverseFullName()
    {
        return mb_strtoupper($this->lastName, 'UTF-8').' '.$this->firstName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = mb_strtolower($email, 'UTF-8');
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        $organization->getUsers()->add($this);
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param LifecycleEventArgs $args
     * @ORM\PrePersist()
     */
    public function prePersistUpload(LifecycleEventArgs $args)
    {
        $this->upperLastName();

        if (in_array($this->role, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            $this->organization = null;
        }
    }

    /**
     * @param PreUpdateEventArgs $args
     * @ORM\PreUpdate()
     */
    public function preUpdateUpload(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('lastName')) {
            $this->upperLastName();
        }

        if ($args->hasChangedField('role')) {
            if (in_array($this->role, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
                $this->organization = null;
            }
        }
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param string $function
     */
    public function setFunction($function)
    {
        $this->function = $function;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function getPreviousPasswords()
    {
        return $this->previousPasswords;
    }

    /**
     * @param array $previousPasswords
     */
    public function setPreviousPasswords($previousPasswords)
    {
        $this->previousPasswords = $previousPasswords;
    }

    /**
     * @param $previousPassword
     */
    public function addPreviousPassword($previousPassword)
    {
        $timestamp = (new \DateTime())->getTimestamp();
        $this->previousPasswords[strval($timestamp)] = $previousPassword;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * @param bool $accountNonExpired
     */
    public function setAccountNonExpired($accountNonExpired)
    {
        $this->accountNonExpired = $accountNonExpired;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * @param bool $credentialsNonExpired
     */
    public function setCredentialsNonExpired($credentialsNonExpired)
    {
        $this->credentialsNonExpired = $credentialsNonExpired;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * @param bool $accountNonLocked
     */
    public function setAccountNonLocked($accountNonLocked)
    {
        $this->accountNonLocked = $accountNonLocked;
    }

    /**
     * @return ArrayCollection
     */
    public function getLoginHistory()
    {
        return $this->loginHistory;
    }

    /**
     * @param ArrayCollection $loginHistory
     */
    public function setLoginHistory($loginHistory)
    {
        $this->loginHistory = $loginHistory;
    }

    /**
     * Add a new user connection.
     */
    public function addLoginHistory($datetime)
    {
        $connection = new LoginHistory($this, $datetime);

        $this->loginHistory->add($connection);
    }

    /**
     * @return mixed
     */
    public function getFailLoginDatetime()
    {
        return $this->failLoginDatetime;
    }

    /**
     * @param mixed $failLoginDatetime
     */
    public function setFailLoginDatetime($failLoginDatetime)
    {
        $this->failLoginDatetime = $failLoginDatetime;
    }

    /**
     * @return int
     */
    public function getFailLoginNumberAttempts()
    {
        return $this->failLoginNumberAttempts;
    }

    /**
     * @param int $failLoginNumberAttempts
     */
    public function setFailLoginNumberAttempts($failLoginNumberAttempts)
    {
        $this->failLoginNumberAttempts = $failLoginNumberAttempts;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return array($this->role);
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @param string|array $role
     *
     * @return bool
     */
    public function isRole($role)
    {
        if (is_string($role)) {
            return $role === $this->role;
        } elseif (is_array($role)) {
            foreach ($role as $_role) {
                if ($_role === $this->role) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return \DateTime
     */
    public function getLastLogin()
    {
        $lastLogin = $this->getLoginHistory()->last();

        return $lastLogin ? $lastLogin->getDatetime() : null;
    }

    /**
     * @return \DateTime
     */
    public function getNotificationReadTime()
    {
        return $this->notificationReadTime;
    }

    /**
     * @param \DateTime $notificationReadTime
     */
    public function setNotificationReadTime($notificationReadTime)
    {
        $this->notificationReadTime = $notificationReadTime;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return null;
    }

    /**
     * @param array $excludedMailerNotificationTypes
     */
    public function setExcludedMailerNotificationTypes($excludedMailerNotificationTypes)
    {
        $this->excludedMailerNotificationTypes = count($excludedMailerNotificationTypes) ? $excludedMailerNotificationTypes : null;
    }

    /**
     * @return array
     */
    public function getExcludedMailerNotificationTypes()
    {
        return $this->excludedMailerNotificationTypes;
    }


    /**
     * @param string $className
     * @return bool
     */
    public function isExcludedMailerNotificationType($className)
    {
        if ($this->excludedMailerNotificationTypes !== null) {
            return in_array($className, $this->excludedMailerNotificationTypes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isMailingGranted($type, $data)
    {
        if ($type == "notification") {
            $notification = $data['context']['notification'];
            $class = get_class($notification);
            return !$this->isExcludedMailerNotificationType($class);
        }
        return true;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->enabled,
            $this->accountNonExpired,
            $this->credentialsNonExpired,
            $this->accountNonLocked,
            $this->role,
            $this->failLoginNumberAttempts,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->enabled,
            $this->accountNonExpired,
            $this->credentialsNonExpired,
            $this->accountNonLocked,
            $this->role,
            $this->failLoginNumberAttempts,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    protected function upperLastName()
    {
        $this->setLastName(mb_strtoupper($this->getLastName(), 'UTF-8'));
    }

    /**
     * Get the value of qaPlatformAccess
     *
     * @return  boolean
     */ 
    public function getQaPlatformAccess()
    {
        return $this->qaPlatformAccess;
    }

    /**
     * Set the value of qaPlatformAccess
     *
     * @param  boolean  $qaPlatformAccess
     *
     * @return  self
     */ 
    public function setQaPlatformAccess($qaPlatformAccess)
    {
        $this->qaPlatformAccess = $qaPlatformAccess;

        return $this;
    }
}
