<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name = "flux_stats_entry",
 *     indexes = {
 *         @ORM\Index(name = "date", columns = { "date" })
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FluxStatsEntryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class FluxStatsEntry extends AbstractStatsEntry
{
    /**
     * @var int;
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Flux
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="Flux", inversedBy="statsEntries")
     */
    private $flux;

    /**
     * @var Organization
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="statsEntries")
     */
    private $organization;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
     */
    private $nbPublishedRdfResources;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false, options={"default": 0})
     */
    private $publicationRatio;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $fluxStatus;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Flux
     */
    public function getFlux(): Flux
    {
        return $this->flux;
    }

    /**
     * @param Flux $flux
     */
    public function setFlux(Flux $flux)
    {
        $this->flux = $flux;
    }

    /**
     * @return Organization
     */
    public function getOrganization(): Organization
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return int
     */
    public function getNbPublishedRdfResources(): int
    {
        return $this->nbPublishedRdfResources;
    }

    /**
     * @param int $nbPublishedRdfResources
     */
    public function setNbPublishedRdfResources(int $nbPublishedRdfResources)
    {
        $this->nbPublishedRdfResources = $nbPublishedRdfResources;
    }

    /**
     * @return float
     */
    public function getPublicationRatio(): float
    {
        return $this->publicationRatio;
    }

    /**
     * @param float $publicationRatio
     */
    public function setPublicationRatio(float $publicationRatio)
    {
        $this->publicationRatio = $publicationRatio;
    }

    /**
     * @return string
     */
    public function getFluxStatus(): string
    {
        return $this->fluxStatus;
    }

    /**
     * @param string $fluxStatus
     */
    public function setFluxStatus(string $fluxStatus)
    {
        $this->fluxStatus = $fluxStatus;
    }
}
