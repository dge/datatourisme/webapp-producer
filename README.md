DATAtourisme - Application producteurs
========================

L'application producteurs DATAtourisme permet aux producteurs de données
de configurer et suivre le traitement des flux en provenance de leur SIT.

Fonctionnalités
--------------

L'application producteurs inclut les fonctionnalités suivantes :

  * Gestion des utilisateurs et des structures

  * Configuration des règles d'alignement du thésaurus

  * Configuration et versionage des règles d'alignement de l'ontologie

  * Suivi de l'exécution des traitements

  * Centre de support des utilisateurs

  * Foire aux questions

  * Statistiques de réutilisation des POI

Installation
--------------

Attention, YARN doit être executé avec node 9.

```
git submodule update --init
docker-compose up -d
composer install
yarn
gulp
```

Configuration
--------------

```
sudo setfacl -R -m g:www-data:rwX var
sudo setfacl -dR -m g:www-data:rwX var
```