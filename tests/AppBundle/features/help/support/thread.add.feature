Feature: DAT-57 Amorcer une discussion (producteur) / DAT-58 Amorcer une discussion (administrateur)
  L'utilisateur est en mesure d'amorcer une didcussion.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | support_thread.yml |
      | support_post.yml |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux amorcer une discussion avec une structure depuis son profil.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "admin/organization"
    When I follow "Dummy Empire"
    And I click the ".nav-tabs > li:last-child > a" element
    Then I should see "Contacter la structure"

    When I follow "Contacter"
    Then I should see "Créer une nouvelle discussion"
    And I should see "Dummy Empire" in the "#support_post_thread_organization" element

    When I fill in the following:
      | support_post[thread][title]         | Dummy New Thread 1 |
      | support_post[content]               | Dummy New Post 1   |
    And I press "Envoyer"

    Then the response status code should be 200
    And I should see "Revenir au centre de support"
    And I should see "Dummy New Thread 1"
    And I should see "Dummy New Post 1"

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux amorcer une discussion.
    Given I am logged in as "user@dt.dev" with "user"
    And I am on "help/support"
    When I follow "Nouvelle discussion"
    And I fill in the following:
      | support_post[thread][title]         | Dummy New Thread 2 |
      | support_post[content]               | Dummy New Post 2   |
    And I press "Envoyer"
    Then the response status code should be 200
    And I should see "Revenir au centre de support"
    And I should see "Dummy New Thread 2"
    And I should see "Dummy New Post 2"