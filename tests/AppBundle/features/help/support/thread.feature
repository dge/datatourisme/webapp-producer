Feature: DAT-61 Détail d'une discussion
  L'utilisateur est en mesure d'afficher une discussion.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | support_thread.yml |
      | support_post.yml |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux afficher n'importe quelle discussion.
    Given I am logged in as "admin@dt.dev" with "admin"
    When I go to "help/support/thread" with random "SupportThread" id
    Then the response status code should be 200

  Scenario: En tant qu'administrateur, je peux afficher le nom des administrateurs.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/support"
    When I follow "Dummy Thread 1"
    Then I should see "John DOE"
    And I should see "Équipe support"

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux afficher une discussion liée à ma structure.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "help/support"
    When I follow "Dummy Thread 1"
    Then the response status code should be 200

  Scenario: En tant qu'utilisateur, je ne peux pas afficher le nom des administrateurs.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "help/support"
    When I follow "Dummy Thread 1"
    Then I should see "Équipe support"
    But I should not see "John DOE"