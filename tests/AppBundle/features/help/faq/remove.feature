Feature: DAT-14 FAQ - Supprimer
  L'administrateur est en mesure de supprimer une FAQ.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | faq_theme.yml |
      | faq.yml |

  # ADMINISTRATEUR
  Scenario: L'administrateur peut afficher la page de confirmation et supprimer une question existante.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/faq"
    When I go to "help/faq/remove" with random "Faq" id
    And I submit the ".modal-body > form" form
    Then the response status code should be 200

  Scenario: L'administrateur peut afficher la page de confirmation et supprimer une question nouvellement créée.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/faq/add"
    When I fill in the following:
      | faq[question]        | Dummy Question Delete 1     |
      | faq[theme]           | new                         |
      | faq[new_theme]       | Dummy Theme Delete 1        |
      | faq[answer]          | Lorem ipsum dolor sit amet. |
    And I press "Enregistrer"
    Then the response status code should be 200
    And I should see "Dummy Question Delete 1"
    When I click the ".faq-remove" element
    Then the response status code should be 200
    When I submit the ".modal-body > form" form
    Then the response status code should be 200

  Scenario: La précédente suppression de l'unique question d'un thème doit avoir supprimé ce thème.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/faq/add"
    Then I should not see "Dummy Theme Delete 1"

  # UTILISATEUR
  Scenario: L'utilisateur ne peut pas afficher le bouton de suppression.
    Given I am logged in as "faq@dt.dev" with "faq"
    And I am on "help/faq"
    Then I should not see an ".faq-remove" element
    When I go to "help/faq/remove" with random "Faq" id
    Then the response status code should be 403