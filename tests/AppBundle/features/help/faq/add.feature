Feature: DAT-12 FAQ - Ajouter
  L'administrateur est en mesure d'ajouter une FAQ.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | faq_theme.yml |
      | faq.yml |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux afficher la page d'ajout de question.
    Given I am logged in as "admin@dt.dev" with "admin"
    When I go to "help/faq/add"
    Then I should be on "help/faq/add"
    And the response status code should be 200

  Scenario: En tant qu'administrateur, je peux créer une nouvelle question avec un thème existant.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/faq/add"
    When I fill in the following:
      | faq[question] | Dummy Question Add 1        |
      | faq[theme]    | Et velit qui.               |
      | faq[answer]   | Lorem ipsum dolor sit amet. |
    And I press "Enregistrer"
    Then the response status code should be 200

  Scenario: En tant qu'administrateur, je peux créer une nouvelle question associée à un nouveau thème (puis nettoyage pour la suite des tests).
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/faq/add"
    When I fill in the following:
      | faq[question]        | Dummy Question Add 2        |
      | faq[theme]           | new                         |
      | faq[new_theme]       | Dummy Theme Add 1           |
      | faq[answer]          | Lorem ipsum dolor sit amet. |
    And I press "Enregistrer"
    Then the response status code should be 200
    And I should see "Dummy Theme Add 1"
    When I click the ".faq-remove" element
    Then the response status code should be 200
    When I submit the ".modal-body > form" form
    Then the response status code should be 200

  Scenario: En tant qu'administrateur, je peux directement pré-selectionner un thème par l'url.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "help/faq/add" with random "FaqTheme" id
    When I fill in the following:
      | faq[question] | Dummy Question Add 3        |
      | faq[answer]   | Lorem ipsum dolor sit amet. |
    And I press "Enregistrer"
    Then the response status code should be 200

  # UTILISATEUR
  Scenario: L'utilisateur ne peut pas afficher la page d'ajout.
    Given I am logged in as "faq@dt.dev" with "faq"
    Given I am on "help/faq"
    Then I should not see "Nouvelle question" in the ".content-header" element
    When I go to "help/faq/add"
    Then I should be on "help/faq/add"
    But the response status code should be 403