Feature: DAT-13 FAQ - Modifier
  L'administrateur est en mesure de modifier une question.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | faq_theme.yml |
      | faq.yml |

  # ADMINISTRATEUR
  Scenario: L'administrateur peut afficher la page d'édition de question.
    Given I am logged in as "admin@dt.dev" with "admin"
    Given I am on "help/faq"
    When I click the ".faq-edit" element
    Then the response status code should be 200

  Scenario: L'administrateur peut modifier une FAQ.
    Given I am logged in as "admin@dt.dev" with "admin"
    When I go to "help/faq/edit" with random "Faq" id
    Then the response status code should be 200
    When I fill in the following:
      | faq[question] | Lorem ipsum modifié ?       |
      | faq[theme]    | Et velit qui.               |
      | faq[answer]   | Lorem ipsum dolor sit amet. |
    And I press "Enregistrer"
    Then the response status code should be 200

  Scenario: Si la question modifiée change de thème et que le thème d'origine se retrouve vide, ce dernier est supprimé.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "help/faq/add"
    When I fill in the following:
      | faq[question]        | Dummy Question Edit 1       |
      | faq[theme]           | new                         |
      | faq[new_theme]       | Dummy Theme Edit 1          |
      | faq[answer]          | Lorem ipsum dolor sit amet. |
    And I press "Enregistrer"
    Then the response status code should be 200
    And I should see "Dummy Theme Edit 1"
    Then I click the ".faq-edit" element
    Then I fill in the following:
      | faq[question]       | Dummy Question Edit 1        |
      | faq[theme]          | Et velit qui.                |
      | faq[answer]         | Lorem ipsum dolor sit amet.  |
    When I press "Enregistrer"
    Then the response status code should be 200
    And I should not see "Dummy Theme Edit 1"

  # UTILISATEUR
  Scenario: L'utilisateur ne peut pas afficher la page d'édition.
    Given I am logged in as "faq@dt.dev" with "faq"
    When I go to "help/faq/edit" with random "Faq" id
    Then the response status code should be 403

  Scenario: L'utilisateur ne peut pas voir les boutons d'édition sur les questions.
    Given I am logged in as "faq@dt.dev" with "faq"
    And I am on "help/faq"
    Then I should not see an ".faq-edit" element