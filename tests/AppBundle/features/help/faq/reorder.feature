Feature: DAT-17 FAQ - Remonter / Redescendre
  L'administrateur est en mesure de modifier l'ordre des questions.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | faq_theme.yml |
      | faq.yml |

  # ADMINISTRATEUR
  Scenario: L'administrateur peut remonter ou redescendre une question.
    Given I am logged in as "admin@dt.dev" with "admin"
    Given I am on "help/faq"
    When I click the ".faq-up" element
    Then the response status code should be 200
    Given I am on "help/faq"
    When I click the ".faq-down" element
    Then the response status code should be 200

  # UTILISATEUR
  Scenario: L'utilisateur ne peut pas afficher les boutons "Up" ou "Down".
    Given I am logged in as "faq@dt.dev" with "faq"
    And I am on "help/faq"
    Then I should not see an ".faq-up" element
    And I should not see an ".faq-down" element

  Scenario: L'utilisateur ne peut pas atteindre l'url de l'action.
    Given I am logged in as "faq@dt.dev" with "faq"
    When I go to "help/faq/reorder" with random "Faq" id
    Then the response status code should be 403