Feature: DAT-65 Widget dashboard
  L'utilisateur est en mesure d'afficher les derniers messages dans un widget du dashboard.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | support_thread.yml |
      | support_post.yml |

  Scenario: Je peux afficher le widget des discussions.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "/dashboard/widget/support"
    Then the response status code should be 200
    Then I should see "Derniers messages"
    And I should see "Dummy Thread 1"
    And I should see "Ivan DOV"

  Scenario: Je ne peux afficher que les discussions de ma structure.
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "/dashboard/widget/support"
    Then the response status code should be 200
    Then I should see "Derniers messages"
    But I should not see "Dummy Thread 1"