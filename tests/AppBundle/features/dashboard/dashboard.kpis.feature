Feature: Widget dashboard
  L'utilisateur est en mesure d'afficher les KPI dans un widget du dashboard.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  # ADMINISTRATEUR
  Scenario: Je peux afficher le widget des KPI.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "/dashboard/widget/kpis"
    Then the response status code should be 200
    Then I should see "Flux en production"
    And I should see "POI publiés"
    And I should see "Taux de publication"
    And I should see "POI réutilisés"

  # UTILISATEUR
  Scenario: Je peux afficher le widget des KPI.
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "/dashboard/widget/kpis"
    Then the response status code should be 200
    Then I should see "Flux en production"
    And I should see "POI publiés"
    And I should see "Taux de publication"
    And I should see "POI réutilisés"