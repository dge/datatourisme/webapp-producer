Feature: API interne : Thesaurus

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  Scenario: Par 127.0.0.1, je peux obtenir récupérer le thésaurus d'une structure au format JSON.
    Given I go to "/internal/api/1/thesaurus"
    Then the response status code should be 200
    And the response headers should contain "content-type":"application/json"
    And the response body should contain valid JSON
    Given I go to "/internal/api/666/thesaurus"
    Then the response status code should be 404