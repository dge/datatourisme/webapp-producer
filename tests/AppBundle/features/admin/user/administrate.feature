Feature: Administration des utilisateurs

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: DAT-39 Substitution de session
    When I am logged in as "admin@dt.dev" with "admin"
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I follow "Se connecter"
    Then I should be on "/"
    When I check the last received email
    Then the subject should contain "Un administrateur a pris le contrôle de votre compte"
    Then I click the ".bg-red" element
    Then the url should match "/admin/user/edit/\d+/administrate"
    Then I should see "John DOE"

  Scenario: DAT-53 Bloquage et débloquage des utilisateurs
    When I am logged in as "admin@dt.dev" with "admin"
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I follow "Bloquer"
    Then I should see "Bloquer l'utilisateur"
    Then I submit the "form" form
    Then the response status code should be 200
    When I check the last received email
    Then the subject should contain "Votre compte a été bloqué"
    Then I disconnect
    Then I am logged in as "user@dt.dev" with "user"
    Then I should see "Le compte est bloqué"
    When I am logged in as "admin@dt.dev" with "admin"
    Then I go to "/admin/user?user_filter%5BfullName%5D=Jean"
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I follow "Débloquer"
    Then I should see "Débloquer l'utilisateur"
    Then I submit the "form" form
    Then the response status code should be 200
    When I check the last received email
    Then the subject should contain "Votre compte a été débloqué"

  Scenario: Pouvoir supprimer un utilisateur
    When I am logged in as "admin@dt.dev" with "admin"
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I click the ".btn.btn-danger" element
    Then I submit the "form" form
    Then the response status code should be 200
    Then I disconnect
    Then I am logged in as "user@dt.dev" with "user"
    Then I should see "Identifiants invalides"