Feature: DAT-51 Création d'un compte

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: En tant qu'administrateur, je peux créer un nouveau compte utilisateur et sa nouvelle structure
    When I am logged in as "admin@dt.dev" with "admin"
    When I go to "/admin/user/add"
    When I fill in the following:
      | user[lastName]     | DEAU           |
      | user[firstName]    | Jean           |
      | user[function]     | Dummy          |
      | user[email]        | user@jean.deau |
      | user[phoneNumber]  | 0666666666     |
      | user[organization] | new            |
    And I press "Enregistrer"
    When I fill in the following:
      | user_organization[organization][name]        | Conjectest                |
      | user_organization[organization][address]     | 29 Rue de Lorient         |
      | user_organization[organization][zip]         | 35000                     |
      | user_organization[organization][city]        | Rennes                    |
      | user_organization[organization][phoneNumber] | 09 80 52 20 21            |
      | user_organization[organization][website]     | http://www.conjecto.com/  |
    And I press "Enregistrer"
    Then I go to "/admin/user?user_filter%5BfullName%5D=Jean"
    Then I should see "DEAU Jean"
    Then I go to "/admin/organization"
    Then I should see "Conjectest"
    Then I go to "/admin/user/add"
    When I fill in the following:
      | user[lastName]     | tourisme           |
      | user[firstName]    | DATA               |
      | user[function]     | Tester             |
      | user[email]        | user@jean.deau     |
      | user[phoneNumber]  | 06 07 08 09 10     |
    And I select "Conjectest" from "user[organization]"
    And I press "Enregistrer"
    Then I should see "Il existe déjà un utilisateur avec cette adresse mail"