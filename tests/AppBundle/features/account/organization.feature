Feature: DAT-6 Profil utilisateur

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: Je peux modifier les informations de ma structure
    When I am logged in as "user@dt.dev" with "user"
    When I go to "/account/organization"
    When I fill in the following:
      | organization[name]        | Conjecto                  |
      | organization[address]     | 29 Rue de Lorient         |
      | organization[zip]         | 35000                     |
      | organization[city]        | Rennes                    |
      | organization[website]     | http://www.conjecto.com/  |
    And I press "Enregistrer"
    Then I go to "/account/organization"
    Then I should see "Jean DEAU Conjecto"