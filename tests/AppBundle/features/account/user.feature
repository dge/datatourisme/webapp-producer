Feature: DAT-6 Profil utilisateur

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: Je peux modifier de mon profil
    When I am logged in as "user@dt.dev" with "user"
    And I go to "/account"
    When I fill in the following:
      | account[lastName]     | tourisme           |
      | account[firstName]    | Data               |
      | account[function]     | Tester             |
      | account[phoneNumber]  | 06 07 08 09 10     |
    And I press "Enregistrer"
    Then I should see "Data TOURISME"
    When I fill in the following:
      | account[lastName]  | DEAU |
      | account[firstName] | Jean |
    And I press "Enregistrer"
    Then I should see "Jean DEAU"
