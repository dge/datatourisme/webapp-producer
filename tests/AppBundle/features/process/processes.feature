Feature: Historique des traitements
  En tant qu'utilisateur, je peux consulter l'historique des traitements.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

   # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux afficher la liste.
    Given I am logged in as "admin@dt.dev" with "admin"
    When I go to "processes"
    Then I should be on "processes"
    And the response status code should be 200

  Scenario: Les traitement sont paginés.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "processes"
    When I follow "2"
    Then I should be on "processes"
    And the response status code should be 200

  Scenario: Je peux trier les résultats.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "processes"
    When I follow "Date / Heure"
    Then the response status code should be 200
    When I follow "Structure"
    Then the response status code should be 200
    When I follow "Flux"
    Then the response status code should be 200
    When I follow "Mise à jour"
    Then the response status code should be 200
    When I follow "Statut"
    Then the response status code should be 200
    When I follow "Fichiers"
    Then the response status code should be 200
    When I follow "Durée"
    Then the response status code should be 200
    When I follow "% publication"
    Then the response status code should be 200
    When I follow "Évolution"
    Then the response status code should be 200
    When I follow "Anomalies"
    Then the response status code should be 200

  Scenario: Je peux filtrer les résultats.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "processes"
    When I fill in "process_filter_organization" with "Dummy Kingdom"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200
    When I fill in "process_filter_flux" with "Voluptas"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200
    When I fill in "process_filter_status" with "success"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux afficher la liste.
    Given I am logged in as "juan@dos.dev" with "juan"
    When I go to "processes"
    Then I should be on "processes"
    And the response status code should be 200

  Scenario: En tant qu'utilisateur, je ne peux pas trier les résultats par structure.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "processes"
    Then I should be on "processes"
    And I should not see "Structure" in the "thead" element