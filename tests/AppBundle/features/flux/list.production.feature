Feature: DAT-80 Liste des flux en production
  En tant qu'utilisateur, je peux consulter la liste des flux en production.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux afficher la liste.
    Given I am logged in as "admin@dt.dev" with "admin"
    When I go to "flux/production"
    Then the response status code should be 200

  Scenario: Les flux sont paginés.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "flux/production"
    When I follow "2"
    Then the response status code should be 200

  Scenario: Je peux trier les résultats.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "flux/production"
    When I follow "Structure"
    Then the response status code should be 200
    When I follow "Flux"
    Then the response status code should be 200
    When I follow "Statut"
    Then the response status code should be 200
    When I follow "Jeu de règles"
    Then the response status code should be 200
    When I follow "Dernier traitement"
    Then the response status code should be 200
    When I follow "POI"
    Then the response status code should be 200

  Scenario: Je peux filtrer les résultats.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I am on "flux/production"
    When I fill in "production_flux_filter_organization" with "1"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200
    When I fill in "production_flux_filter_name" with "Lorem"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200
    When I fill in "production_flux_filter_ruleset" with "pending"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux afficher la liste.
    Given I am logged in as "juan@dos.dev" with "juan"
    When I go to "flux/production"
    Then the response status code should be 200

  Scenario: En tant qu'utilisateur, je ne peux pas trier les résultats par structure.
    Given I am logged in as "juan@dos.dev" with "juan"
    And I am on "flux/production"
    Then I should not see "Structure" in the "thead" element

  Scenario: Je peux filtrer les résultats.
    When I fill in "production_flux_filter_status" with "running"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200