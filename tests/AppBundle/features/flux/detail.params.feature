Feature: Détail d'un flux : Paramètres
  En tant qu'utilisateur, je peux modifer les paramètres d'un flux

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |

  Scenario: J'édite les paramètres d'un flux
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "flux/1"
    Then the response status code should be 200
    And I follow "Paramètres"
    Then the response status code should be 200
    When I fill in the following:
      | flux_details[name]                    | test                   |
      | flux_details[description]             | test                   |
      | flux_details[processStrategy]         | 2                      |
      | flux_details[mode]                    | pull                   |
      | flux_details[fluxCrawler][url]        | http://dt.dev/feed.xml |
      | flux_details[fluxCrawler][sshKey]     | this_is_a_gret_ssh_key |
      | flux_details[success_url]             | http://test.dev        |
      | flux_details[fail_url]                | http://test.dev        |
    And I submit the "form" form
    And I should see "Le protocole HTTP n'est pas autorisé"
    When I fill in the following:
      | flux_details[name]                    | test                   |
      | flux_details[description]             | test                   |
      | flux_details[processStrategy]         | 2                      |
      | flux_details[mode]                    | pull                   |
      | flux_details[fluxCrawler][url]        | sftp://dt.dev/feed.xml |
      | flux_details[fluxCrawler][sshKey]     | this_is_a_gret_ssh_key |
      | flux_details[success_url]             | http://test.dev        |
      | flux_details[fail_url]                | http://test.dev        |
    And I submit the "form" form
    Then the response status code should be 200
