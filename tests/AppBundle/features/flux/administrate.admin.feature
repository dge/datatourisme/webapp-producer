Feature: Administration d'un flux

  Scenario: En tant qu'admin, je peux gérer les étapes d'un flux
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process_strategy.yml           |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  Scenario: En tant qu'administrateur, je peux accepter la mise en production
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "flux/1/administrate"
    Then I should see "Valider la mise en production"
    And I follow "Valider"
    Then I should see "Êtes-vous sûr de vouloir accepter la mise en production du flux"
    Then I submit the "form" form
    Then the response status code should be 200
    Then I go to "flux/1/administrate"
    Then I should see "Mettre en veille"
    # Notifications
    Then I check the last received email
    Then the subject should contain "a été validé"

  Scenario: En tant qu'utilisateur, je vois la notification envoyée par l'administrateur
    Given I am logged in as "user@dt.dev" with "user"
    Then I go to "notification"
    Then I should see "a été validé"

  Scenario: En tant qu'administrateur, je peux mettre en veille un flux en production
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "flux/1/administrate"
    Then I should see "Mettre en veille"
    And I follow "Mettre en veille"
    Then I should see "Êtes-vous sûr de vouloir mettre en veille le flux"
    Then I submit the "form" form
    Then the response status code should be 200
    Then I go to "flux/1/administrate"
    Then I should see "En pause"

  Scenario: En tant qu'administrateur, je peux sortir de veille un flux en production
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "flux/1/administrate"
    Then I should see "Sortir de veille"
    And I follow "Sortir de veille"
    Then I should see "Êtes-vous sûr de vouloir sortir de veille le flux"
    Then I submit the "form" form
    Then the response status code should be 200
    Then I go to "flux/1/administrate"
    Then I should see "Mettre en veille"

  Scenario: En tant qu'administrateur, je peux bloquer un flux en production
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "flux/1/administrate"
    Then I should see "Bloquer le flux"
    And I follow "Bloquer"
    Then I should see "Êtes-vous sûr de vouloir bloquer le flux"
    Then I submit the "form" form
    Then the response status code should be 200
    Then I go to "flux/1/administrate"
    Then I should see "Débloquer le flux"

  Scenario: En tant qu'administrateur, je peux débloquer un flux
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "flux/1/administrate"
    Then I should see "Débloquer le flux"
    And I follow "Débloquer"
    Then I should see "Êtes-vous sûr de vouloir débloquer le flux"
    Then I submit the "form" form
    Then the response status code should be 200
    Then I go to "flux/1/administrate"
    Then I should see "Bloquer le flux"

  Scenario: En tant qu'administrateur, je peux supprimer un flux
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "flux/1/administrate"
    Then I should see "Supprimer définitivement le flux"
    And I follow "Supprimer"
    Then I should see "Êtes-vous sûr de vouloir supprimer le flux"
    Then I submit the "form" form
    Then the response status code should be 200
    Then I go to "flux/1/administrate"
    Then the response status code should be 404
