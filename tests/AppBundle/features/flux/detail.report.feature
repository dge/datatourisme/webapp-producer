Feature: Détail d'un flux : Rapport
  En tant qu'utilisateur, je peux consulter le rapport d'un flux

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process_strategy.yml           |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux afficher le rapport.
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Rapport"
    Then the response status code should be 200

  Scenario: En tant qu'utilisateur, je peux afficher la liste de fichiers.
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Rapport"
    Then I should see "Statistiques du flux"
