Feature: Historique des traitements liés à un flux
  En tant qu'utilisateur, je peux consulter l'historique des traitements liés à un flux.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

   # UTILISATEUR
  Scenario: En tant qu'administrateur, je peux afficher la liste.
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Historique"
    Then the response status code should be 200

  Scenario: Les traitement sont paginés.
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Historique"
    When I follow "2"
    Then the response status code should be 200

  Scenario: Je peux trier les résultats.
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Historique"
    When I follow "Date / Heure"
    Then the response status code should be 200
    When I follow "Mise à jour"
    Then the response status code should be 200
    When I follow "Statut"
    Then the response status code should be 200
    When I follow "Fichiers"
    Then the response status code should be 200
    When I follow "Durée"
    Then the response status code should be 200
    When I follow "% publication"
    Then the response status code should be 200
    When I follow "Évolution"
    Then the response status code should be 200
    When I follow "Anomalies"
    Then the response status code should be 200