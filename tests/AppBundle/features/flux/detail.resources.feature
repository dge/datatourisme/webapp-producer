Feature: Détail d'un flux : Ressources RDF
  En tant qu'utilisateur, je peux consulter la liste des ressources RDF liées au dernier traitement d'un flux.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je peux afficher la liste.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    Then the response status code should be 200

  Scenario: Je peux trier les résultats.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    When I follow "Titre"
    Then the response status code should be 200
    When I follow "Type"
    Then the response status code should be 200
    # TODO: To be reimplemented
#    When I follow "Ville"
#    Then the response status code should be 200
    When I follow "Statut"
    Then the response status code should be 200
    When I follow "Anomalies"
    Then the response status code should be 200

  Scenario: Je peux filtrer les résultats par titre.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    When I fill in "rdf_resource_filter_label" with "Corrupti"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200

  Scenario: Je peux filtrer les résultats par type.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    When I fill in "rdf_resource_filter_type" with "http://www.datatourisme.fr/ontology/core#EntertainmentAndEvent"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200

    # TODO
#  Scenario: Je peux filtrer les résultats par ville.
#    Given I am logged in as "user@dt.dev" with "user"
#    And I follow "En production"
#    Then I clean up the GET globals
#    And I follow "Dummy blocked Flux"
#    And I follow "POI"
#    When I fill in "rdf_resource_filter_city" with "Rennes"
#    And I submit the "form.form-horizontal" form
#    Then the response status code should be 200

  Scenario: Je peux filtrer les résultats par statut.
    Given I am logged in as "admin@dt.dev" with "admin"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    When I fill in "rdf_resource_filter_status" with "published"
    And I submit the "form.form-horizontal" form
    Then the response status code should be 200

  Scenario: En tant qu'administrateur, je ne peux pas afficher le bouton de création d'un nouveau traitement
    Given I am logged in as "admin@dt.dev" with "admin"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    Then I should not see "Lancer un traitement"

  # UTILISATEUR
  Scenario: Je peux afficher la liste des anomalies d'un POI.
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    Then the response status code should be 200
    Then I clean up the GET globals
    When I follow "1"
    Then the response status code should be 200

  Scenario: En tant qu'utilisateur, je peux afficher la liste.
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "POI"
    Then the response status code should be 200