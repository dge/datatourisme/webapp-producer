Feature: Administration d'un flux

  Scenario: En tant qu'admin, je peux gérer les étapes d'un flux
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process_strategy.yml           |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  Scenario: En tant qu'utilisateur, je ne peux pas accéder au flux d'une autre organisation.
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "flux/2"
    Then the response status code should be 403

  Scenario: En tant qu'administrateur, je peux refuser la mise en production
    Given I am logged in as "admin@dt.dev" with "admin"
    And I go to "flux/1/administrate"
    Then I should see "Valider la mise en production"
    Then I should see "Refuser la mise en production"
    And I follow "Refuser"
    Then I should see "Êtes-vous sûr de vouloir refuser la mise en production"
    Then I fill in "form[reasonInvalidate]" with "my reason"
    Then I submit the "form" form
    Then the response status code should be 200
    Then I go to "flux/1/ruleset/revision"
    Then I should see "Brouillon"
#     Mail Notifications
    Then I check the last received email
    Then the subject should contain "n'a pas été validé"
#     Notifications
  Scenario: En tant qu'utilisateur, je vois la notification envoyé par l'administrateur
    Given I am logged in as "user@dt.dev" with "user"
    Then I go to "notification"
    Then I should see "n'a pas été validé"

#  @javascript
#  Scenario:
#    And I am logged in as "user@dt.dev" with "user"
#    And I go to "/flux/1/ruleset/revision"
#    Then I should see "Supprimer"
#    Then I follow "Supprimer"
#    Then I wait for "Êtes-vous sûr de vouloir supprimer le jeu de règle" to appear while 15 seconds
#    Then I submit the "form" form
#    Then I wait for 3 seconds
#    And I should be on "/flux/1/ruleset/revision"
#    Then I should see "Créer un nouveau jeu de règles"

#  @javascript
#  Scenario: En tant qu'utilisateur, je peux créer une nouvelle règle d'alignement
#    Given I clean up the data directory
#    And I clean up the cache directory
#    And I am logged in as "user@dt.dev" with "user"
#    And I go to "/flux/1/ruleset/revision"
#    Then I should see "Créer un nouveau jeu de règles"
#    And I follow "Créer un nouveau jeu de règles"
#    Then I wait for "Pour amorcer la définition" to appear while 10 seconds
#    When I attach the file "datasource/datasource.xml" to "file"
#    Then I wait for 3 seconds
#    Then I press "Etape suivante"
#    Then I wait for 10 seconds
#    Then I follow "Cliquez pour ajouter un chemin"
#    Then I wait for 3 seconds
#    Then I follow "poi"
#    Then I wait for 3 seconds
#    Then I press "Enregistrer"
#    Then I wait for 6 seconds
#    Then I press "Etape suivante"
#    Then I wait for 3 seconds
#    Then I follow "Cliquez pour ajouter un chemin"
#    Then I wait for 3 seconds
#    Then I follow "nom"
#    Then I press "Enregistrer"
#    Then I wait for 6 seconds
#    Then I follow "Cliquez pour ajouter un chemin"
#    Then I wait for 3 seconds
#    Then I follow "nom"
#    Then I press "Enregistrer"
#    Then I wait for 6 seconds
#    Then I press "Etape suivante"
#    Then I wait for 3 seconds
#    Then I select "select.form-control":"string:http://www.datatourisme.fr/ontology/core#PointOfInterest"
#    Then I wait for 3 seconds
#    Then I press "Commencer l'alignement"
#    Then I wait for 20 seconds
#    When I go to "/flux/1/ruleset/revision"
#    Then I should see "Brouillon"
#    Then I should see "Modifier"
#    Then I should see "Supprimer"
#    Then I follow "Modifier"
#    Then I wait for 10 seconds
#    Then I follow "a été créé par"
#    Then I wait for 2 seconds
#    Then I click the "label.radio-inline:first-child" element
#    Then I wait for 1 seconds
#    Then I follow "Nom court"
#    Then I wait for 2 seconds
#    Then I click the "label.radio-inline:last-child" element
#    Then I wait for 1 seconds
#    Then I follow "Cliquez pour ajouter un chemin"
#    Then I wait for 2 seconds
#    Then I follow "nom"
#    Then I press "Enregistrer"
#    Then I wait for 3 seconds
#    Then I follow "Revenir au flux"
#    Then I wait for 4 seconds
#    Then I should see "Demander l'activation"
#
#  @javascript
#  Scenario:
#    And I am logged in as "user@dt.dev" with "user"
#    And I go to "/flux/1/ruleset/revision"
#    Then the response status code should be 200
#    Then I should see "Demander l'activation"
#    Then I follow "Demander l'activation"
#    Then I submit the "form" form
#    Then the response status code should be 200
#    And I should be on "/flux/1/ruleset/revision"
#    Then I should see "Attente de validation"
#
#  @javascript
#  Scenario: En tant qu'administrateur, je peux refuser la mise en production
#    Given I am logged in as "admin@dt.dev" with "admin"
#    And I go to "flux/1/administrate"
#    Then I should see "Valider la mise en production"
#    Then I should see "Refuser la mise en production"
#    And I follow "Refuser"
#    Then I should see "Êtes-vous sûr de vouloir refuser la mise en production"
#    Then I fill in "form[reasonInvalidate]" with "my reason"
#    Then I submit the "form" form
#    Then the response status code should be 200
#    Then I go to "flux/1/ruleset/revision"
#    Then I should see "Brouillon"