Feature: Détail d'un flux : Anomalies
  En tant qu'utilisateur, je peux consulter la liste des anomalies RDF liées au dernier traitement d'un flux.

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | process_strategy.yml           |
      | organization_type.yml          |
      | organization.yml               |
      | user.yml                       |
      | flux_crawler.yml               |
      | alignment_ruleset.yml          |
      | alignment_ruleset_revision.yml |
      | flux.yml                       |
      | process.yml                    |
      | file.yml                       |
      | rdf_resource.yml               |
      | anomaly.yml                    |

  # ADMINISTRATEUR
  Scenario: En tant qu'administrateur, je ne peux pas afficher le bouton de création d'un nouveau traitement
    Given I am logged in as "admin@dt.dev" with "admin"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Anomalies"
    Then I should not see "Lancer un traitement"

  # UTILISATEUR
  Scenario: En tant qu'utilisateur, je peux afficher la liste des anomalies d'un flux
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Anomalies"
    Then the response status code should be 200
    Then I should see "Dummy breaker Anomaly"
    And I should see "Dummy non breaker Anomaly"
    And I should see "Télécharger le rapport d'anomalie"

  Scenario: En tant qu'utilisateur, je peux télécharger le rapport d'anomalie
    Given I am logged in as "user@dt.dev" with "user"
    And I follow "En production"
    Then I clean up the GET globals
    And I follow "Dummy blocked Flux"
    And I follow "Anomalies"
    When I follow "Télécharger le rapport d'anomalie"
    Then the response status code should be 200
    And the response content type should be "text/csv"

  Scenario: En tant qu'utilisateur, je peux afficher les ressources concernées par une anomalie
    Given I am logged in as "user@dt.dev" with "user"
    And I go to "/flux/production"
    And I follow "Dummy blocked Flux"
    Then I clean up the GET globals
    And I follow "Anomalies"
    When I follow "1"
    Then the response status code should be 200
    And I should see "POI concernés"