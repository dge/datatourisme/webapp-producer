Feature: DAT-2 Authentification d'un utilisateur
  L'utilisateur est en mesure de se connecter

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | organization_type.yml |
      | organization.yml |
      | user.yml |

  Scenario: Je suis redirigé vers le formulaire de login
    When I go to "/"
    Then I should be on "/login"

  Scenario: Si les identifiants sont incorrects j'ai une erreur explicite
    When I am logged in as "no-account@dt.dev" with "no"
    Then I should see "Identifiants invalides."
    When I am logged in as "block@dt.dev" with "bad_password"
    Then I should see "Identifiants invalides."
    When I am logged in as "block@dt.dev" with "bad_password"
    Then I should see "Identifiants invalides."
    When I am logged in as "block@dt.dev" with "bad_password"
    Then I should see "Identifiants invalides. Votre compte va être bloqué pendant une heure après cinq tentatives de connexion infructueuses. Il vous reste 2 tentatives."
    When I am logged in as "block@dt.dev" with "bad_password"
    Then I should see "Identifiants invalides. Votre compte va être bloqué pendant une heure après cinq tentatives de connexion infructueuses. Il vous reste une tentative."
    When I am logged in as "block@dt.dev" with "bad_password"
    Then I should see "Votre compte est bloqué pendant une heure."

  Scenario: Je peux me connecter et me déconnecter si j'ai un compte
    When I am logged in as "admin@dt.dev" with "admin"
    Then I follow "John DOE"
    When I follow "Déconnexion"
    Then I should be on "/login"

  Scenario: Je peux me connecter avec une adresse mail insensible à la casse
    When I am logged in as "upper@dt.dev" with "upper"
    Then I should be on the homepage