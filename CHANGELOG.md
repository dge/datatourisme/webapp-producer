CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [En cours]

## [1.5.0] - 2022.04.05

### Ajout
- Paramètre utilisateur : autorisation d'utiliser le SSO plateforme qualité
- Connexion SSO vers la plateforme qualité

### Correction
- Correction de la dépendance `hautelook/alice-bundle` (mise à jour composer.lock)
- Dashboard : calcul incorrect du nombre total des POI, utilisation d'une requête PostgreSQL plus performante
- FluxStatsEntryRepository : suppression des méthodes devenues inutiles

## [1.4.8] - 2021.06.05

### Ajout
- Nouvelle entrée de menu "Exploitation"
- Ajout de la page de téléchargement des traductions

## [1.4.7] - 2020.10.07

### Modifications
- Mise à jour de la dépendance alignment-editor

## [1.4.6] - 2020.04.04

### Corrections
- Persistence des réutilisations sur le disque : les mois complets ne sont plus re-traités

## [1.4.5] - 2020.01.28

### Modifications
- Ajout des liens de téléchargement des archives sur la page des réutilisations

## [1.4.4] - 2019.12.18

### Corrections
- substring préventif de certaines propriété de RdfResource et Anomaly pour éviter le plantage dû aux valeurs hors gabarit

## [1.4.3] - 2019.12.16

### Corrections
- Corrections d'affichage pour les actualités

## [1.4.2] - 2019.12.13

### Modifications
- Optimisation des calculs de réutilisation

## [1.4.1] - 2019.12.13

### Modifications
- Réutilisation : retour à 1 mois glissant

## [1.4.0] - 2019.12.12

### Ajout
- Onglet Mon profil > paramètres pour gérer les envois de notifications mail
- Plus de colonnes dans les anomalies
- Nettoyage de la base de donnée
- Persistence des réutilisations sur le disque
- Déploiement de l'interface de thesaurus en un clic

## [1.3.3] - 2019.10.01

### Modifications
- Mise à jour du submodule (datatourisme.fr vers https://info.datatourisme.gouv.fr)

## [1.3.2] - 2019.07.03

### Correction
- hotfix : une erreur twig provoquait une erreur 500 sur l'accueil du support

## [1.3.1] - 2019.01.14

### Modifications
- Beanstalk peut maintenant initier des processes
- Mise à jour des dépendances

## [1.3.0] - 2018.08.20

### Modifications
- Mise à jour de l'éditeur d'alignement

## [1.2.5] - 2019.01.14

### Modifications
- Mise à jour des IRI des ontologies DATAtourisme

### Ajout
- Divers commandes de maintenance

## [1.2.4] - 2018.07.12

### Ajout
- Support des flux linguistiques (i18nUrls)

## [1.2.3] - 2018.06.20

### Modifications
- Mise à jour de la dépendance webapp-bundle
- FaqCreateType : ajout de la réponse dans le corp de la notification

### Correction
- Notification : affichage des notifications rattachées à aucune organisation pour l'ensemble des utilisateurs

## [1.2.2] - 2018.06.07

### Correction
- hotfix : l'identifiant RdfResource VARCHAR(63) a VARCHAR(255)

## [1.2.1] - 2018.06.04

### Modifications
- Mise à jour des dépendances

## [1.2.0] - 2018.05.16

### Ajout
- FAQ : Ajout d'une notification aux producteurs dès qu'une question est
ajoutée
- Export CSV des utilisateurs et des structures associées

### Modifications
- Ajout du pays dans les stats de réutilisation

## [1.1.9] - 2018.03.30

### Modifications
- Les stats de réutilisation sont limitées aux resources publiées
- Suppression de toutes les balises HTML dans les notifications

## [1.1.8] - 2018.03.22

### Modifications
- Limitation du calcul des stats de réutilisation à 30 jours glissants

## [1.1.7] - 2018.03.01

### Modifications
- Symfony 3.4
- webapp-bundle 1.0.0

## [1.1.6] - 2018.02.28

### Modifications
- Les exports CSV se font maintenant en ISO-8859-1
- Statistiques : intégration des téléchargements manuels
- Statistiques : ajout de la structure et du mode de téléchargement dans le fichier CSV

### Corrections
- Mise à jour de la dépendance alignment-editor pour fixer l'import de PointOfInterest

## [1.1.5] - 2018.02.16

### Corrections
- Pagination des traitements erronée pour les utilisateurs non-admins
- Suppression des règles vides dans les règles d'alignement
- Thesaurus : multiples parents

## [1.1.4] - 2018.02.14

### Modifications
- Ajout d'informations dans le CSV du rapport de réutilisation :
    * producteur
    * lien de l'application
    * type de diffuseur
    * code postal du diffuseur
    * date du dernier téléchargement

## [1.1.3] - 2018.02.12

### Modifications
- Mise à jour de la dépendance alignment-editor
- Ajout d'une commande de migration des alignements vers le nouveau format

## [1.1.2] - 2018.02.08

### Modifications
- Stats réutilisation : le traitement et les rapport prennent en considération le type secondaire "type2"

## [1.1.1] - 2018.01.31

### Ajouts
- Interface de consultation des statistiques de réutilisation
- Export des statistiques de réutilisation au format CSV

## [1.1.0] - 2018.01.31

### Corrections
- Le traitement du rapport de la tâche producerdf prend maintenant en considération la data "creator"

## [1.0.0] - 2018.01.16

### Ajouts
- Les supers administrateurs reçoivent un mail lorsqu'un flux est en attente de validation
- Texte d'avertissement à la création d'une nouvelle discussion dans le support
- Général : Le logo Datatourisme a été ajouté comme Favicon
- FAQ : Support du format Markdown dans les réponses
- Centre de support : Support du format Markdown dans les posts 
- Centre de support : Support des formats XML et ZIP pour les pièces jointes
- Centre de support : Affichage au survol de la liste des formats de pièces jointes autorisés
- Assistant de création de flux : la liste des types est maintenant gérée par Selectize

### Modifications
- Modification du design des titres sur les paramètres du détail d'un flux 
- Autorisation des fichiers text/plain et text/rtf dans les pièces jointes du support
- Taille limite de pièce jointe augmentée à 5Mo
- Modification du texte de changement de mot de passe utilisateur
- Le graphique d'évolution du tableau de bord affiche désormais les traitements des 10 derniers jours
- Les graphiques de la liste des flux en production affichent désormais les traitements des 10 derniers jours
- La plupart des options des champs select sont désormais triées par ordre alphabétique
- Le favicon Symfony a été remplacé par le logo Datatourisme
- Un message d'erreur plus explicite s'affiche désormais lorsque le serveur de traitement ne répond pas
- Le thésaurus filtre désormais les doublons, les espaces et les chaînes de ponctuation au sein des champs
- Browserify
- Lors de la phase de brouillon, un nouveau fichier XML peut être soumis
- Les flux sont traité directement à leur validation
- Un producteur peut annuler une demande de validation

### Corrections
- Optimisations significatives des performances
- Le nombre de Flux sur la page d'administration d'une Structure était fixé à 15 au lieu d'être dynamique
- Le nombre d'anomalies sur l'onglet historique donnait le nombre de poi en anomalie, pas le nombre d'anomalies
- Le message d'erreur lors de l'ajout d'une pièce jointe vide était trompeur
- Correction des problèmes de performances liés au nombre élevé d'anomalies en production
- Le menu parent "Flux" n'était pas correctement mis en surbrillance lorsque l'on était
sur l'historique des traitements
- Un mystérieux `&nbsp;` se glissait dans la FAQ lorsqu'elle était vide
- La création d'une nouvelle révision entraînait la perte de certaines données d'alignement
- La durée des traitements n'était pas correcte
- Une erreur dans la requête de pagination des anomalies faussait le nombre de pages
- La taille totale des fichiers d'un traitement était incorrecte dans certains cas
- Les formulaires ne géraient pas les contraintes de taille max pour les varchars
- L'import manuel était bloqué sur certaines configurations de PHP
- Certains flux avec une révision en brouillon ne s'afichaient pas dans la liste des flux en conception
- Le bouton de soumission d'une révision s'affichait où il ne devait pas
- Faille XSS dans les noms des thèmes de la FAQ
- Faille XSS dans les notifications
- La liste des notifications affichait l'historique des 15 dernières secondes au lieu de 15 derniers jours.
- Un fichier XML sans prologue est maintenant accepté pour un traitement manuel
- L'assistant de création de flux accepte maintenant les fichiers ZIP
- La configuration du mode pull nécessite maintenant un protocole sécurisé (https, sftp)
- La réponse de la FAQ peut être > 2047 car.

## [0.1.1] - 2017-07-04
### Ajouts
- Notification : un nouvel utilisateur a rejoint votre structure
- Liste des notifications de l'utilisateur des 15 derniers jours
- Liste des flux en production
- Liste des flux en conception / attente de validation
- Historique des traitements
- Thésaurus
- Notifications
- Détails d'un flux :
    - Rapport d'état
    - Paramètres
    - Liste des règles d'alignement
    - Liste des ressources RDF
    - Liste des anomalies éventuelles
    - Historique des traitements
- Procédé de mise en production d'un flux :
    - Création du flux
    - Choix du mode de mise à jour
    - Création du jeu de règles d'alignement
- Tableau de bord :
    - Indicateurs clés de performance :
        - Nombre de flux en production
        - Nombre de POI publiés
        - Taux de publication des POI
        - Nombre de POI réutilisés
    - 5 derniers traitements exécutés
    - Statistiques des POI publiés
    - Liste simplifiée des flux en production
- Interface d'alignement
- Interface de prévisualisation des POI
- Lancement et programmation de traitement

### Modifications
- Menu : plus de valeur par défaut lors du chargement du nombre de nouveaux messages
- Amélioration de la pertinence du fil d'Ariane
- Nouvelle interface de thésaurus

### Corrections
- Ajout d'un template de mail spécifique pour la création d'un compte
- Correction du breadcrumb des pages du centre de support
- MIOGA#6107 Liste des utilisateurs : la recherche par nom est maintenant case-insensitive
- Le composant FrameLoader ne se met pas à jour en cas d'erreur 401q
- Correction CSS du profil utilisateur
- UserRepository -> webapp bundle
- JS frameloader -> webapp bundle
- Le système de mailing utilise maintenant webapp.mailer
- Correction du layout de la page des CGU
- Nouveau message de l'utilisateur considéré à tort comme non lu pour lui-même
- Mioga #6645 : fichier XML invalide
- Mioga #6632 : Noeud proposé pour l'alignement
- Mioga #6606 : Mot de passe invalide
- Mioga #6512 : Non enregistrement du thésaurus
- Mioga #6381 : Récupération du mot de passe
- Mioga #6395 : Amélioration de la liste des utilisateurs
- Mioga #6514 : impossible de passer à l'étape 4
- Mioga #6607 : Compte et adresse conjecto
- Mioga #6397 : Export CSV : champs éronnés
- Mioga #6400 : Bloc profil : date en anglais
- Dépendance require-dir 0.3.1 => 0.3.2 pour la compatibilité avec Node 8.x

## [0.1.0] - 2017-04-18
### Ajouts
- Rôles / droits
    - Implémentation du rôle "Super administrateur"
    - Implémentation du rôle "Administrateur"
    - Implémentation du rôle "Producteur"
- Authentification
    - Procédure d'authentification / blocage automatique
    - Procédure de récupération de mot de passe
    - Procédure d'acceptation des conditions d'utilisation
    - Procédure de déconnexion
- Gestion du profil
    - Modification du profil utilisateur
    - Modification du profil structure
- Administration des utilisateurs
    - Liste des utilisateurs
    - Modification des utilisateurs
    - Blocage d'un utilisateur
    - Substitution d'authentification
    - Création d'un nouvel utilisateur
    - Téléchargement d'un rapport de connexions
- Administration des structures
    - Liste des structures
    - Modification d'une structure
    - Création d'une nouvelle utilisateur
- Foire aux questions
    - Liste des questions/réponses
    - Recherche fulltexte des questions/réponses
    - Administration des questions/réponses (ajout, suppression, ré-organisation)
- Centre de support
    - Liste des discussions/messages
    - Création d'une nouvelle discussion
    - Participation à une discussion
    - Notifications par mail des nouveaux messages
    - Notifications des nouveaux messages dans la barre de navigation
    - Notifications des nouveaux messages dans le tableau de bord
- Canevas du tableau de bord
- Tests Behat/Mink pour l'ensemble des fonctionnalités