### 1. Préambule

1\. La Direction générale des entreprises édite et développe www.datatourisme.gouv.fr, plateforme nationale de collecte, d’uniformisation et de diffusion en open data des données touristiques.

2\. La plateforme http://www.datatourisme.gouv.fr a pour objectif d’agréger et d’uniformiser les données émanant des systèmes d’information touristiques territoriaux (« SIT ») des acteurs institutionnels de tourisme, à savoir les offices de tourisme, les agences et comités départementaux du tourisme, ou encore les comités régionaux de tourisme.

### 2. Définitions

3\. Les termes ci-dessous définis auront entre les parties la signification suivante :

* « administrateur » : désigne les personnes habilitées par la DGE pour veiller au bon fonctionnement de la plateforme ;
* « conditions générales » : désigne les présentes conditions générales d’utilisation de la plateforme ;
* « DGE » : désigne la Direction générale des entreprises ;
* « diffuseur » : désigne le ré-utilisateur de jeux de données publiés sur la plateforme ;
* « jeu de données » : désigne les données ou objets touristiques de type « Points d’intérêt touristique » (« POI ») agrégés, stockés et publiés sur la plateforme ;
* « plateforme » : désigne la plateforme DATAtourisme mise à disposition par la DGE et accessible, uniquement aux producteurs, professionnel de tourisme, à l’adresse https://producteurs.datatourisme.gouv.fr ;
* « producteur » : désigne toute personne physique ou morale autorisée par la DGE à uploader les jeux de données sur la plateforme. Il s’agit notamment des offices de tourisme, les agences et comités départementaux de tourisme, ou encore les comités régionaux de tourisme.

### 3. Prérequis

4\. L’accès à la plateforme n’est autorisé qu’aux professionnels du tourisme et, est conditionné au respect des prérequis suivants :

* disposer d’un équipement informatique adapté ;
* disposer d’une adresse de courrier électronique valide ;
* disposer des navigateurs web compatible ;
* autoriser l’utilisation des cookies de session et des scripts javascripts

5\. Le producteur, en acceptant les présentes conditions générales, déclare :

* avoir pris connaissance des conditions dans lesquelles fonctionne la plateforme ;
* disposer de toutes les informations nécessaires pour considérer que la plateforme correspond à ses attentes, ses objectifs et ses besoins ;
* disposer de toutes les compétences techniques nécessaires pour accéder et utiliser la plateforme.

### 4. Objet

6\. Les présentes conditions générales ont pour objet de définir les conditions et modalités régissant l’utilisation de la plateforme par le producteur.

### 5. Entrée en vigueur – Durée

7\. Les présentes conditions générales sont applicables pendant toute la durée de l’utilisation de la plateforme par le producteur, c’est-à-dire à compter de l’activation du compte par la DGE jusqu’à sa désactivation.

### 6. Acceptation et opposabilité

8\. Les présentes conditions générales sont opposables au producteur dès leur acceptation, formalisée par un clic sur le bouton « Accepter », lors de la première connexion du producteur sur la plateforme, ce qui entraîne l’archivage automatique des conditions générales sur le compte personnel du producteur.

9\. Dans tous les cas, à la date de la première utilisation de la plateforme par le producteur, les conditions générales sont réputées lues et acceptées sans réserve par ce dernier.

10\. Les conditions générales sont susceptibles d’être modifiées ou aménagées à tout moment par la DGE afin de faire évoluer la plateforme et ses fonctionnalités. Dans ce cas, une notification sera envoyée aux utilisateurs par email.

11\. En cas de modification, les nouvelles conditions générales sont notifiées au moment de la nouvelle connexion, par le producteur, à la plateforme.

12\. Toute nouvelle utilisation de la plateforme, postérieure à la notification de la modification des conditions générales, vaut acceptation par le producteur des nouvelles conditions générales.

13\. Les conditions générales figurant sur la plateforme prévalent sur toute version imprimée de date antérieure.

### 7. Disponibilité

14\. La plateforme est accessible 24/24 heures, 7/7 jours sous réserve des opérations de maintenance ou de difficultés ou contraintes techniques.

15\. La DGE se réserve le droit, sans préavis, ni indemnité, de fermer temporairement ou définitivement l’accès à la plateforme en cas de risque pour la sécurité de cette dernière ou de menace pour l’intégrité des jeux de données.

### 8. Utilisation de la plateforme

16\. L’utilisation de la plateforme par le producteur est strictement personnelle.

17\. Le compte personnel du producteur est intuitu personae c’est à dire qu’il est à la discrétion du seul producteur et ne peut en aucun cas être partagé avec d’autres utilisateurs.

18\. Sauf à avoir engagé préalablement une demande de suspension ou de suppression de compte ou une procédure d’opposition auprès de la DGE, tout usage de la plateforme est réputé avoir été réalisé par le producteur.

19\. Le compte personnel est utilisé sous la seule et unique responsabilité du producteur.

20\. Le producteur s’engage à n’utiliser la plateforme que dans les seules conditions définies aux présentes et en outre :

* à ne pas détourner l’utilisation de la plateforme à des fins personnelles publicitaires ou de vente de produits ;
* à ne commettre aucun acte de contrefaçon, à ne pas reproduire, télécharger, représenter ou modifier, tout ou partie de la plateforme ;
* à ne pas accéder et/ou se maintenir dans la plateforme entendue comme un système de traitement automatisé de données. Tout accès ou maintien frauduleux à ce dernier est interdit et sanctionné pénalement. Il en est de même pour toute entrave ou altération du fonctionnement de ce système, ou en cas d’introduction, de suppression ou de modification des données qui y sont contenues ;
* à ne pas perturber le bon fonctionnement de la plateforme, à ne pas altérer, transformer ou modifier les données accessibles et publiées sur la plateforme et notamment à ne pas introduire de virus ou toute autre technologie nuisible à la plateforme, aux données ou aux services qui y sont proposés ;
* à ne pas nuire à l’image de l’Administration.

21\. Le producteur est entièrement responsable de l’utilisation qu’il fait de la plateforme. Il s’engage à l’utiliser de façon loyale, dans le respect des présentes conditions générales, des lois et règlements applicables, notamment ceux relatifs à la propriété intellectuelle.

22\. Le producteur ne doit, en aucune manière, se livrer au chargement, au stockage, à la publication ou à la diffusion de fichiers et de messages dont le contenu présente un caractère injurieux, raciste, pornographique ou diffamatoire, sans que cette liste ne soit exhaustive. En cas de violation de ce point,  les comportements litigieux seront signalés au tribunal compétent.

### 9. Condition d’accès à la plateforme

23\. L’accès à la plateforme n’est pas libre pour le producteur.

24\. Seul l’administrateur est habilité à créer un compte personnel au nom du producteur.

25\. La création de compte est faite par et sur appréciation de la DGE, à la demande expresse du producteur transmise :

* par formulaire de contact auprès de la DGE, disponible à l’adresse https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme
* ou en utilisant le système de messagerie interne à la plateforme.

26\. Dès que le compte est créé par l’administrateur, la DGE envoie un email confirmant la création du compte et invite le producteur à modifier son mot de passe dans une interface dédiée.

27\. Une fois le mot de passe modifié, le producteur est automatiquement redirigé vers le formulaire d’authentification permettant d’accéder à la plateforme.

28\. Le producteur a la possibilité de modifier son mot de passe à tout moment au sein de son espace personnel par l’intermédiaire de l’onglet « Administration », puis, « Modifier mon mot de passe ».

29\. Le producteur est entièrement responsable de la conservation et de l’utilisation de son mot de passe. Il doit prendre toutes les mesures nécessaires pour empêcher une utilisation non autorisée ou frauduleuse de son compte.

30\. A cet effet, il lui est recommandé :

* de modifier régulièrement son mot de passe ;
* de ne pas utiliser un mot de passe déjà utilisé pour un autre service.

31\. Le producteur doit se déconnecter de sa session et fermer la fenêtre de son navigateur à l’issue de ses démarches sur son compte pour éviter que d‘autres personnes n’accèdent à son espace personnel. Le producteur doit être particulièrement prudent lorsqu’il accède à son compte en wi-fi ou partage de connexion.

32\. Si le producteur constate ou suspecte une utilisation non autorisée ou frauduleuse de son mot de passe ou toute brèche dans la sécurité, il doit alerter immédiatement la DGE :

* par le formulaire de contact, disponible à l’adresse https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme
* ou en utilisant le système de messagerie interne à la plateforme.

32\. A compter de la réception de cette notification, la DGE procédera dans un délai raisonnable à la désactivation du compte du producteur.

33\. Le mot de passe est strictement personnel et :

* doit correspondre aux besoins de sécurisation fixés par la DGE ;
* ne doit, sous aucun prétexte, être communiqué par le producteur à un tiers.

34\. **IL EST EXPRESSEMENT RAPPELE QUE LA DGE NE DEMANDE JAMAIS ET POUR QUELQUE RAISON QUE CE SOIT AU PRODUCTEUR DE LUI COMMUNIQUER SON MOT DE PASSE ET QUE TOUTE DEMANDE EN CE SENS DEVRA ETRE CONSIDEREE COMME UNE DEMANDE FRAUDULEUSE.**

35\. Le compte personnel se verrouille après 5 tentatives de connexion infructueuses pendant un délai d’une heure. Le déverrouillage est également possible par l’intermédiaire des administrateurs.

36\. La DGE se réserve le droit de modifier les conditions techniques liées à l’authentification et de substituer au mot de passe toute autre technologie qu’elle estimera nécessaire.

### 10. Notifications

37\. Le producteur peut être amené à recevoir, lors de l’utilisation de la plateforme, des messages, sous forme de notifications.

38\. Ces notifications ont vocation à informer le producteur sur l’ensemble des événements importants concernant son activité et les flux de sa structure.

### 11. Sécurité

39\. La DGE fait ses meilleurs efforts, conformément aux règles de l’art, pour sécuriser la plateforme et respecte les référentiels de sécurité RGS 2.0, OWASP Top 10 – 2013 ainsi que les bonnes pratiques formulées par l’ANSSI. De plus, tous les échanges entre le navigateur du producteur et la plateforme font l’objet d’un chiffrement à l’aide d’un certificat TLS, émanant d’une autorité de certification désignée par la DGE.

40\. Le producteur reconnaît avoir connaissance de la nature du réseau de l’internet, et en particulier, de ses performances techniques et des temps de réponse pour consulter, interroger ou transférer les données d’informations.

41\. Le producteur se doit d’informer la DGE de toute défaillance ou dysfonctionnement de l’espace personnel du producteur.

42\. Si une faille dans la sécurité est détectée, la DGE informera le producteur dans le respect des dispositions légales qui s’imposent à elle. Elle lui indiquera éventuellement des mesures à prendre. L’exécution de ces mesures est à la charge du producteur.

43\. La DGE peut prendre toutes les mesures d’urgence nécessaires à la sécurité de la plateforme.

44\. Le producteur accepte de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par des éventuels virus ou toute autre technologie nuisible sur le réseau de l’internet.

45\. En tout état de cause, le producteur déclare accepter les caractéristiques et limites de l’internet.

### 12. Responsabilité de la DGE

46\. La responsabilité de la DGE ne pourra être recherchée ni retenue en cas d’indisponibilité temporaire ou totale de tout ou partie de l’accès à la plateforme, d’une difficulté liée au temps de réponse, et d’une manière générale, d’un défaut de performance quelconque.

47\. La responsabilité de la DGE ne saurait être recherchée en cas d’usage frauduleux ou abusif du compte dû à une divulgation volontaire ou accidentelle à quiconque de son mot de passe.

48\. La DGE ne saurait être responsable de la violation des présentes conditions par un autre producteur.

49\. Le producteur reconnaît et accepte que la DGE ne peut pas être tenue pour responsable de la perte, ou altération des jeux de données résultant de l’utilisation non-conforme de la plateforme ou d’une mauvaise gestion de la sécurité par le producteur.

50\. Il incombe donc au producteur de sauvegarder, sur un autre support, les jeux de données publiés sur la plateforme.

51\. Seul le producteur est responsable de la véracité et de la qualité des jeux de données publiés sur la plateforme vis-à-vis des diffuseurs, la DGE ne faisant que les héberger.

### 13. Signalement des contenus

52\. La DGE s’engage dans la lutte contre les contenus illicites, erronés et incompatibles techniquement. A ce titre, la DGE permet au producteur de lui notifier toute catégorie de contenus contrevenant aux présentes conditions générales, aux lois et règlements, publiés sur la plateforme.

53\. En plus d’un contrôle a priori réalisé par les administrateurs, la DGE a mis en place un système de contrôle a posteriori de la mise en ligne de jeu de données par les producteurs  via une fonctionnalité de signalement, permettant d’assurer, dans la mesure du possible, que les jeux de données publiés sur la plateforme respectent les conditions générales de la plateforme.

54\. Cette notification peut être réalisée par n’importe quel producteur ou diffuseur qui identifierait un contenu inapproprié ou illicite, erroné et incompatible techniquement. La notification est engagée sous la seule et unique responsabilité du producteur ou du diffuseur.

55\. Tout jeu de données ayant fait l’objet d’un signalement préalable et qui ne respecterait pas les conditions générales est supprimé par la DGE dans un délai raisonnable à compter du signalement.

### 14. Propriété intellectuelle

56\. Les présentes conditions générales n’emportent aucune cession d’aucune sorte de droits de propriété intellectuelle sur les éléments appartenant à la DGE au bénéfice du producteur.

57\. Le site, les marques, les dessins, les modèles, les images, les textes, les photos, les logos, les chartes graphiques, les logiciels, les moteurs de recherche, les structures des bases de données et les noms de domaine, sans que cette liste soit exhaustive, sont la propriété exclusive de la DGE ou de ses partenaires qui lui ont concédé une licence.

58\. Toute reproduction et/ou représentation, totale ou partielle d’un de ces droits, sans l’autorisation expresse de la DGE, est interdite et constituerait une contrefaçon sanctionnée par les articles L. 335-2 et suivants du Code de la propriété intellectuelle.

59\. En conséquence, le producteur s'interdit tout agissement et tout acte susceptible de porter atteinte directement ou non aux droits de propriété intellectuelle de la DGE.

60\. La présente clause de propriété intellectuelle à vocation à perdurer après la terminaison des conditions générales.

### 15. Données publiques

#### 15.1. Licence ouverte / open licence Etalab

61\. Le producteur est informé que les jeux de données publiés sur la plateforme sont régis par la licence ouverte / open licence Etalab à jour au moment de leur publication.

62\. A ce titre, le producteur concède au diffuseur un droit non exclusif et gratuit de libre extraction et réutilisation des jeux de données publiés sur la plateforme, à des fins commerciales ou non, dans le monde entier et pour une durée illimitée.

63\. Le diffuseur est donc libre de réutiliser les jeux de données afin de :

* les reproduire, les copier ;
* les adapter, les modifier, les extraire et les transformer, pour créer des flux, des produits ou des services ;
* les communiquer, les diffuser, les redistribuer, les publier et les transmettre ;
* les exploiter à titre commercial, par exemple en les combinant avec d’autres jeux de données, ou en les incluant dans son propre produit ou application.

64\. Néanmoins, le diffuseur doit toujours mentionner la paternité du jeu de données utilisé dans le cadre de sa réutilisation et la date de dernière mise à jour du jeu de données réutilisé.

65\. Pour plus de détails sur le régime des jeux de données et sur les droits découlant de ce régime, il convient de se référer à la licence ouverte / open licence Etalab, accessible à l’adresse suivante : https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf.

#### 15.2. Garantie de jouissance paisible

66\. Le producteur déclare :

* qu’il dispose des droits nécessaires à la diffusion des jeux de données et que rien ne s’oppose à la réutilisation y compris à des fins commerciales des jeux de données par des tiers ;
* que les jeux de données ne sont pas protégés par un droit privatif de tiers (droit de propriété intellectuelle, secret, confidentialité, vie privée) ;
* que, si des éléments des jeux de données sont des œuvres protégées par le droit d’auteur, il détient l’ensemble des droits patrimoniaux nécessaires à la publication de ces données sur la plateforme ; que les jeux de données contiennent des œuvres dérivées, il a respecté, respecte et respectera les droits de propriété intellectuelle de l'auteur de l’œuvre initiale, conformément au Code de la propriété intellectuelle ;
* que les éléments des jeux de données ne constituent pas une contrefaçon d'une œuvre préexistante ;
* qu'il a respecté les droits de propriété intellectuelle des tiers, notamment sur les dessins et modèles, sur les brevets et sur les marques des tiers ;
* que les jeux de données ne sont pas couvertes par un secret ou par une obligation de confidentialité ;
* que les jeux de données ne contiennent pas d’élément relatif à la vie privée.

67\. A ce titre, le producteur garantit la DGE contre toute action, réclamation, revendication ou opposition de la part de toute personne invoquant un droit privatif, un droit de propriété intellectuelle ou un acte de concurrence déloyale et/ou parasitaire auquel l'exécution du contrat aurait porté atteinte.

68\. En conséquence, le producteur prendra à sa charge tous dommages et intérêts auxquels serait condamnée la DGE à raison notamment d'un acte de contrefaçon, d’atteinte à la vie privée ou encore de concurrence déloyale ou de parasitisme résultant de l’exécution du contrat, et ce, dès que la condamnation les prononçant devient exécutoire, ainsi que les frais de toute nature supportés par la DGE pour assurer sa défense, y compris les frais d'avocat.

69\. Le producteur indemnisera de même la DGE de toutes les conséquences dommageables subies par elle du fait des actions engagées à son encontre et des troubles dans sa jouissance paisible.

#### 15.3. Qualité des jeux de données

70\. Le producteur fait ses meilleurs efforts pour fournir des jeux de données « fiables et à jour », collectées au plus près du terrain.

71\. Le producteur fait ses meilleurs efforts pour mettre à jour, au minimum une fois par an, les jeux de données publiés sur la plateforme ainsi qu’à améliorer de façon permanente la qualité et la véracité des jeux de données, notamment lorsqu’une erreur lui serait signalée.

72\. Le producteur fait ses meilleurs efforts pour fournir des jeux de données suffisamment qualifiés pour permettre une réutilisation efficace et satisfaisante par les diffuseurs.

A ce titre, les jeux de données doivent, au minimum, respecter les exigences prévues dans la documentation disponible dans la rubrique « aide » de la plateforme.

### 16. Données à caractère personnel

#### 16.1. A la charge de la DGE

73\. L’accès à la plateforme est conditionné à l’ouverture d’un compte personnel au nom du producteur et nécessite l’utilisation par la DGE de données à caractère personnel.

74\. Certaines données collectées sont des données à caractère personnel permettant de créer le compte personnel du producteur, tels que, notamment, le nom, le prénom, la fonction, l’adresse de courrier électronique et le numéro de téléphone. Le producteur peut à tout moment mettre à jour ses données à caractère personnel ou modifier son profil grâce à l’interface « Mon profil ».

75\. Les données sont collectées sur la base du consentement du producteur, recueilli lors de l’inscription à la plateforme.

76\. Ces données sont destinées à la DGE, responsable de traitement.

77\. Dans le cadre du bon fonctionnement de la plateforme, la DGE est amenée à recueillir et stocker des données à caractère personnel pour les finalités suivantes :

* la gestion du compte du producteur ;
* la gestion des demandes de support technique ;
* la gestion des demandes de droit d’accès, de rectification et d’opposition ;
* la publication de statistiques sur la plateforme et, notamment, sur l’interface utilisable par les producteurs (catégories de données téléchargées par le diffuseur, fréquences des téléchargements etc.).

78\. Les données à caractère personnel sont accessibles aux agents habilités de la DGE dans la limite de leurs fonctions ainsi qu’à des sous-traitants éventuels.

79\. La DGE enregistre, traite les données collectées, à seule fin de permettre au producteur le plein et bon usage de la plateforme.

80\. Dans le cadre du partage des données, la DGE peut faire appel à des sous-traitants pour le fonctionnement des services proposés par la plateforme et pour l’hébergement des données.

81\. Les données à caractère personnel du producteur sont conservées pendant toute la durée d’activation du compte augmentée des durées de conservation obligatoires en matière de durée légale de prescription.

82\. Le producteur est informé que la DGE ne transfert aucune donnée en dehors de France.

83\. Le producteur est informé qu'il dispose d'un droit d'accès, d’interrogation et de rectification qui lui permet, le cas échéant, de faire rectifier, compléter, mettre à jour ou effacer les données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques, périmées ou dont la collecte, l'utilisation, la communication ou la conservation est interdite.

84\. Le producteur a également le droit de formuler des directives spécifiques et générales concernant la conservation, l’effacement et la communication de ses données post-mortem. En ce qui concerne les directives générales, elles devront être adressées à tiers qui sera désigné par Décret.

85\. Le producteur peut exercer l'ensemble de ces droits en s’adressant à la DGE par courrier à l’adresse suivante :

* par courrier à l’adresse : DGE – Bureau de la communication – 67 rue Barbès  - 94200 Ivry sur Seine ;
* par le formulaire de contact électronique disponible à l’adresse : https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme.
* en utilisant la messagerie interne de l’application.

86\. Les demandes d’exercice du droit d’accès devront être accompagnées d’une copie d’un titre d’identité en cours de validité.

87\. Enfin, le producteur a le droit d’introduire une réclamation auprès de la Commission nationale Informatique et libertés (ci-après « CNIL »).

#### 16.2. A la charge du producteur

88\. Les jeux de données ainsi que les statistiques publiés sur la plateforme peuvent éventuellement contenir des données à caractère personnel.

89\. Le producteur fait son affaire des formalités et obligations nécessaires en la matière et garantit la DGE du respect des obligations légales et réglementaires lui incombant au titre de la protection des données à caractère personnel.

90\. Le producteur est responsable, vis-à-vis des personnes concernées, des manquements à la règlementation en matière de protection des données à caractère personnel qui pourraient éventuellement lui être reprochés dans le cadre de la publication des jeux de données et des statistiques.

### 17. Statistiques

91\. Afin d’améliorer et de personnaliser la plateforme et son contenu aux besoins des diffuseurs, la DGE conserve l’historique des connexions des producteurs et des diffuseurs, à savoir le nombre de visites effectuées sur la plateforme, le nombre de pages vues, l’activité des producteurs et des diffuseurs sur la plateforme et la fréquence de retour.

92\. Les catégories de données publiées à des fins de statistiques sont déterminées uniquement par la DGE et en aucun cas par un tiers.

### 18. Convention de preuve

93\. Les données récoltées et enregistrées dans les systèmes d’information de la DGE seront conservées dans des conditions raisonnables de sécurité et considérés comme les preuves des communications intervenues entre les parties.

94\. L’acceptation des conditions générales par voie électronique, les notifications au sein de la plateforme ainsi que tout engagement contractuel pris en ligne par le producteur ont, entre les parties, la même valeur probante que sur support papier.

95\. La présente disposition doit être considérée comme une convention de preuve valablement admise par les parties.

### 19. Liens hypertextes

96\. La plateforme peut contenir des liens hypertextes donnant accès à des sites web de tiers.

97\. Le producteur est formellement informé que les sites web auxquels il peut accéder par l’intermédiaire des liens hypertextes n’appartiennent pas à la DGE.

98\. La DGE décline toute responsabilité quant au contenu des informations fournies sur ces sites au titre de l’activation de l’hyperlien et quant à la politique de confidentialité de ces sites.

99\. Le producteur ne peut invoquer la responsabilité de la DGE en cas de perte ou de dommage de quelque sorte que ce soit du fait de l’activation de ces liens hypertextes.

### 20. Cookies

100\. La DGE s’autorise à suivre la navigation du producteur sur la plateforme et s’engage à n’utiliser des cookies que soit dans le cadre du suivi de session (nécessaire au bon fonctionnement de la plateforme soit à des fins de mesure d’audience.

### 21. Fermeture du compte

101\. Le producteur peut, à tout moment, se désinscrire de la plateforme sur simple demande.

102\. Lorsqu’un producteur ne s’est pas connecté ou n’a pas fait usage de son compte pendant un délai de deux ans, l’administrateur dispose de la capacité de fermer le compte. Cette fermeture est notifiée au producteur par email.

103\. Le producteur reçoit un email d’alerte préalablement à toute fermeture de compte.

104\. Toute fermeture est notifiée au producteur.

105\. Le producteur est informé qu’à partir du moment où il n’est plus utilisateur de la plateforme, il perd l’accès à son espace personnel sécurisé.

106\. La suppression du compte est irréversible.

107\. La fermeture du compte, pour quelque motif que ce soit, implique la perte de l’ensemble des données, informations et avantages conférés par la plateforme.

108\. Il incombe au producteur de prendre à sa charge les mesures nécessaires pour conserver ses données s’il le souhaite.

109\. Le producteur peut demander la fermeture de son compte auprès de l’administrateur :

* par courrier à l’adresse : DGE – Bureau de la communication – 67 rue Barbès  - 94200 Ivry sur Seine ;
* par le formulaire de contact électronique disponible à l’adresse : https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme.
* en utilisant le système de messagerie interne à la plateforme.

### 22. Suspension et résolution

109\. En cas de manquement aux obligations des présentes conditions générales par le producteur, l’administrateur se réserve le droit, sans indemnité ni remboursement le cas échéant, huit jours après l’envoi au producteur d’un courrier lui demandant de se conformer aux présentes conditions générales, de suspendre l’accès à la plateforme jusqu’à ce que la cause de la suspension ait disparu.

110\. Une suspension de plus de 30 jours non traitée par le producteur entraine la suppression du compte.

111\. La DGE pourra fermer le compte de manière immédiate et sans préavis en cas d’usage frauduleux et de mise en cause de sa responsabilité. Toute fermeture du compte est notifiée par email au producteur.

112\. La fermeture du compte pour faute du producteur lui interdit de rouvrir un espace producteur pendant une période d’au moins un an, la réactivation ou la réouverture d’un compte pouvant par ailleurs être en l’espèce conditionnée par un accord express et préalable de la DGE.

### 23. Dispositions générales

113\. Les présentes conditions générales sont régies par la loi française.

114\. En cas de difficultés d’interprétation résultant d’une contradiction entre l’un quelconque des titres figurant en tête des clauses et l’une quelconque des clauses, les titres seront déclarés inexistants.

115\. Si une ou plusieurs stipulations du présent contrat sont tenues pour non valides ou déclarées comme telles en application d’une loi, d’un règlement ou à la suite d’une décision passée en force de chose jugée d’une juridiction compétente, les autres stipulations garderont toute leur force et leur portée.

116\. **TOUT LITIGE CONCERNANT L’ENSEMBLE DES CONDITIONS GENERALES EST DE LA COMPETENCE DES JURIDICTIONS DU RESSORT DE LA COUR D’APPEL DE PARIS, NONOBSTANT PLURALITE DE DEFENDEURS OU APPEL EN GARANTIE.**