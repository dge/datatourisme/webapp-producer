/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

var $ = require("jquery");
require("morris.js/morris");
window.Raphael = require("raphael");

$.fn.initComponents.add(function(dom) {
    $("[data-morris-donut]", dom).each(function () {
        var self = $(this);

        var values = self.data("morrisDonut");

        var donut = new Morris.Donut({
            element: 'donut',
            resize: true,
            colors: ["#efa500", "#116390", "#00a65a", "#c02846", "#5a5a5a"],
            data: values,
            hideHover: 'auto'
        });

        donut.options.data.forEach(function(data, i){
            var legendlabel=$('<span>'+data.label+'</span>')
            var legendItem = $('<div class="mbox"></div>').css('background-color', donut.options.colors[i]).append(legendlabel)
            $('#donut-legend').append(legendItem)
        });
    });
});