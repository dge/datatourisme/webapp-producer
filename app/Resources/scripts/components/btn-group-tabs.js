/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var $ = require("jquery");

$.fn.initComponents.add(function(dom) {
    $('.btn-group:has(.btn[data-toggle="tab"]) .btn', dom).on('click', function(){
        var $this = $(this);
        if ($this.hasClass('active'))
            return false;
        $this.parent().find('.active').removeClass('active');
        $this.addClass('active');
    });
});