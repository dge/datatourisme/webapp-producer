/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

var $ = require("jquery");
var moment = require('moment');
require('moment/locale/fr');
require('jquery-sparkline');

/**
 * Spark
 *
 * @param parameters
 * @param options
 *
 * el: Element (HTMLElement).
 * values: Sparkline values (Array).
 * tooltips: Sparkline tooltips (Array).
 * options: Sparkline options (Object).
 */
function Spark (parameters, options)
{
    parameters = typeof parameters !== 'undefined' ? parameters : {};
    options = typeof options !== 'undefined' ? options : {};

    var defaultParameters = {
        el: '',
        values: [],
        tooltips: []
    };
    var defaultOptions = {
        width: 100,
        height: 20,
        fillColor: '#ECF0F5',
        lineColor: '#275C7A',
        minSpotColor: false,
        maxSpotColor: false,
        lineWidth: 2,
        spotColor: false,
        tooltipValueLookups: {},
        tooltipFormat: '{{x:names}} : {{y}}'
    };

    this.parameters = $.extend(defaultParameters, parameters);
    this.options = $.extend(defaultOptions, options);
}

/**
 * Runs process
 */
Spark.prototype.run = function()
{
    if (this.parameters.tooltips.length > 0)
        this.configureTooltips();
    this.initSparklines();
};

/**
 * Configures tooltips
 */
Spark.prototype.configureTooltips = function()
{
    moment().locale('fr');
    var tooltips = this.parameters.tooltips;

    var names = {};
    for (var i = 0, count = tooltips.length; i < count; i++)
        names[i] = moment(new Date(tooltips[i])).format('l');

    this.options.tooltipValueLookups = {
        names: names
    };
};

/**
 * Initializes chart
 * @returns {boolean}
 */
Spark.prototype.initSparklines = function()
{
    var className = 'sparklines-'+new Date().getTime();
    this.parameters.el.classList.add(className);

    $('.'+className).sparkline(
        this.parameters.values,
        this.options
    );

    return true;
};

module.exports = Spark;