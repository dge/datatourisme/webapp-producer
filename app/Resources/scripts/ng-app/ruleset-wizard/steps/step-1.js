/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular.module('ruleset-wizard').config(config);

/**
 * @ngInject
 */
function config($stateProvider) {
    $stateProvider.state('wizard.step1', {
        url: "step-1",
        template: require("./step-1.html"),
        controller: controller
    });
}

/**
 * @ngInject
 */
function controller($state, $scope, $q, $http, config) {
    if($scope.schema == null) {
        return $state.go('wizard.step0');
    }
    $scope.$parent.currentStep = 1;

    if(!$scope.data.xpathContext) {
        // pre-select best match
        var elements = $scope.schema.filter(function(obj) {
            return obj.type == "element" && !obj.value;
        });
        var preselect = elements.reduce(function(prev, curr) {
            return prev.minOccurs > curr.minOccurs ? prev : curr;
        });
        if(preselect.minOccurs > 1) {
            $scope.data.xpathContext = { type: 'xpath', expr: preselect.xpath};
        }
    }

    $scope.submit = function() {
        if($scope.form.$valid) {
            $state.go('wizard.step2');
        }
    }
}