/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular.module('ruleset-wizard').config(config);

/**
 * @ngInject
 */
function config($stateProvider) {
    $stateProvider.state('wizard.step0', {
        url: "step-0",
        template: require("./step-0.html"),
        controller: controller
    });
}

/**
 * @ngInject
 */
function controller($state, $http, config, $scope, Upload) {
    $scope.$parent.currentStep = 0;

    // upload on file select or drop
    $scope.upload = function (file) {
        $scope.error = null;
        $scope.$parent.progress = 'Téléchargement du fichier...';
        Upload.upload({
            url: config.processFileUrl,
            data: {file: file}
        }).then(function (resp) {
            // todo : notify
            $scope.$parent.schema = resp.data;
            $scope.$parent.progress = null;
            $scope.data.file = null;
            $scope.data.xpathContext = null;
            $state.go('wizard.step1');
        }, function (resp) {
            // todo : error
            $.notify({message: resp.data.error}, {type:"danger"});
            $scope.progress = null;
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            if(progressPercentage < 100) {
                $scope.$parent.progress = progressPercentage + ' %';
            } else {
                $scope.$parent.progress = 'Traitement du fichier...';
            }
        });
    };

    /**
     *
     */
    $scope.submit = function() {
        if($scope.form.$valid && $scope.form.file.$valid && $scope.data.file) {
            $scope.upload($scope.data.file);
        } else if($scope.schema != null) {
            $state.go('wizard.step1');
        }
    }
}