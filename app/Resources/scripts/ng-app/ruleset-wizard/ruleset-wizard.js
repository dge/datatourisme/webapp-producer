/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');
require('ng-file-upload');
require("angular-ui-router");
require('xpath-editor');

window.Selectize = require('selectize');
require('angular-selectize2/dist/selectize');


// module definition
angular.module('ruleset-wizard', [
    'ngFileUpload',
    'xpath-editor',
    'selectize',
    'ui.router'
]).config(config);

require('./steps/*.js', {mode: 'expand'});

/**
 * @ngInject
 */
function config($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise("/step-0");

    $stateProvider.state('wizard', {
        url: "/",
        abstract: true,
        template: require("./ruleset-wizard.html"),
        controller: controller,
        resolve: {
            schema: resolveSchema
        }
    });

    $httpProvider.interceptors.push(function ($q) {
        return {
            'responseError': function (rejection) {
                if(rejection.status >= 500) {
                    var msg = rejection.data.error || "Erreur inconnue.";
                    $.notify(msg, {type: "danger", delay: 10000});
                }
                return $q.reject(rejection);
            }
        };
    });
}

/**
 * @ngInject
 */
function resolveSchema(config, $http) {
    return $http.get(config.xpathSchemaUrl).then(function(data) {
        return data.data;
    }, function() { return null; });
}

/**
 * @ngInject
 */
function controller(config, $state, $scope, $q, $http, schema) {
    $scope.schema = schema;
    if($scope.schema != null) {
        $state.go("wizard.step1");
    }

    $scope.currentStep = 0;
    $scope.data = {
        //xpathContext: { type: 'xpath', expr: '/tif:OI'},
        //xpathId: { type: 'xpath', expr: 'tif:DublinCore/dc:identifier'},
        //xpathLabel: { type: 'xpath', expr: 'tif:DublinCore/dc:title'},
        //xpathType: { type: 'xpath', expr: 'tif:DublinCore/tif:ControlledVocabulary/@code'}
    };

    $scope.stats = {
        count: null,
        uniqness: null,
        labelness: null,
        types: null,
        countTypes: null
    };
    $scope.statsLoading = false;

    $scope.typeSelectize = initTypeSelectize(config.typeChoices);

    var loadStats = function(promise) {
        $scope.statsLoading = true;
        promise.then(function() {
            $scope.statsLoading = false;
        }, function(resp) {
            $scope.statsLoading = false;
        });
        return promise;
    };

    $scope.$watch("data.xpathContext", function(xpath) {
        if(xpath) {
            loadStats($http.post(config.xpathSourceUrl, {context: xpath.expr, count: true})).then(function(data) {
                $scope.stats.count = data.data;
            }, function() {
                delete $scope.data.xpathContext;
            });
        }
    });

    $scope.$watch("data.xpathId", function(xpath) {
        if(xpath) {
            loadStats($http.post(config.xpathSourceUrl, angular.extend({}, xpath, {
                context: $scope.data.xpathContext.expr,
                distinct: true,
                firstOnly: true
            })).then(function(response) {
                $scope.stats.uniqness = response.data.length / $scope.stats.count;
            }));
        }
    });

    $scope.$watch("data.xpathLabel", function(xpath) {
        if(xpath) {
            loadStats($http.post(config.xpathSourceUrl, angular.extend({}, xpath, {
                context: $scope.data.xpathContext.expr,
                firstOnly: true
            })).then(function(response) {
                $scope.stats.labelness = response.data.length / $scope.stats.count;
            }));
        }
    });

    $scope.$watch("data.xpathType", function(xpath) {
        if(xpath) {
            loadStats($http.post(config.xpathSourceUrl, angular.extend({}, xpath, {
                context: $scope.data.xpathContext.expr
            })).then(function(data) {
                var types = {};
                var values = data.data.filter(function(value, index, self) {
                    return self.indexOf(value) === index;
                });
                for(var i=0; i<data.data.length; i++) {
                    types[data.data[i]] = types[data.data[i]] ? types[data.data[i]]+1 : 1;
                }
                $scope.stats.types = types;
                $scope.stats.countTypes = values.length;
                $scope.statsLoading = false;

                // init corresps data : now in step 3
                var corresps = {};
                for(var key in types) {
                    corresps[key] = config.thesaurusValues[key] ? config.thesaurusValues[key] : null;
                }
                $scope.data.corresps = corresps;
            }));
        }
    });

    // watch data to reset some data & stats
    $scope.$watchCollection("data", function(data, old) {
        if(!data.xpathContext || !angular.equals(data.xpathContext, old.xpathContext)) {
            delete $scope.data.xpathId;
            delete $scope.data.xpathLabel;
            delete $scope.data.xpathType;
            delete $scope.data.resourceType;
            $scope.stats.count = null;
            data = $scope.data;
        }
        if(!data.xpathId) {
            $scope.stats.uniqness = null;
        }
        if(!data.xpathLabel) {
            $scope.stats.labelness = null;
        }
        if(!data.xpathType) {
            $scope.stats.types = null;
            $scope.stats.countTypes = null;
            delete $scope.data.corresps;
        }
    });

    /**
     * Return remaining corresps to align
     *
     * @returns {*}
     */
    $scope.remainingCorresps = function() {
        if(!$scope.data.corresps) {
            return null;
        }
        var arr = [];
        for(var key in $scope.data.corresps) {
            if($scope.data.corresps[key] == null) {
                arr.push(key);
            }
        }
        return arr;
    }

    /**
     * Validate the whole process
     *
     * @returns {*}
     */
    $scope.validate = function() {

        var thesaurus = $scope.data.corresps;
        for (var key in thesaurus) {
            if (thesaurus[key] === null) {
                delete thesaurus[key];
            }
        }

        var params = {
            'alignment': {
                '@context': {
                    'rdf':'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                    'rdfs':'http://www.w3.org/2000/01/rdf-schema#',
                    'dc':'http://purl.org/dc/elements/1.1/'
                },
                'contextExpr': $scope.data.xpathContext.expr,
                'rules': {
                    "dc:identifier": $scope.data.xpathId,
                    "rdfs:label": $scope.data.xpathLabel,
                    "rdf:type": $scope.data.resourceType ? {
                        type:'constant',
                        value: [$scope.data.resourceType]
                    } : $scope.data.xpathType
                }
                //'xpathId': $scope.data.xpathId,
                //'xpathType': $scope.data.xpathType,
                //'xpathLabel': $scope.data.xpathLabel,
                //'resourceType': $scope.data.resourceType
            },
            'thesaurus': thesaurus
        };

        $scope.progress = "Préparation de l'alignement...";
        // post to current page
        $http.post(window.location.href, params).then(function(data) {
            if(data.data.redirect) {
                window.location.href = data.data.redirect;
            } else {
                $scope.progress = null;
            }
        }, function() {
            $scope.progress = null;
        });
    };

}

/**
 * Selectize initialization
 */
function initTypeSelectize(options) {

    // selectize options
    var config = {
        searchField: ['label'],
        valueField: 'uri',
        labelField: 'label',
        placeholder: 'Recherchez un type',
        maxItems: 1,
        render: {
            option: function(item, escape) {
                var str = escape(item.label);
                if(!item.depth) {
                    str = "<strong>" + str + "</strong>";
                }
                return '<div>' + Array((item.depth?item.depth:0)+1).join("&nbsp;&nbsp;&nbsp;") + str + '</div>'
            }
        },
        score: function(search) {
            var scoreFunc = this.getScoreFunction(search);
            return function(item) {
                var score = scoreFunc(item);
                if(score || typeChildMatch(item, scoreFunc)) {
                    return 1;
                }
                return 0;
            };
        },
    };

    function typeChildMatch(item, scoreFunc) {
        for(var i=0; i<options.length; i++) {
            if(options[i].parent == item.uri) {
                if(scoreFunc(options[i]) || typeChildMatch(options[i], scoreFunc)) {
                    return true;
                }
            }
        }
        return false;
    }

    return {
        options: options,
        config: config
    }
}